package com.aait.ahjezclient.Cart


import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

import androidx.room.OnConflictStrategy.REPLACE


@Dao
interface DAO {
    @get:Query("select * from ProviderModelOffline")
    val allCartItems: LiveData<List<ProviderModelOffline>>


    @Insert(onConflict = REPLACE)
    fun addItem(cartModelOffline: ProviderModelOffline): Long?
    //
    //    @Insert(onConflict = REPLACE)
    //    void addOrder(OrderModel orderModel);

    @Delete
    fun deleteItem(cartModel:  ProviderModelOffline)

    //    @Delete
    //    void deleteOrder(OrderModel orderModel);

    @Delete
    fun deleteItems(cartModel: List<ProviderModelOffline>)

    //    @Delete
    //    void deleteOrders(List<OrderModel> orderModels);

    @Update
    fun updateItem(cartModel: ProviderModelOffline)


    @Query("select * from ProviderModelOffline where provider_id =:productId")
    fun selectItem(productId: Int): LiveData<ProviderModelOffline>



}
