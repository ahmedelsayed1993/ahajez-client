package com.aait.ahjezclient.Cart

import android.app.Application

import android.os.AsyncTask

import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AddCartViewModel(application: Application) : AndroidViewModel(application) {
    var cartDataBase: CartDataBase
    internal var dao: DAO
    lateinit var cart: LiveData<ProviderModelOffline>
        internal set
    //   new AllAsynTask().execute();
    var allCart: LiveData<List<ProviderModelOffline>>
        internal set

    init {
        cartDataBase = CartDataBase.getDataBase(application)
        dao = cartDataBase.dao()
        allCart = dao.allCartItems
    }

    fun addItem(cartModel: ProviderModelOffline) {

        AddAsyncTask().execute(cartModel)
    }

    fun selectItem(id: Int): AsyncTask<Int, Void, LiveData<ProviderModelOffline>> {

        return SelectAsyncTask().execute(id)
    }

    inner class AddAsyncTask : AsyncTask<ProviderModelOffline, Void, Void>() {

        override fun doInBackground(vararg cartModels: ProviderModelOffline): Void? {
            val id = cartDataBase.dao().addItem(cartModels[0])
            Log.e("id", id!!.toString() + "")
            dao.addItem(cartModels[0])
            return null
        }


    }

    inner class SelectAsyncTask : AsyncTask<Int, Void, LiveData<ProviderModelOffline>>() {
        override fun doInBackground(vararg p0: Int?): LiveData<ProviderModelOffline> {
            cart = dao.selectItem(p0[0]!!)
            return cart
        }



    }

    inner class AllAsynTask : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            allCart = dao.allCartItems
            return null
        }
    }
}
