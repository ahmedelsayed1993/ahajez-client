package com.aait.ahjezclient.Cart

import android.app.Application

import android.os.AsyncTask

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AllCartViewModel(application: Application) : AndroidViewModel(application) {
    var cartDataBase: CartDataBase
    internal var dao: DAO
    //   new AllAsynTask().execute();
    var allCart: LiveData<List<ProviderModelOffline>>
        internal set

    init {
        cartDataBase = CartDataBase.getDataBase(application)
        dao = cartDataBase.dao()
        allCart = dao.allCartItems
        //Log.e("cart", Gson().toJson(allCart))
    }

    fun getCart() {
        AllAsynTask().execute()
    }

    fun updateItem(cartModel: ProviderModelOffline) {

        UpdateAsyncTask().execute(cartModel)
    }

    inner class UpdateAsyncTask : AsyncTask<ProviderModelOffline, Void, Void>() {

        override fun doInBackground(vararg cartModels: ProviderModelOffline): Void? {
            dao.updateItem(cartModels[0])
            return null
        }
    }

    fun deleteItem(cartModel: ProviderModelOffline) {

        DelteAsyncTask().execute(cartModel)
    }

    fun deleteItems(cartModel: List<ProviderModelOffline>) {

        DeleteAllAsyncTask().execute(cartModel)
    }

    inner class DelteAsyncTask : AsyncTask<ProviderModelOffline, Void, Void>() {

        override fun doInBackground(vararg cartModels: ProviderModelOffline): Void? {
            dao.deleteItem(cartModels[0])
            return null
        }
    }

    inner class DeleteAllAsyncTask : AsyncTask<List<ProviderModelOffline>, Void, Void>() {


        override fun doInBackground(vararg lists: List<ProviderModelOffline>): Void? {
            dao.deleteItems(lists[0])
            return null
        }
    }

    inner class AllAsynTask : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            allCart = dao.allCartItems
            return null
        }
    }


}
