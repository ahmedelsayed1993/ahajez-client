package com.aait.ahjezclient.Cart

import androidx.room.*

@Entity(indices = [Index(value = ["product_id"], unique = true)])

class ProviderModelOffline(var provider_id: Int,
                           var provider_name: String?,
                           var provider_image: String?,
                           var address:String?,
                           var distance:String?,
                           var product_id: Int,
                           var product_name: String?,
                           var product_image: String?,
                           var description:String?,
                           var price:String?,
                           var count:Int?,
                           var max:Int?,
                           var delivery:Int?
                          /* , var order_id:Int?,
                           var user_id:Int?,
                           var payment:Int?*/
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}