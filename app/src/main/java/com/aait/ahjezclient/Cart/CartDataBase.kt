package com.aait.ahjezclient.Cart


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase


@Database(entities = [ProviderModelOffline::class], version = 2)

abstract class CartDataBase : RoomDatabase() {

    abstract fun dao(): DAO

    val MIGRATION_1_2: Migration =
        object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL(
                    "ALTER TABLE department "
                            + " ADD COLUMN phone_num TEXT"
                )
            }
        }
    companion object {

        private var instance: CartDataBase? = null

       public fun getDataBase(context: Context): CartDataBase {

            if (instance == null)
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    CartDataBase::class.java,
                    "cart.db"
                )
//                    .addMigrations(
//                    object : Migration(1, 2) {
//                        override fun migrate(database: SupportSQLiteDatabase) {
//                            database.execSQL(
//                                "ALTER TABLE ProviderModelOffline "
//                                        + " ADD COLUMN max Integer"
//                            +"ALTER TABLE ProviderModelOffline "
//                                        + " ADD COLUMN delivery Integer"
//                            )
//                        }
//                    })

                    .
                        build()
            return instance as CartDataBase
        }

        fun closeDataBase() {
            instance = null
        }


        fun destroyInstance() {
            instance = null
        }
    }
}
