package com.aait.ahjezclient.Cart


import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.TypeConverter

@Entity(indices = [Index(value = ["product_id"], unique = true)])

class CartModelOffline(
    var product_id: Int,
    var product_name: String?,
    var product_image: String?,
    var description:String?,
    var price:String?,
    var count:Int?


) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0



}
