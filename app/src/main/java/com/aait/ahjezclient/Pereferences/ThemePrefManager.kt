package com.aait.ahjezclient.Pereferences

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate

class ThemePrefManager (private val mContext: Context) {

    // return sharedPreferences.getString(App_LANGUAGE, Locale.getDefault().getLanguage());
    var appTheme:Unit
        get() {
            val sharedPreferences = mContext.getSharedPreferences(
                SHARED_PREF_NAME, 0
            )
            //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            //requireActivity().recreate()
           // return //sharedPreferences.getString(App_Theme, )!!
             return AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        set(theme) {
            val sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, 0)
            val editor = sharedPreferences.edit()
            editor.putString(App_Theme, theme.toString())
            editor.apply()
        }

    companion object {
        private val SHARED_PREF_NAME = "Ahjez_pref"
        private val App_Theme = "Ahjez_Theme"
    }
}