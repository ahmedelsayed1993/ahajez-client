package com.aait.ahjezclient.Models

import java.io.Serializable

class CartModel:Serializable {

    var product_id:Int?=null
    var count:Int?=null
    var price:String?=null
    var preferences:String?=null


    constructor(product_id: Int?, count: Int?, price: String?, preferences: String?) {
        this.product_id = product_id
        this.count = count
        this.price = price
        this.preferences = preferences
    }
}