package com.aait.ahjezclient.Models

import java.io.Serializable

class ProviderDetailsModel:Serializable {
    var avatar:String?=null
    var id:Int?=null
    var user_id:Int?=null
    var name:String?=null
    var address:String?=null
    var rate:Float?=null
    var comments:Int?=null
    var favorite:Int?=null
    var desc:String?=null
    var closed:Int?=null
    var services:Int?=null
    var products:Int?=null
    var maintenance:Int?=null
    var work_time_from:String?=null
    var work_time_to:String?=null
    var work_times:ArrayList<WorkTimesModel>?=null
    var distance:String?=null
    var delivery_service:Int?=null
}