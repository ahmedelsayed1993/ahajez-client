package com.aait.ahjezclient.Models

import java.io.Serializable

class MyFavModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var address:String?=null
    var rate:Float?=null
    var provider_id:Int?=null
    var provider_closed:Int?=null
    var subcategory_id:Int?=null

}