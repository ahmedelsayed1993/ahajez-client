package com.aait.ahjezclient.Models

import java.io.Serializable

class CancelResponse:BaseResponse(),Serializable {
    var data:ArrayList<CancelModel>?=null
}