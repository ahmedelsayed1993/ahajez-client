package com.aait.ahjezclient.Models

import java.io.Serializable

class WorkTimesModel :Serializable{
    var days:String?=null
    var day:String?=null
    var time:String?=null
    var extra:String?=null

}