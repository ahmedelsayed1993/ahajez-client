package com.aait.ahjezclient.Models

import java.io.Serializable

class ProdModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var count:String?=null
    var price:Double?=null
}