package com.aait.ahjezclient.Models

import java.io.Serializable

class ServiceDetailsModel :Serializable {
    var images:ArrayList<String>?=null
    var name:String?=null
    var price:String?=null
    var desc:String?=null
    var discount:String?=null
    var free_reservation:Int?=null
    var total_discount:String?=null
    var part_amount:String?=null
    var refundable:Int?=null
    var after_discount:String?=null
}