package com.aait.ahjezclient.Models

import java.io.Serializable

class ProviderModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var address:String?=null
    var closed:Int?=null
    var maintenance:Int?=null
    var distance:Int?=null
    var rate:Float?=null
    var lat:String?=null
    var lng:String?=null
}