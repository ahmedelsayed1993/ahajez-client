package com.aait.ahjezclient.Models

import java.io.Serializable

class ProvidersResponse :BaseResponse(),Serializable {
    var banners:ArrayList<BannersModel>?=null
    var data:ArrayList<ProviderModel>?=null
}