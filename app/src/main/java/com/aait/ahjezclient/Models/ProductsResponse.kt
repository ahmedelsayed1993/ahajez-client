package com.aait.ahjezclient.Models

import java.io.Serializable

class ProductsResponse :BaseResponse(),Serializable {
    var subsections:ArrayList<SubSectionModel>?=null
    var data:ArrayList<ProductModel>?=null
}