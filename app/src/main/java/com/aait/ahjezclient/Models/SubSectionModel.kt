package com.aait.ahjezclient.Models

import java.io.Serializable

class SubSectionModel :Serializable {
    var id:Int?=null
    var name:String?=null
    var selected:Boolean?=false

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }
}