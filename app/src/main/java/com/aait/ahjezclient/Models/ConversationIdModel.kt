package com.aait.ahjezclient.Models

import java.io.Serializable

class ConversationIdModel:Serializable {
    var conversation_id:Int?=null
    var user_id:Int?=null
    var receiver_id:Int?=null
    var lastPage:Int?=null
}