package com.aait.ahjezclient.Models

import java.io.Serializable

class ReservationModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var user:String?=null
    var name:String?=null
    var created:String?=null
    var status:String?=null
}