package com.aait.ahjezclient.Models

import java.io.Serializable

class ProductDetailsModel:Serializable {
    var id:Int?=null
    var price:String?=null
    var discount:String?=null
    var quantity:String?=null
    var name:String?=null
    var desc:String?=null
    var image:ArrayList<String>?=null
    var share_link:String?=null
}