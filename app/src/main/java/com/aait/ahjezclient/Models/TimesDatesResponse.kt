package com.aait.ahjezclient.Models

import java.io.Serializable
import java.util.ArrayList

class TimesDatesResponse:BaseResponse(),Serializable {
    var data:ArrayList<String>?=null
    var times:ArrayList<TimesModel>?=null
}