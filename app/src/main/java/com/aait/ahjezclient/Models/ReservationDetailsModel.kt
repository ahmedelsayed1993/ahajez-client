package com.aait.ahjezclient.Models

import java.io.Serializable

class ReservationDetailsModel:Serializable {
    var status:String?=null
    var id:Int?=null
    var name_service:String?=null
    var service_price:String?=null
    var total:String?=null
    var created:String?=null
    var date:String?=null
    var time:String?=null
    var rate:Float?=null
    var name:String?=null
    var address:String?=null
    var avatar:String?=null
    var chat_provider:Int?=null
    var cancellation:String?=null
    var payment_free:Int?=null
    var order_rate:Int?=null
    var order_comment:String?=null
    var refundable:Int?=null
}