package com.aait.ahjezclient.Models

import java.io.Serializable

class ProductModel :Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var desc:String?=null
    var price:String?=null
    var discount:String?=null
    var percent:String?=null
}