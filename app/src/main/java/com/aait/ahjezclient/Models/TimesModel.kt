package com.aait.ahjezclient.Models

import java.io.Serializable

class TimesModel:Serializable {
    var id:Int?=null
    var time:String?=null
    var show_time:String?=null
    var checked:Int?=null
    var selected:Boolean?=false

}