package com.aait.ahjezclient.Models

import java.io.Serializable

class CancelModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var selected:Boolean?=null
}