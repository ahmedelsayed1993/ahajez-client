package com.aait.ahjezclient.UI.Activities.Auth

import android.app.Dialog
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.*
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.NotificationActivity
import com.aait.ahjezclient.UI.Activities.Main.ProviderDetails
import com.aait.ahjezclient.UI.Activities.Main.ProviderDetailsActivity
import com.aait.ahjezclient.UI.Controllers.FavAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FavouriteActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_fav
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var favAdapter: FavAdapter
    var favModels = ArrayList<MyFavModel>()
    lateinit var notification:ImageView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.favourite)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        favAdapter = FavAdapter(mContext,favModels,R.layout.recycle_fav)
        favAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = favAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))

        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.MyFavs("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object:
            Callback<MyFavResponse> {
            override fun onFailure(call: Call<MyFavResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<MyFavResponse>,
                response: Response<MyFavResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {

                            favAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.cancel){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddFav("Bearer "+user.userData.token!!,lang.appLanguage,favModels.get(position).provider_id!!,favModels.get(position).subcategory_id!!)?.enqueue(object:
                Callback<FavRespopnse>{
                override fun onFailure(call: Call<FavRespopnse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

                override fun onResponse(
                    call: Call<FavRespopnse>,
                    response: Response<FavRespopnse>
                ) {
                    hideProgressDialog()
                    if(response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                           getData()

                        }
                        else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
        }else{
            if (favModels.get(position).provider_closed==0) {
                val intent = Intent(mContext, ProviderDetails::class.java)
                 intent.putExtra("cat",favModels.get(position).subcategory_id)
                intent.putExtra("id", favModels.get(position).provider_id)

                startActivity(intent)
            }else{
                val dialog = Dialog(mContext)
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                dialog ?.setCancelable(true)
                dialog ?.setContentView(R.layout.dialog_sorry)


                val ok = dialog?.findViewById<Button>(R.id.ok)
                val text = dialog?.findViewById<TextView>(R.id.text)
                text.text = getString(R.string.Service_provider_outside_working_hours)

                ok?.setOnClickListener {
                    dialog?.dismiss()
//                            val intent = Intent(mContext, ProviderDetailsActivity::class.java)
//                            intent.putExtra("cat", Cat)
//                            intent.putExtra("id", myShopModel!!.id)
//                            intent.putExtra("lat",mLat)
//                            intent.putExtra("lng",mLang)
//                            startActivity(intent)

                }
                dialog?.show()
            }
        }

    }
}