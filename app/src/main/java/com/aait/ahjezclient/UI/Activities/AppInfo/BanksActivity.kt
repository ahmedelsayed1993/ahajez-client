package com.aait.ahjezclient.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.BanksModel
import com.aait.ahjezclient.Models.BanksResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.NotificationActivity
import com.aait.ahjezclient.UI.Controllers.BanksAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class BanksActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_bank_account
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var accounts:RecyclerView
    lateinit var next:Button
    lateinit var banksAdapter: BanksAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var banks = ArrayList<BanksModel>()

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        accounts = findViewById(R.id.accounts)
        next = findViewById(R.id.next)
        back.setOnClickListener { onBackPressed()
        finish()}
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        title.text = getString(R.string.recharge_balance)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java)) }
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        banksAdapter = BanksAdapter(mContext,banks,R.layout.recycle_banks)
        accounts.layoutManager = linearLayoutManager
        accounts.adapter = banksAdapter
        getData()
        next.setOnClickListener { startActivity(Intent(this,TransferActivity::class.java))
        finish()}


    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Banks(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object :
            Callback<BanksResponse>{
            override fun onFailure(call: Call<BanksResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BanksResponse>, response: Response<BanksResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        banksAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}