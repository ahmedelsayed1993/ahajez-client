package com.aait.ahjezclient.UI.Controllers

import android.app.Activity
import android.content.Context
import android.graphics.BitmapFactory
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView

import com.aait.ahjezclient.Models.ProviderModel
import com.aait.ahjezclient.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

import java.io.IOException
import java.io.InputStream
import java.net.URL

import de.hdodenhof.circleimageview.CircleImageView

class CustomInfoWindowOfMap(private val mContext: Context, private val shopModel: ProviderModel) :
    GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker): View {
        val viewMarker = (mContext as Activity).layoutInflater.inflate(R.layout.custom_info, null)
        //        FrameLayout custom_marker_view = viewMarker.findViewById(R.id.custom_marker_view);

        val image = viewMarker.findViewById<CircleImageView>(R.id.image)


        val drawableRes = shopModel.image
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        try {
            val url = URL(drawableRes)
            image.setImageBitmap(BitmapFactory.decodeStream(url.content as InputStream))
        } catch (e: IOException) {
            Log.e("error", e.message!!)
        }



        return viewMarker

    }

    override fun getInfoContents(marker: Marker): View? {
        return null
    }
}
