package com.aait.ahjezclient.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.UserModel
import com.aait.ahjezclient.Models.UserResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewPassActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_new_pass
    lateinit var new_password: EditText
    lateinit var confirm_password: EditText
    lateinit var confirm: Button
    lateinit var view: ImageView
    lateinit var view_confirm: ImageView
    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("data") as UserModel


        new_password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_pass)
        view = findViewById(R.id.view)
        view_confirm = findViewById(R.id.view_confirm)
        confirm = findViewById(R.id.confirm)

        view.setOnClickListener { if (new_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            new_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            new_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        view_confirm.setOnClickListener { if (confirm_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            confirm_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        confirm.setOnClickListener {
            if (
                CommonUtil.checkEditError(new_password,getString(R.string.enter_new_pass))||
                CommonUtil.checkLength(new_password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.enter_confirm_new))){
                return@setOnClickListener
            }else{
                if (!new_password.text.toString().equals(confirm_password.text.toString())){
                    confirm_password.error = getString(R.string.password_not_match)
                }else{
                    NewPassword()
                }
            }
        }

    }
    fun NewPassword(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.NewPass("Bearer "+userModel.token,new_password.text.toString(),userModel.code!!,lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@NewPassActivity,LoginActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

}