package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import android.content.Intent
import android.view.MotionEvent
import android.view.ScaleGestureDetector

import android.view.View
import android.widget.ImageView
import com.aait.ahjezclient.Models.BannersModel
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.ImageActivity


import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderAdapter

import kotlinx.android.synthetic.main.card_image_slider.view.*


class SliderAdapter (context:Context,list : ArrayList<BannersModel>) : CardSliderAdapter<BannersModel>(list) {


    var list = list
    var context=context

    lateinit var image:ImageView
    override fun bindView(position: Int, itemContentView: View, item: BannersModel?) {
            image = itemContentView.image
            Glide.with(context).load(item!!.image).into(image)
            itemContentView.setOnClickListener {
                val intent  = Intent(context, ImageActivity::class.java)
                intent.putExtra("link",list)
                context.startActivity(intent)
            }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }

}