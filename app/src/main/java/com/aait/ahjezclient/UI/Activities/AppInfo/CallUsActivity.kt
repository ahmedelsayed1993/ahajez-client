package com.aait.ahjezclient.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.TermsResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.NotificationActivity
import com.aait.ahjezclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallUsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_call_us
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var message:EditText
    lateinit var send:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        name = findViewById(R.id.user)
        phone = findViewById(R.id.phone)
        message = findViewById(R.id.message)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.call_us)
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(
            Intent(this,
                NotificationActivity::class.java)
        )
            finish()}
        send.setOnClickListener {
            if(CommonUtil.checkEditError(name,getString(R.string.enter_user_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(message,getString(R.string.enter_message))){
                return@setOnClickListener
            }else{
                     SendData()
            }
        }

    }

    fun SendData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Contact(lang.appLanguage,name.text.toString(),phone.text.toString()
        ,message.text.toString())?.enqueue(object: Callback<TermsResponse>{
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        onBackPressed()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}