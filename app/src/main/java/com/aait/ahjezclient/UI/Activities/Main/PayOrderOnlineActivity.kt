package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.os.Handler
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.Models.TermsResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PayOrderOnlineActivity  : ParentActivity() {

    internal var web: WebView? = null
    internal var id: Int = 0
    internal var user_id = 0
   var  provider = 0
    internal lateinit var pay: String
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    var providers = ArrayList<ProviderModelOffline>()
    protected override val layoutResource: Int
        get() = R.layout.activity_online



    protected override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        user_id = intent.getIntExtra("user",0)
        pay = intent.getStringExtra("type")!!
        provider = intent.getIntExtra("provider",0)
        web = findViewById(R.id.web)
        web!!.settings.javaScriptEnabled = true
        Log.e(
            "link",
            "https://aihjazwanjuz.aait-sa.com/online-payment?user=" + user_id+"&order_id="+id+"&methods="+pay
        )
        web!!.loadUrl("https://aihjazwanjuz.aait-sa.com/online-payment?user=" + user_id+"&order_id="+id+"&methods="+pay)
        Log.e("url", web!!.url!!)
        web!!.webViewClient = MyWebVew()

        if (web!!.url!!.contains("https://aihjazwanjuz.aait-sa.com/payment-success")) {
            Pay()

        }else if (web!!.url!!.contains("https://aihjazwanjuz.aait-sa.com/payment-fail")){

            onBackPressed()
            finish()
            // CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
        }

        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        //allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)


        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){

                }else {
                    for (i in 0..cartModels.size-1){
                        // providers.clear()
                        // cart.clear()
                        if (cartModels.get(i).provider_id==provider) {
                            providers.add(cartModels.get(i))

                        }

                    }
                    Log.e("car", Gson().toJson(providers))


                }


            }
        })


    }

    protected fun hideInputType(): Boolean {
        return false
    }

    inner class MyWebVew : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: String): Boolean {
            Log.e("link",request)
            if (request.contains("https://aihjazwanjuz.aait-sa.com/payment-fail")) {
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        finish()
                       // CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            } else if (request.contains("https://aihjazwanjuz.aait-sa.com/payment-success")) {


                Pay()

            }else{
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        finish()
                       // CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            }
            return true
        }
    }

    fun Pay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.PayOrder("Bearer"+user.userData.token,lang.appLanguage,id,"online")?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        val intent = Intent(this@PayOrderOnlineActivity,DoneActivity::class.java)
                        intent.putExtra("id",id)
                        startActivity(intent)
                        finish()
                        allCartViewModel!!.allCart.observe(this@PayOrderOnlineActivity, object : Observer<List<ProviderModelOffline>> {
                            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                                cartModelOfflines = cartModels

                                if(cartModels.size==0){


                                }else {

                                    for (i in 0..cartModels.size-1){
                                        // providers.clear()
                                        // cart.clear()
                                        if (cartModels.get(i).provider_id==provider) {
                                            allCartViewModel!!.deleteItem(cartModels.get(i))

                                        }


                                    }
                                }


                            }
                        })
                    }else
                    {
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}
