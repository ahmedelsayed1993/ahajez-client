package com.aait.ahjezclient.UI.Fragments

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.aait.ahjezclient.Base.BaseFragment
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class ReservationFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_reservation
    companion object {
        fun newInstance(): ReservationFragment {
            val args = Bundle()
            val fragment = ReservationFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null
    override fun initializeComponents(view: View) {
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeTapAdapter(mContext!!,childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
    }
}