package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Fragments.*

class OrderTapAdapter (
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            CurrentOrderFragment()
        } else {
            FinishedOrderFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.current_order)
        }  else {
            context.getString(R.string.finished_orders)
        }
    }
}
