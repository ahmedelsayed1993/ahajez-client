package com.aait.ahjezclient.UI.Activities.Main

import android.view.View
import android.widget.ImageView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.BannersModel
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.SlideAdapter
import com.aait.ahjezclient.UI.Controllers.SliderAdpaters
import com.github.islamkhsh.CardSliderViewPager

class ImageActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_show_image
    lateinit var viewpager: CardSliderViewPager
    var list = ArrayList<BannersModel>()
    lateinit var back: ImageView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
            finish()}
        viewpager = findViewById(R.id.viewPager)
        list = intent.getStringArrayListExtra("link") as ArrayList<BannersModel>
        initSliderAds(list)

    }
    fun initSliderAds(list:ArrayList<BannersModel>){
        if(list.isEmpty()){
            viewpager.visibility= View.GONE


        }
        else{
            viewpager.visibility= View.VISIBLE


            viewpager.adapter= SlideAdapter(mContext!!,list)

        }
    }
}