package com.aait.ahjezclient.UI.Activities.Auth
import android.app.Dialog
import android.content.Intent
import android.provider.Settings
import android.text.InputType
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.UserResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.MainActivity
import com.aait.ahjezclient.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging

import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_login
    lateinit var phone: EditText
    lateinit var password:EditText
    lateinit var view: ImageView
    lateinit var forgot_pass: TextView
    lateinit var login: Button
    lateinit var register: LinearLayout
    var deviceID = ""
    var ID = ""
    override fun initializeComponents() {
        ID =  Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })

        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        forgot_pass = findViewById(R.id.forgot_pass)
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        register.setOnClickListener { startActivity(Intent(this,RegisterActivity::class.java)) }
        forgot_pass.setOnClickListener {startActivity(Intent(this,ForgotPassActivity::class.java))  }
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }

        login.setOnClickListener {
            if(CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkEditError(password,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                Login()
            }
        }

    }

    fun Login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Login(phone.text.toString()
            ,password.text.toString(),deviceID,"android",lang.appLanguage,ID)?.enqueue(object:
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
                Log.e("error", Gson().toJson(t))
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.user_type!!.equals("user")) {
                            user.loginStatus = true
                            user.userData = response.body()?.data!!

                            val intent = Intent(this@LoginActivity, MainActivity::class.java)
                            intent.putExtra("state", "login")
                            startActivity(intent)
                            finish()

                        }else{
                            CommonUtil.makeToast(mContext,getString(R.string.you_not_user))
                        }
//                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
//                        startActivity(intent)
//                        finish()
                    }else if (response.body()?.value.equals("2")){
                        val intent = Intent(this@LoginActivity,ActivationCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}