package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentRecyclerAdapter
import com.aait.ahjezclient.Base.ParentRecyclerViewHolder
import com.aait.ahjezclient.Models.ReservationModel
import com.aait.ahjezclient.R

class OrdersAdapter  (context: Context, data: MutableList<ReservationModel>, layoutId: Int) :
    ParentRecyclerAdapter<ReservationModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.order_type.text = mcontext.getString(R.string.order_num)+": # "+questionModel.id

        viewHolder.order_num.text = questionModel.name
        viewHolder.time.text = questionModel.created
        if (questionModel.status.equals("refuse")||questionModel.status.equals("cancel")){
            viewHolder.canncelled.visibility = View.VISIBLE
        }else{
            viewHolder.canncelled.visibility = View.GONE
        }

        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_fall_down)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var order_num=itemView.findViewById<TextView>(R.id.order_num)
        internal var order_type = itemView.findViewById<TextView>(R.id.order_type)
        internal var time = itemView.findViewById<TextView>(R.id.time)
        internal var canncelled = itemView.findViewById<TextView>(R.id.cancelled)





    }
}