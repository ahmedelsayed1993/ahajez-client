package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.Models.CartModel
import com.aait.ahjezclient.Models.FavRespopnse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.LocationActivity
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_product_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderLocationActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_location
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var address:TextView
    lateinit var phone:EditText
    lateinit var notes:EditText
    lateinit var next:Button
    var lat = ""
    var lng = ""
    var result = ""
    var cart = ArrayList<CartModel>()
    var id = 0
    var tax = 0.0
    var value = 0.0
    var tax_val = ""
    var total = 0.0
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    override fun initializeComponents() {
        cart = intent.getSerializableExtra("cart") as ArrayList<CartModel>
        id = intent.getIntExtra("id",0)
        tax = intent.getDoubleExtra("tax",0.0)
        value = intent.getDoubleExtra("value",0.0)
        tax_val = intent.getStringExtra("tax_val")!!
        total = intent.getDoubleExtra("total",0.0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        address = findViewById(R.id.address)
        phone = findViewById(R.id.phone)
        notes = findViewById(R.id.notes)
        next = findViewById(R.id.next)
        phone.setText(user.userData.phone)
        back.setOnClickListener { onBackPressed()
        finish()}
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        title.text = getString(R.string.complete_order)
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        //allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)


        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels



            }
        })

        address.setOnClickListener { startActivityForResult(Intent(this, LocationActivity::class.java),1)  }
        next.setOnClickListener { if (CommonUtil.checkTextError(address,getString(R.string.enter_delivery_address))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_recipient))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)){
        return@setOnClickListener
        }else{
            AddOrder()
        }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                address.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                address.text = ""
            }
        }
    }

    fun AddOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddOrder("Bearer" + user.userData.token,lang.appLanguage,id,
            Gson().toJson(cart),value.toString(),tax.toString(),tax_val,total.toString(),phone.text.toString(),lat,lng,
            address.text.toString(),notes.text.toString(),1)
            ?.enqueue(object : Callback<FavRespopnse> {
                override fun onFailure(call: Call<FavRespopnse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<FavRespopnse>,
                    response: Response<FavRespopnse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
//                            allCartViewModel!!.allCart.observe(this@OrderLocationActivity, object : Observer<List<ProviderModelOffline>> {
//                                override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
//                                    cartModelOfflines = cartModels
//
//                                    if(cartModels.size==0){
//
//
//                                    }else {
//
//                                        for (i in 0..cartModels.size-1){
//                                            // providers.clear()
//                                            // cart.clear()
//                                            allCartViewModel!!.deleteItem(cartModels.get(i))
//
//                                        }
//                                    }
//
//
//                                }
//                            })

                            val intent = Intent(this@OrderLocationActivity,ConfirmOrderActivity::class.java)
                            intent.putExtra("id",response.body()?.data)
                            intent.putExtra("user",response.body()?.user)
                            intent.putExtra("provider",id)
                            startActivity(intent)
                            finish()
                        }
                        else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}