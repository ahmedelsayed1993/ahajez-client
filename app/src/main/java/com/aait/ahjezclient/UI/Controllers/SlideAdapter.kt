package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import com.aait.ahjezclient.Models.BannersModel
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.ImageActivity
import com.aait.ahjezclient.UI.Activities.Main.ImagesActivity
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import com.github.islamkhsh.CardSliderAdapter
import kotlinx.android.synthetic.main.images.view.*

class SlideAdapter  (context: Context, list : ArrayList<BannersModel>) : CardSliderAdapter<BannersModel>(list) {


    var list = list
    var context=context

    lateinit var image: PhotoView
    override fun bindView(position: Int, itemContentView: View, item: BannersModel?) {
        image = itemContentView.image
        //Log.e("image",item?.image)
        Glide.with(context).load(item?.image).into(image)
//        itemContentView.setOnClickListener {
//            val intent  = Intent(context, ImageActivity::class.java)
//            intent.putExtra("link",list)
//            context.startActivity(intent)
//        }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.images }

}