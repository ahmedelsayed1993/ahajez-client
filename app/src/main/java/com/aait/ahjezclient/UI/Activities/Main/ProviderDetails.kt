package com.aait.ahjezclient.UI.Activities.Main

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.GPS.GPSTracker
import com.aait.ahjezclient.GPS.GpsTrakerListener
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.*
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Views.ListDialog
import com.aait.ahjezclient.Utils.CommonUtil
import com.aait.ahjezclient.Utils.DialogUtil
import com.aait.ahjezclient.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class ProviderDetails : ParentActivity(), OnItemClickListener, GpsTrakerListener {
    override fun onItemClick(view: View, position: Int) {

    }

    override val layoutResource: Int
        get() = R.layout.activity_provider_details
    var id = 0
    lateinit var back: ImageView
    lateinit var title: TextView
    private var mAlertDialog: AlertDialog? = null
    lateinit var notification: ImageView
    lateinit var image: CircleImageView
    lateinit var name: TextView
    lateinit var rating: RatingBar
    lateinit var rate: TextView
    lateinit var address: TextView
    lateinit var heart: ImageView
    lateinit var description: TextView
    lateinit var status: TextView
    lateinit var times: TextView
    lateinit var show: TextView
    lateinit var services: LinearLayout
    lateinit var products: LinearLayout
    lateinit var chat: LinearLayout

    lateinit var listDialog: ListDialog
    var workTimesModels = ArrayList<WorkTimesModel>()

    var reciver = 0
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var cat = 0
    var providerDetailsModel: ProviderDetailsModel?=null
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        cat = intent.getIntExtra("cat",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)


        notification = findViewById(R.id.notification)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        rate = findViewById(R.id.rate)
        address = findViewById(R.id.address)
        heart = findViewById(R.id.heart)
        description = findViewById(R.id.description)
        status = findViewById(R.id.status)
        times = findViewById(R.id.times)
        show = findViewById(R.id.show)
        services = findViewById(R.id.services)
        products = findViewById(R.id.products)
        chat = findViewById(R.id.chat)
        back.setOnClickListener { onBackPressed()
            finish()}

        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
       getLocationWithPermission()
        show.setOnClickListener { listDialog.show() }
        rate.setOnClickListener { if (user.loginStatus!!) {
            val intent = Intent(this, CommentsActivity::class.java)
            intent.putExtra("id", id)
            startActivity(intent)
        }}
        rating.setOnClickListener { if (user.loginStatus!!) {
            val intent = Intent(this, CommentsActivity::class.java)
            intent.putExtra("id", id)
            startActivity(intent)
        }}
        products.setOnClickListener {
//            if (providerDetailsModel?.maintenance==1){
//
//            }else {
            val intent = Intent(this, ProductsActivity::class.java)
            intent.putExtra("id", id)
            intent.putExtra("cat", cat)
            intent.putExtra("provider", providerDetailsModel)
            startActivity(intent)
            // }
        }
        services.setOnClickListener {
//            if (providerDetailsModel?.maintenance==1){
//
//            }else {
            val intent = Intent(this, ServicesActivity::class.java)
            intent.putExtra("id", id)
            intent.putExtra("cat", cat)
            intent.putExtra("provider", providerDetailsModel)
            startActivity(intent)
            // }
        }

        chat.setOnClickListener {
            if (user.loginStatus!!){
                getID()
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }

        heart.setOnClickListener { if(user.loginStatus!!){

            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddFav("Bearer "+user.userData.token!!,lang.appLanguage,id,cat)?.enqueue(object:
                Callback<FavRespopnse> {
                override fun onFailure(call: Call<FavRespopnse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

                override fun onResponse(
                    call: Call<FavRespopnse>,
                    response: Response<FavRespopnse>
                ) {
                    hideProgressDialog()
                    if(response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            if (response.body()?.data == 0){
                                heart.setImageResource(R.mipmap.heart)
                            }else{
                                heart.setImageResource(R.mipmap.like)
                            }

                        }
                        else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
        }
        else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
        }
        }

    }

    fun getData(token:String?,lat:String,lng:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderDetails(lang.appLanguage,id,token,lat,lng)?.enqueue(object:
            Callback<ProviderDetailsResponse> {
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                Log.e("response", Gson().toJson(t))
            }

            override fun onResponse(
                call: Call<ProviderDetailsResponse>,
                response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        providerDetailsModel = response.body()?.data!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        reciver = response.body()?.data?.user_id!!
                        name.text = response.body()?.data?.name
                        rating.rating = response.body()?.data?.rate!!
                        rate.text = "("+response.body()?.data?.comments.toString()+")"
                        address.text = response.body()?.data?.address
                        description.text = response.body()?.data?.desc
                        if (response.body()?.data?.favorite == 0){
                            heart.setImageResource(R.mipmap.heart)
                        }else{
                            heart.setImageResource(R.mipmap.like)
                        }
                        if (response.body()?.data?.closed == 0){
                            status.text = getString(R.string.opened)
                        }else{
                            status.text = getString(R.string.closed)
                        }
                        times.text = response.body()?.data?.work_time_from+" _ "+response.body()?.data?.work_time_to
                        if (response.body()?.data?.products==0){
                            products.visibility = View.GONE
                        }else{
                            products.visibility = View.VISIBLE
                        }
                        if (response.body()?.data?.services==0){
                            services.visibility = View.GONE
                        }else{
                            services.visibility = View.VISIBLE
                        }
                        workTimesModels = response.body()?.data?.work_times!!
                        listDialog = ListDialog(mContext,this@ProviderDetails,workTimesModels,getString(
                            R.string.work_times))
                    }
                    else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Conversation("Bearer"+user.userData.token,lang.appLanguage,reciver)?.enqueue(object :
            Callback<ConversationIdResponse> {
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ConversationIdResponse>,
                response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@ProviderDetails,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (user.loginStatus!!) {
                    getData("Bearer " + user.userData.token,gps.latitude.toString(),gps.longitude.toString())
                }else{
                    getData(null,gps.latitude.toString(),gps.longitude.toString())
                }
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
}