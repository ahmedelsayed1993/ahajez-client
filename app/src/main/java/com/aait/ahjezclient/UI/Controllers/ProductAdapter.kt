package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentRecyclerAdapter
import com.aait.ahjezclient.Base.ParentRecyclerViewHolder
import com.aait.ahjezclient.Models.ProductModel
import com.aait.ahjezclient.Models.ProviderModel
import com.aait.ahjezclient.R
import com.aait.ahjezclient.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class ProductAdapter (context: Context, data: MutableList<ProductModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProductModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)
        viewHolder.desc.text = questionModel.desc
//        viewHolder.price.text = questionModel.discount + mcontext.getString(R.string.rs)
        viewHolder.discount.text = questionModel.percent+"%"

        if (questionModel.percent.equals("")){
            viewHolder.discount.visibility = View.GONE
        }else{
            viewHolder.discount.visibility = View.VISIBLE
        }
        if (questionModel.discount.equals("")){
            viewHolder.offer.visibility = View.GONE
            viewHolder.price.text = questionModel.price + mcontext.getString(R.string.rs)
        }else{
            if (questionModel.percent.equals("")){
                viewHolder.offer.visibility = View.GONE
                viewHolder.price.text = questionModel.price + mcontext.getString(R.string.rs)
            }else {
                viewHolder.offer.visibility = View.VISIBLE
                viewHolder.price.text = questionModel.discount + mcontext.getString(R.string.rs)
                viewHolder.offer.text = questionModel.price + mcontext.getString(R.string.rs)
                CommonUtil.setStrokInText(viewHolder.offer)
            }
        }

        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_fall_down)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var discount = itemView.findViewById<TextView>(R.id.discount)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var desc = itemView.findViewById<TextView>(R.id.desc)
        internal var offer = itemView.findViewById<TextView>(R.id.offer)




    }
}