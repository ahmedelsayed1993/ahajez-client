package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.OrderDetailsResponse
import com.aait.ahjezclient.Models.ProdModel
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.LocationActivity
import com.aait.ahjezclient.UI.Controllers.ProdsAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ConfirmOrderActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_details
    lateinit var back:ImageView
    lateinit var title:TextView

    lateinit var notification:ImageView
    lateinit var address:TextView
    lateinit var change:TextView
    lateinit var phone:TextView
    lateinit var notes:TextView
    lateinit var favourites:TextView
    lateinit var products:RecyclerView
    lateinit var products_value:TextView
    lateinit var delivery_lay:LinearLayout
    lateinit var delivery_price:TextView
    lateinit var tax:TextView
    lateinit var tax_value:TextView
    lateinit var total:TextView
    lateinit var confirm:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var prodsAdapter: ProdsAdapter
    var Products = ArrayList<ProdModel>()
    var id = 0
    var user_id = 0
    private var lat = ""
    private var lng = ""
    private var result = ""
    var provider = 0


    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        user_id = intent.getIntExtra("user",0)
        provider = intent.getIntExtra("provider",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)

        notification = findViewById(R.id.notification)
        address = findViewById(R.id.address)
        change = findViewById(R.id.change)
        phone = findViewById(R.id.phone)
        notes = findViewById(R.id.notes)
        favourites = findViewById(R.id.favourites)
        products = findViewById(R.id.products)
        products_value = findViewById(R.id.products_value)
        delivery_price = findViewById(R.id.delivery_price)
        delivery_lay = findViewById(R.id.delivery_lay)
        tax = findViewById(R.id.tax)
        tax_value = findViewById(R.id.tax_value)
        total = findViewById(R.id.total)
        confirm = findViewById(R.id.confirm)
        title.text = getString(R.string.order_review)

        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        back.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("state","normal")
            startActivity(intent)
            finish()}
        change.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        address.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        prodsAdapter = ProdsAdapter(mContext,Products,R.layout.recycle_prod)
        products.layoutManager = linearLayoutManager
        products.adapter = prodsAdapter
        getData()
        confirm.setOnClickListener { val intent = Intent(this,OrderPayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("provider",provider)

            startActivity(intent)
            finish() }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOrder("Bearer"+user.userData.token,lang.appLanguage,id,"user",null)
            ?.enqueue(object : Callback<OrderDetailsResponse> {
                override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<OrderDetailsResponse>,
                    response: Response<OrderDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            address.text = response.body()?.data?.address
                            phone.text = response.body()?.data?.phone
                            notes.text = response.body()?.data?.notes
                            favourites.text = response.body()?.data?.preferences
                            prodsAdapter.updateAll(response.body()?.data?.products!!)
                            products_value.text = response.body()?.data?.total+getString(R.string.rs)
                            tax.text = getString(R.string.The_tax_amount)+response.body()?.data?.tax+"%"
                            tax_value.text = response.body()?.data?.total_tax+getString(R.string.rs)
                            if (response.body()?.data?.delivery_price.equals("")){
                                delivery_lay.visibility = View.GONE
                            }else{
                                delivery_lay.visibility = View.VISIBLE
                                delivery_price.text = response.body()?.data?.delivery_price+getString(R.string.rs)
                            }
                            total.text = response.body()?.data?.final_total+getString(R.string.rs)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                address.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                address.text = ""
            }
        }
    }
}