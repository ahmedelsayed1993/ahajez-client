package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.BaseResponse
import com.aait.ahjezclient.Models.TaxResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.CartProviderAdapter
import com.aait.ahjezclient.UI.Controllers.ProductAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_order_pay.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BasketActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_basket
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var rv_recycle: RecyclerView

    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var providers = ArrayList<ProviderModelOffline>()
    var providers1 = ArrayList<ProviderModelOffline>()
    lateinit var linearLayoutManager: LinearLayoutManager
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    lateinit var cartProviderAdapter: CartProviderAdapter
    var tax = ""
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        back.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("state","normal")
            startActivity(intent)
            finish()}
        title.text = getString(R.string.shopping_basket)
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        rv_recycle = findViewById(R.id.rv_recycle)

        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)

        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        cartProviderAdapter = CartProviderAdapter(mContext,providers,R.layout.recycler_provider)
        cartProviderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = cartProviderAdapter
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)

        Coast()
        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){
                    layNoItem!!.visibility = View.VISIBLE

                }else {
                   layNoItem!!.visibility = View.GONE
                    for (i in 0..cartModels.size-1){
                        providers.add(cartModels.get(i))

                    }

                    Log.e("prov", Gson().toJson(providers.distinctBy { it.provider_id }))

                     cartProviderAdapter.updateAll(providers.distinctBy { it.provider_id })
                    Log.e("cart", Gson().toJson(cartModels))


                }


            }
        })


    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,CartProductsActivity::class.java)
        intent.putExtra("id",providers.get(position).provider_id)
        intent.putExtra("tax",tax)
        startActivity(intent)
        finish()

    }

    fun Coast(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Coasts()?.enqueue(object :Callback<TaxResponse>{
            override fun onFailure(call: Call<TaxResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TaxResponse>, response: Response<TaxResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        tax = response.body()?.data?.tax!!

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}