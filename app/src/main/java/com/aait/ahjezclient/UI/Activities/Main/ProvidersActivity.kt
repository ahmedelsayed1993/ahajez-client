package com.aait.ahjezclient.UI.Activities.Main

import android.Manifest
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.GPS.GPSTracker
import com.aait.ahjezclient.GPS.GpsTrakerListener
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.*
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.CustomInfoWindowOfMap
import com.aait.ahjezclient.UI.Controllers.ProvidersAdapter
import com.aait.ahjezclient.UI.Controllers.SliderAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.aait.ahjezclient.Utils.DialogUtil
import com.aait.ahjezclient.Utils.PermissionUtils
import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class ProvidersActivity:ParentActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GpsTrakerListener , OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (providersModels.get(position).closed==1){
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_sorry)


            val ok = dialog?.findViewById<Button>(R.id.ok)
            val text = dialog?.findViewById<TextView>(R.id.text)
            text.text = getString(R.string.Service_provider_outside_working_hours)

            ok?.setOnClickListener {
                dialog?.dismiss()
//                val intent = Intent(mContext, ProviderDetailsActivity::class.java)
//                intent.putExtra("cat", Cat)
//                intent.putExtra("id", providersModels.get(position).id)
//                intent.putExtra("lat",mLat)
//                intent.putExtra("lng",mLang)
//                startActivity(intent)
//                finish()
            }
            dialog?.show()
        }else if (providersModels.get(position).maintenance==1){
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_sorry)


            val ok = dialog?.findViewById<Button>(R.id.ok)
            val text = dialog?.findViewById<TextView>(R.id.text)
            text.text = getString(R.string.The_service_provider_is_under_maintenance)

            ok?.setOnClickListener {
                dialog?.dismiss()
//                val intent = Intent(mContext, ProviderDetailsActivity::class.java)
//                intent.putExtra("cat", Cat)
//                intent.putExtra("id", providersModels.get(position).id)
//                intent.putExtra("lat",mLat)
//                intent.putExtra("lng",mLang)
//                startActivity(intent)
                //finish()
            }
            dialog?.show()
        }else {
            val intent = Intent(mContext, ProviderDetailsActivity::class.java)
            intent.putExtra("cat", Cat)
            intent.putExtra("id", providersModels.get(position).id)
            intent.putExtra("lat", mLat)
            intent.putExtra("lng", mLang)
            startActivity(intent)
        }
    }

    override fun onMarkerClick(p0: Marker): Boolean {
        if (p0!!.getTitle() != "موقعي") {
            if (hmap.containsKey(p0)) {
                val myShopModel = hmap.get(p0)

                val customInfoWindowOfMap = CustomInfoWindowOfMap(mContext, myShopModel!!)
                googleMap!!.setInfoWindowAdapter(customInfoWindowOfMap)
                googleMap!!.setOnInfoWindowClickListener {
                    if (myShopModel!!.closed==1){
                        val dialog = Dialog(mContext)
                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                        dialog ?.setCancelable(true)
                        dialog ?.setContentView(R.layout.dialog_sorry)


                        val ok = dialog?.findViewById<Button>(R.id.ok)
                        val text = dialog?.findViewById<TextView>(R.id.text)
                        text.text = getString(R.string.Service_provider_outside_working_hours)

                        ok?.setOnClickListener {
                            dialog?.dismiss()
//                            val intent = Intent(mContext, ProviderDetailsActivity::class.java)
//                            intent.putExtra("cat", Cat)
//                            intent.putExtra("id", myShopModel!!.id)
//                            intent.putExtra("lat",mLat)
//                            intent.putExtra("lng",mLang)
//                            startActivity(intent)

                        }
                        dialog?.show()
                    }
                    else if (myShopModel!!.maintenance==1){
                        val dialog = Dialog(mContext)
                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                        dialog ?.setCancelable(true)
                        dialog ?.setContentView(R.layout.dialog_sorry)


                        val ok = dialog?.findViewById<Button>(R.id.ok)
                        val text = dialog?.findViewById<TextView>(R.id.text)
                        text.text = getString(R.string.The_service_provider_is_under_maintenance)

                        ok?.setOnClickListener {
                            dialog?.dismiss()
//                            val intent = Intent(mContext, ProviderDetailsActivity::class.java)
//                            intent.putExtra("cat", Cat)
//                            intent.putExtra("id", myShopModel!!.id)
//                            intent.putExtra("lat",mLat)
//                            intent.putExtra("lng",mLang)
//                            startActivity(intent)

                        }
                        dialog?.show()
                    }
                    else {
                        val intent = Intent(mContext, ProviderDetailsActivity::class.java)
                        intent.putExtra("cat", Cat)
                        intent.putExtra("id", myShopModel!!.id)
                        intent.putExtra("lat", mLat)
                        intent.putExtra("lng", mLang)
                        startActivity(intent)
                    }
                }

            } else {
                googleMap!!.setInfoWindowAdapter(null)
            }
        } else {
            googleMap!!.setInfoWindowAdapter(null)
            googleMap!!.setOnInfoWindowClickListener { }
        }
        return false
    }
    internal var hmap = HashMap<Marker, ProviderModel>()
    override fun onMapReady(p0: GoogleMap) {
        this.googleMap = p0!!
        googleMap!!.setOnMarkerClickListener(this)
        getLocationWithPermission(top,nearest,mostest,null)
    }



    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
               // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override val layoutResource: Int
        get() = R.layout.activity_providers
    private var mAlertDialog: AlertDialog? = null
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var search:ImageView

    lateinit var notification:ImageView
    lateinit var sort_lay:LinearLayout
    lateinit var show_lay:LinearLayout
    lateinit var sort:TextView
    lateinit var show:TextView
    lateinit var viewPager: CardSliderViewPager
    lateinit var normal:LinearLayout
    lateinit var map:MapView
    lateinit var sorted_lay:LinearLayout
    lateinit var high:TextView
    lateinit var near:TextView
    lateinit var most:TextView
    lateinit var showing_lay:LinearLayout
    lateinit var list:TextView
    lateinit var schedule:TextView
    lateinit var mapping:TextView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
     var top: Int?=1
    var nearest: Int?=null
    var mostest: Int?=null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var providersAdapter:ProvidersAdapter
    var providersModels = ArrayList<ProviderModel>()
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var Cat: CatsModel
    var providers = ArrayList<ProviderModel>()
    internal lateinit var markerOptions: MarkerOptions
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initializeComponents() {
        Cat = intent.getSerializableExtra("cat") as CatsModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        search = findViewById(R.id.search)
        notification = findViewById(R.id.notification)
        sort_lay = findViewById(R.id.sort_lay)
        show_lay = findViewById(R.id.show_lay)
        sort = findViewById(R.id.sort)
        show = findViewById(R.id.show)
        viewPager = findViewById(R.id.viewPager)
        normal = findViewById(R.id.normal)
        map = findViewById(R.id.map)
        sorted_lay = findViewById(R.id.sorted_lay)
        high = findViewById(R.id.high)
        near = findViewById(R.id.near)
        most = findViewById(R.id.most)
        showing_lay = findViewById(R.id.showing_lay)
        list = findViewById(R.id.list)
        schedule = findViewById(R.id.schedule)
        mapping = findViewById(R.id.mapping)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        notification.setOnClickListener {
            if (user.loginStatus!!){
                startActivity(Intent(this,NotificationActivity::class.java))
                finish()
            }else{

            }
        }

        swipeRefresh = findViewById(R.id.swipe_refresh)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = Cat.name
        swipeRefresh!!.isRefreshing = false
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        sort_lay.setOnClickListener {
            Log.e("rrrr","tttttttt")
            sorted_lay.visibility = View.VISIBLE
            showing_lay.visibility = View.GONE
            sort_lay.background = mContext.getDrawable(R.drawable.offwhite_yellow_shape)
        }
        show_lay.setOnClickListener {
            sorted_lay.visibility = View.GONE
            showing_lay.visibility = View.VISIBLE
            show_lay.background = mContext.getDrawable(R.drawable.offwhite_yellow_shape)
        }

        search.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_search)
            val text = dialog?.findViewById<EditText>(R.id.text)
            val done = dialog?.findViewById<Button>(R.id.done)
            done.setOnClickListener {
                dialog?.dismiss()

                if (text.text.trim().equals("")){
                    Log.e("ttt","gggg")
                }else{
                    getLocationWithPermission(top,nearest,mostest,text.text.toString())
                }
            }
            dialog?.show()

        }

//        text.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(p0: Editable?) {
//
//            }
//
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//
//            }
//
//            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                getLocationWithPermission(null,null,null,p0.toString())
//            }
//
//        })


        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider)
        providersAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = providersAdapter

        gridLayoutManager = GridLayoutManager(mContext,2)
        schedule.setOnClickListener {
            normal.visibility = View.VISIBLE
            map.visibility = View.GONE
            show.text = getString(R.string.Schedule)
            sorted_lay.visibility = View.GONE
            showing_lay.visibility = View.GONE
            show_lay.background = mContext.getDrawable(R.drawable.offwhite_shape)
            providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider_sc)
            providersAdapter.setOnItemClickListener(this)
            rv_recycle.layoutManager = gridLayoutManager
            rv_recycle.adapter = providersAdapter
            getLocationWithPermission(top,nearest,mostest,null)
        }
        list.setOnClickListener {
            normal.visibility = View.VISIBLE
            map.visibility = View.GONE
            show.text = getString(R.string.list)
            sorted_lay.visibility = View.GONE
            showing_lay.visibility = View.GONE
            show_lay.background = mContext.getDrawable(R.drawable.offwhite_shape)
            providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider)
            providersAdapter.setOnItemClickListener(this)
            rv_recycle.layoutManager = linearLayoutManager
            rv_recycle.adapter = providersAdapter
            getLocationWithPermission(top,nearest,mostest,null)
        }
        mapping.setOnClickListener { normal.visibility = View.GONE
            map.visibility = View.VISIBLE
            show.text = getString(R.string.map)
            sorted_lay.visibility = View.GONE
            showing_lay.visibility = View.GONE
            show_lay.background = mContext.getDrawable(R.drawable.offwhite_shape)

        }
        high.setOnClickListener {
            sorted_lay.visibility = View.GONE
            showing_lay.visibility = View.GONE
            show_lay.background = mContext.getDrawable(R.drawable.offwhite_shape)
            sort.text = getString(R.string.highest_rate)
            top = 1
            nearest = null
            mostest = null
            if(show.text.toString().equals(getString(R.string.list))){
                getLocationWithPermission(top,nearest,mostest,null)
                normal.visibility = View.VISIBLE
                map.visibility = View.GONE
                providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider)
                providersAdapter.setOnItemClickListener(this)
                rv_recycle.layoutManager = linearLayoutManager
                rv_recycle.adapter = providersAdapter

            }else if(show.text.toString().equals(getString(R.string.Schedule))){
                getLocationWithPermission(top,nearest,mostest,null)
                normal.visibility = View.VISIBLE
                map.visibility = View.GONE
                providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider_sc)
                providersAdapter.setOnItemClickListener(this)
                rv_recycle.layoutManager = gridLayoutManager
                rv_recycle.adapter = providersAdapter

            }else{
                getLocationWithPermission(top,nearest,mostest,null)
                map.visibility = View.VISIBLE
                show.text = getString(R.string.map)
                sorted_lay.visibility = View.GONE
                showing_lay.visibility = View.GONE
            }
        }
        near.setOnClickListener {
            sorted_lay.visibility = View.GONE
            showing_lay.visibility = View.GONE
            show_lay.background = mContext.getDrawable(R.drawable.offwhite_shape)
            sort.text = getString(R.string.closest)
            top = null
            nearest = 1
            mostest = null
            if(show.text.toString().equals(getString(R.string.list))){
                getLocationWithPermission(top,nearest,mostest,null)
                normal.visibility = View.VISIBLE
                map.visibility = View.GONE
                providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider)
                providersAdapter.setOnItemClickListener(this)
                rv_recycle.layoutManager = linearLayoutManager
                rv_recycle.adapter = providersAdapter

            }else if(show.text.toString().equals(getString(R.string.Schedule))){
                getLocationWithPermission(top,nearest,mostest,null)
                normal.visibility = View.VISIBLE
                map.visibility = View.GONE
                providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider_sc)
                providersAdapter.setOnItemClickListener(this)
                rv_recycle.layoutManager = gridLayoutManager
                rv_recycle.adapter = providersAdapter

            }else{
                getLocationWithPermission(top,nearest,mostest,null)
                map.visibility = View.VISIBLE
                show.text = getString(R.string.map)
                sorted_lay.visibility = View.GONE
                showing_lay.visibility = View.GONE
            }
        }
        most.setOnClickListener {
            sorted_lay.visibility = View.GONE
            showing_lay.visibility = View.GONE
            show_lay.background = mContext.getDrawable(R.drawable.offwhite_shape)
            sort.text = getString(R.string.wanted)
            top = null
            nearest = null
            mostest = 1
            if(show.text.toString().equals(getString(R.string.list))){
                getLocationWithPermission(top,nearest,mostest,null)
                normal.visibility = View.VISIBLE
                map.visibility = View.GONE
                providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider)
                providersAdapter.setOnItemClickListener(this)
                rv_recycle.layoutManager = linearLayoutManager
                rv_recycle.adapter = providersAdapter

            }else if(show.text.toString().equals(getString(R.string.Schedule))){
                getLocationWithPermission(top,nearest,mostest,null)
                normal.visibility = View.VISIBLE
                map.visibility = View.GONE
                providersAdapter = ProvidersAdapter(mContext,providersModels,R.layout.recycler_provider_sc)
                providersAdapter.setOnItemClickListener(this)
                rv_recycle.layoutManager = gridLayoutManager
                rv_recycle.adapter = providersAdapter

            }else{
                getLocationWithPermission(top,nearest,mostest,null)
                map.visibility = View.VISIBLE
                show.text = getString(R.string.map)
                sorted_lay.visibility = View.GONE
                showing_lay.visibility = View.GONE
            }
        }
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {

            getLocationWithPermission(top, nearest, mostest, null)
        }
        getLocationWithPermission(top, nearest, mostest, null)

    }

    override fun onResume() {
        super.onResume()
        getLocationWithPermission(top, nearest, mostest, null)
    }

//    fun putMapMarker(lat: Double?, log: Double?) {
//        val latLng = LatLng(lat!!, log!!)
//        myMarker = googleMap.addMarker(
//            MarkerOptions()
//                .position(latLng)
//                .title("موقعي")
//                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.loc))
//        )
//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
//        // kkjgj
//
//    }

    fun getLocationWithPermission(top:Int?,near:Int?,most:Int?,text:String?) {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(top,near,most,text)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(top,near,most,text)
        }

    }

    internal fun getCurrentLocation(top:Int?,near:Int?,most:Int?,text:String?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
               // putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.getLatitude().toString(),gps.getLongitude().toString(),top,near,most,text)
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                       // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }

    fun getData(lat:String,lng:String,top:Int?,near:Int?,most:Int?,text1:String?){

        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getProviders(lang.appLanguage,Cat.id!!,lat,lng,top,near,most,text1)?.enqueue(object:
            Callback<ProvidersResponse> {
            override fun onFailure(call: Call<ProvidersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ProvidersResponse>,
                response: Response<ProvidersResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        initSliderAds(response.body()?.banners!!)
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            googleMap!!.clear()

                        } else {
//
                            providersAdapter.updateAll(response.body()!!.data!!)

                            for (i in 0..response.body()!!.data?.size!!-1){
                                addShop(response.body()!!.data?.get(i)!!)
                            }

                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun initSliderAds(list:ArrayList<BannersModel>){
        if(list.isEmpty()){
            viewPager.visibility=View.GONE
        }
        else{
            viewPager.visibility=View.VISIBLE
            viewPager.adapter= SliderAdapter(mContext!!,list)
        }
    }

    internal fun addShop(shopModel: ProviderModel) {
        markerOptions = MarkerOptions().position(
            LatLng(java.lang.Float.parseFloat(shopModel.lat!!).toDouble(), java.lang.Float.parseFloat(shopModel.lng!!).toDouble()))
            .title(shopModel.name)
            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.map))
        myMarker = googleMap!!.addMarker(markerOptions)!!
        googleMap!!.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(java.lang.Double.parseDouble(shopModel.lat!!), java.lang.Double.parseDouble(shopModel.lng!!)), 8f)
        )
        hmap[myMarker] = shopModel

    }


}