package com.aait.ahjezclient.UI.Fragments

import android.content.Intent
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.BaseFragment
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.BaseResponse
import com.aait.ahjezclient.Models.ReservationModel
import com.aait.ahjezclient.Models.ReservationResponse
import com.aait.ahjezclient.Models.TermsResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.ConfirmOrderActivity
import com.aait.ahjezclient.UI.Activities.Main.OrderPayActivity
import com.aait.ahjezclient.UI.Activities.Main.ReservationDetailsActivity
import com.aait.ahjezclient.UI.Controllers.OrdersAdapter
import com.aait.ahjezclient.UI.Controllers.OrdersDelAdapter
import com.aait.ahjezclient.UI.Controllers.ReservationAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PendingFragment : BaseFragment(), OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.app_recycle
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    var reservationModels = ArrayList<ReservationModel>()
    lateinit var reservationAdapter: OrdersDelAdapter
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL,false)
        reservationAdapter = OrdersDelAdapter(mContext!!,reservationModels,R.layout.recycle_delete_order)
        reservationAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = reservationAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()
    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Orders("Bearer"+user.userData.token,lang.appLanguage,"pending_payment")?.enqueue(object:
            Callback<ReservationResponse> {
            override fun onFailure(call: Call<ReservationResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ReservationResponse>,
                response: Response<ReservationResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            reservationAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.cancelled){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DeleteOrder(
                "Bearer"+user.userData.token,lang.appLanguage
                ,reservationModels.get(position).id!!
            )?.enqueue(object :
                Callback<TermsResponse> {
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext!!, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            CommonUtil.makeToast(mContext!!, response.body()?.data!!)
                            getData()
                        } else {
                            CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                        }
                    }
                }

            })
        }else {
            val intent = Intent(activity, OrderPayActivity::class.java)
            intent.putExtra("id", reservationModels.get(position).id)
            startActivity(intent)
        }
    }
}