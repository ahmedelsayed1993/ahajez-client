package com.aait.ahjezclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.MainActivity

class IntroFiveActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_intro_one
    lateinit var image: ImageView
    lateinit var title: TextView
    lateinit var next: Button
    lateinit var skip: TextView
    lateinit var one: ImageView
    lateinit var four: ImageView
    override fun initializeComponents() {
        image = findViewById(R.id.image)
        title =findViewById(R.id.title)
        one = findViewById(R.id.one)
        four = findViewById(R.id.four)
        next = findViewById(R.id.next)
        skip = findViewById(R.id.skip)
        one.setImageResource(R.drawable.gray_circle)
        four.setImageResource(R.drawable.black_circle)
        image.setImageResource(R.mipmap.three)
        title.text = getString(R.string.Search_and_ask_while_you_are_comfortable)
        next.setOnClickListener { startActivity(Intent(this,IntroSixActivity::class.java)) }
        skip.setOnClickListener { val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("state","normal")
            startActivity(intent)
            finish()  }
    }
}