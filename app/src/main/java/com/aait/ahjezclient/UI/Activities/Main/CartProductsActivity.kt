package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.CartModel
import com.aait.ahjezclient.Models.FavRespopnse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Auth.LoginActivity
import com.aait.ahjezclient.UI.Controllers.CartProductsAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartProductsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_cart_products
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var products:RecyclerView
    lateinit var products_value:TextView
    lateinit var tax:TextView
    lateinit var tax_value:TextView
    lateinit var total:TextView
    lateinit var check:CheckBox
    lateinit var confirm:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var cartProductsAdapter: CartProductsAdapter
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    var providers = ArrayList<ProviderModelOffline>()
    var id = 0
    var tot = 0.0
    var value = 0.0
    var ta = 0.0
    var count = 0
    var price = 0.0
    var cart = ArrayList<CartModel>()
    var tax_val = ""
    var carts = ArrayList<CartModel>()
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        tax_val = intent.getStringExtra("tax")!!
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        back.setOnClickListener { onBackPressed()
            finish()}
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        title.text = getString(R.string.shopping_basket)
        products = findViewById(R.id.products)
        products_value = findViewById(R.id.products_value)
        tax = findViewById(R.id.tax)
        tax_value = findViewById(R.id.tax_value)
        total = findViewById(R.id.total)
        check = findViewById(R.id.check)
        confirm = findViewById(R.id.confirm)
        providers.clear()
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        cartProductsAdapter = CartProductsAdapter(mContext,ArrayList<ProviderModelOffline>(),R.layout.recycle_cart_product)
        cartProductsAdapter.setOnItemClickListener(this)
        products.layoutManager = linearLayoutManager
        products.adapter = cartProductsAdapter
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        //allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)


        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){


                }else {

                    for (i in 0..cartModels.size-1){
                       // providers.clear()
                       // cart.clear()
                        if (cartModels.get(i).provider_id==id) {
                            providers.add(cartModels.get(i))

                        }

                    }
                    Log.e("car", Gson().toJson(providers))

                    cartProductsAdapter.updateAll(providers.distinctBy { it.product_id })


                    for (i in 0..cartProductsAdapter.data.size-1){
                        value = value+(cartProductsAdapter.data.get(i).price!!.toDouble()*cartProductsAdapter.data.get(i).count!!)
                        cart.add(CartModel(cartProductsAdapter.data.get(i).product_id,cartProductsAdapter.data.get(i).count,cartProductsAdapter.data.get(i).price,cartProductsAdapter.data.get(i).description))

                    }
                    Log.e("valuee",value.toString())
                    Log.e("cart", Gson().toJson(cart.distinctBy { it.product_id }))
                    products_value.text = value.toString()+getString(R.string.rs)
                    ta = (value*tax_val.toDouble())/100
                    tax.text = getString(R.string.The_tax_amount)+tax_val+"%"
                    tax_value.text = ta.toString()+getString(R.string.rs)
                    tot = value+ta
                    total.text = tot.toString()+getString(R.string.rs)
                    if (providers.get(0).delivery==0){
                      check.visibility = View.GONE
                    }else{
                        check.visibility = View.VISIBLE
                    }


                }


            }
        })

        confirm.setOnClickListener {
            if (user.loginStatus!!) {
                if (check.isChecked) {

                    val intent = Intent(this, OrderLocationActivity::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra(
                        "cart",
                        cart.distinctBy { it.product_id } as ArrayList<CartModel>)
                    intent.putExtra("tax", ta)
                    intent.putExtra("value", value)
                    intent.putExtra("tax_val", tax_val)
                    intent.putExtra("total", tot)
                    startActivity(intent)
                } else {
                    AddOrder()
                }
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
                startActivity(Intent(this,LoginActivity::class.java))
                finish()

            }
        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.add){
            if (providers.get(position).max==providers.get(position).count){

            }else{
                value = 0.0
                    Log.e("value",value.toString())

                    cartModelOfflines.get(position).count = cartModelOfflines.get(position).count!!+1
                    cartProductsAdapter.notifyDataSetChanged()
                    allCartViewModel!!.updateItem(cartModelOfflines.get(position))

                finish()
                val intent = Intent(this,CartProductsActivity::class.java)
                intent.putExtra("id",id)
                intent.putExtra("tax",tax_val)
                startActivity(intent)

                Log.e("count",count.toString())


            }
        }else if (view.id == R.id.minus){
            if (providers.get(position).count==1){
               allCartViewModel!!.deleteItem(cartModelOfflines.get(position))
               CommonUtil.makeToast(mContext,getString(R.string.product_deleted))
                startActivity(Intent(this,BasketActivity::class.java))
                finish()
            }
            else{
                value = 0.0
                Log.e("value",value.toString())

                cartModelOfflines.get(position).count = cartModelOfflines.get(position).count!!-1
                cartProductsAdapter.notifyDataSetChanged()
                allCartViewModel!!.updateItem(cartModelOfflines.get(position))

                finish()
                val intent = Intent(this,CartProductsActivity::class.java)
                intent.putExtra("id",id)
                intent.putExtra("tax",tax_val)
                startActivity(intent)
            }

        }else if (view.id == R.id.cancel){
            allCartViewModel!!.deleteItem(cartModelOfflines.get(position))
            CommonUtil.makeToast(mContext,getString(R.string.product_deleted))
            startActivity(Intent(this,BasketActivity::class.java))
            finish()
        }

    }
    fun AddOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddOrder("Bearer" + user.userData.token,lang.appLanguage,id
            ,Gson().toJson(cart.distinctBy { it.product_id }),value.toString(),ta.toString(),tax_val,tot.toString(),null,null,null,null,"",0)
            ?.enqueue(object :Callback<FavRespopnse>{
                override fun onFailure(call: Call<FavRespopnse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<FavRespopnse>,
                    response: Response<FavRespopnse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
//                            allCartViewModel!!.allCart.observe(this@CartProductsActivity, object : Observer<List<ProviderModelOffline>> {
//                                override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
//                                    cartModelOfflines = cartModels
//
//                                    if(cartModels.size==0){
//
//
//                                    }else {
//
//                                        for (i in 0..cartModels.size-1){
//                                            // providers.clear()
//                                            // cart.clear()
//                                            allCartViewModel!!.deleteItem(cartModels.get(i))
//
//                                        }
//                                    }
//
//
//                                }
//                            })

                            val intent = Intent(this@CartProductsActivity,OrderPayActivity::class.java)
                            intent.putExtra("id",response.body()?.data)
                            intent.putExtra("user",response.body()?.user)
                            intent.putExtra("provider",id)
                            startActivity(intent)
                            finish()
                        }
                        else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}


