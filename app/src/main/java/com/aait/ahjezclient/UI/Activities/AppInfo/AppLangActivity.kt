package com.aait.ahjezclient.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.NotificationActivity
import com.aait.ahjezclient.UI.Activities.SplashActivity

class AppLangActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_app_lang
    lateinit var arabic:LinearLayout
    lateinit var english:LinearLayout
    lateinit var one:ImageView
    lateinit var two:ImageView
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        back.setOnClickListener { onBackPressed()
        finish()}
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this, NotificationActivity::class.java))
            finish()}
        title.text = getString(R.string.app_lang)
        if(lang.appLanguage=="ar"){
            one.visibility = View.VISIBLE
            two.visibility = View.GONE
        }else{
            one.visibility = View.GONE
            two.visibility = View.VISIBLE
        }
       arabic.setOnClickListener {
           lang.appLanguage = "ar"
           finishAffinity()
           startActivity(Intent(this, SplashActivity::class.java))
       }
        english.setOnClickListener {
            lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
    }
}