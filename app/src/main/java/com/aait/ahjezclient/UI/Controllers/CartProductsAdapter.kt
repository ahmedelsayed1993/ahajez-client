package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentRecyclerAdapter
import com.aait.ahjezclient.Base.ParentRecyclerViewHolder
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class CartProductsAdapter (context: Context, data: MutableList<ProviderModelOffline>, layoutId: Int) :
    ParentRecyclerAdapter<ProviderModelOffline>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.product_name)
        viewHolder.count.text = questionModel.count.toString()
        viewHolder.price.text = (questionModel.price!!.toDouble()*questionModel.count!!).toString()+mcontext.getString(R.string.rs)


        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_fall_down)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)

        viewHolder.add.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.minus.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

        viewHolder.cancel.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })


    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var count = itemView.findViewById<TextView>(R.id.count)
        internal var minus = itemView.findViewById<ImageView>(R.id.minus)
        internal var add = itemView.findViewById<ImageView>(R.id.add)
        internal var cancel = itemView.findViewById<ImageView>(R.id.cancel)




    }
}