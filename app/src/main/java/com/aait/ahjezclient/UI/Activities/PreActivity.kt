package com.aait.ahjezclient.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.ContactsContract
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.GPS.GPSTracker
import com.aait.ahjezclient.GPS.GpsTrakerListener
import com.aait.ahjezclient.R
import com.aait.ahjezclient.Utils.CommonUtil
import com.aait.ahjezclient.Utils.DialogUtil
import com.aait.ahjezclient.Utils.PermissionUtils
import com.fxn.pix.Pix
import java.io.IOException
import java.util.*

class PreActivity:ParentActivity()
   // , GpsTrakerListener
{
    override val layoutResource: Int
        get() = R.layout.activity_pre
    lateinit var lang_lay:LinearLayout
    lateinit var light:ImageView
    lateinit var dark:ImageView
    lateinit var next:Button
    private var mAlertDialog: AlertDialog? = null
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    override fun initializeComponents() {
        lang_lay = findViewById(R.id.lang_lay)
        light = findViewById(R.id.light)
        dark = findViewById(R.id.dark)
        next = findViewById(R.id.next)

//        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
//            if (!(PermissionUtils.hasPermissions(mContext,
//                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
//                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
//                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                        )) {
//                CommonUtil.PrintLogE("Permission not granted")
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    requestPermissions(
//                        PermissionUtils.IMAGE_PERMISSIONS,
//                        400
//                    )
//                }
//            } else {
//
//                CommonUtil.PrintLogE("Permission is granted before")
//            }
//        } else {
//            CommonUtil.PrintLogE("SDK minimum than 23")
//
//        }
        lang_lay.setOnClickListener { if (lang.appLanguage == "ar"){
        lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }else{
            lang.appLanguage = "ar"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
        }

        light.setOnClickListener {
          //  theme.appTheme =
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

//            finishAffinity()
//            startActivity(Intent(this, SplashActivity::class.java))
        }
        dark.setOnClickListener {
           // theme.appTheme =
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//            finishAffinity()
//            startActivity(Intent(this, SplashActivity::class.java))
        }
        next.setOnClickListener { startActivity(Intent(this,IntroOneActivity::class.java))
            finish()
            }
    }

//    override fun onTrackerSuccess(lat: Double?, log: Double?) {
//        if (startTracker) {
//            if (lat != 0.0 && log != 0.0) {
//                hideProgressDialog()
//                // putMapMarker(lat, log)
//            }
//        }
//    }
//
//    override fun onStartTracker() {
//        startTracker = true
//    }

//    fun getLocationWithPermission() {
//        gps = GPSTracker(mContext, this)
//        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
//            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
//                        (PermissionUtils.hasPermissions(mContext,
//                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
//                CommonUtil.PrintLogE("Permission not granted")
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    requestPermissions(
//                        PermissionUtils.GPS_PERMISSION,
//                        800
//                    )
//                    Log.e("GPS", "1")
//                }
//            } else {
//                getCurrentLocation()
//                Log.e("GPS", "2")
//            }
//        } else {
//            Log.e("GPS", "3")
//            getCurrentLocation()
//        }
//
//    }
//
//    internal fun getCurrentLocation() {
//        gps.getLocation()
//        if (!gps.canGetLocation()) {
//            mAlertDialog = DialogUtil.showAlertDialog(mContext,
//                getString(R.string.gps_detecting),
//                DialogInterface.OnClickListener { dialogInterface, i ->
//                    mAlertDialog?.dismiss()
//                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
//                    startActivityForResult(intent, 300)
//                })
//        } else {
//            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
//                // putMapMarker(gps.getLatitude(), gps.getLongitude())
//
//                mLat = gps.getLatitude().toString()
//                mLang = gps.getLongitude().toString()
//                val addresses: List<Address>
//                geocoder = Geocoder(this, Locale.getDefault())
//                try {
//                    addresses = geocoder.getFromLocation(
//                        java.lang.Double.parseDouble(mLat),
//                        java.lang.Double.parseDouble(mLang),
//                        1
//                    )
//                    if (addresses.isEmpty()) {
//                        Toast.makeText(
//                            mContext,
//                            resources.getString(R.string.detect_location),
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    } else {
//                        result = addresses[0].getAddressLine(0)
//                        Log.e("address",result)
//                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))
//
//
//
//                    }
//
//                } catch (e: IOException) {
//                }
//
//
//                //putMapMarker(gps.getLatitude(), gps.getLongitude())
//
//
//            }
//        }
//    }
}