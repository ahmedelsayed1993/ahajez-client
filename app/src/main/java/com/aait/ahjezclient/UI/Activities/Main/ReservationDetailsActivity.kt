package com.aait.ahjezclient.UI.Activities.Main

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.*
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.ReasonsAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_order_pay.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReservationDetailsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_reservation_details
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var report:ImageView
    lateinit var notification:ImageView
    lateinit var one:ImageView
    lateinit var line:TextView
    lateinit var two:ImageView
    lateinit var line_1:TextView
    lateinit var three:ImageView
    lateinit var order_num:TextView
    lateinit var service_type:TextView
    lateinit var service_price:TextView
    lateinit var paid_amount:TextView
    lateinit var date:TextView
    lateinit var appointment_date:TextView
    lateinit var appointment_time:TextView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var rating:RatingBar
    lateinit var chat:ImageView
    lateinit var ratee:Button
    lateinit var lay:LinearLayout
    lateinit var lay1:LinearLayout
    lateinit var address:TextView
    lateinit var cancel:Button
    var id = 0
    lateinit var reasonsAdapter: ReasonsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var linearLayoutManager1: LinearLayoutManager
    var reasonsModels = ArrayList<CancelModel>()
    var swipeRefresh: SwipeRefreshLayout? = null
    var reason = 0
    var reciver = 0
    var photo = ""
    var provider = ""
    lateinit var comment_lay:LinearLayout
    lateinit var rate:RatingBar
    lateinit var comment:TextView
    lateinit var text:TextView
    lateinit var resons:TextView
//    var TextView.background: Int
//        get() = currentTextColor
//        set(v) = setTextColor(v)
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        chat = findViewById(R.id.chat)
    comment_lay = findViewById(R.id.comment_lay)
    comment = findViewById(R.id.comment)
    rate = findViewById(R.id.rate)
        ratee = findViewById(R.id.ratee)
        report = findViewById(R.id.report)
    text = findViewById(R.id.text)
    resons = findViewById(R.id.reason)
        lay = findViewById(R.id.lay)
        lay1 = findViewById(R.id.lay1)
        notification = findViewById(R.id.notification)
        one = findViewById(R.id.one)
        line = findViewById(R.id.line)
        two = findViewById(R.id.two)
        line_1 = findViewById(R.id.line_1)
        three = findViewById(R.id.three)
        order_num = findViewById(R.id.order_num)
        service_type = findViewById(R.id.service_type)
        service_price = findViewById(R.id.service_price)
        paid_amount = findViewById(R.id.paid_amount)
        date = findViewById(R.id.date)
        appointment_date = findViewById(R.id.appointment_date)
        appointment_time = findViewById(R.id.appointment_time)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        address = findViewById(R.id.address)
        cancel = findViewById(R.id.cancel)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.reservation_details)
    notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
    finish()}

        report.setOnClickListener {

            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.cancel_dialog)
            val reasons = dialog?.findViewById<RecyclerView>(R.id.reasons)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            val titl = dialog?.findViewById<TextView>(R.id.titl)
            titl.text = getString(R.string.reason_of_reporting)
            linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
            reasonsAdapter = ReasonsAdapter(mContext,reasonsModels,R.layout.recycle_reason)
            reasonsAdapter.setOnItemClickListener(this)
            reasons.layoutManager = linearLayoutManager1
            reasons.adapter = reasonsAdapter
            getReports()
            send.setOnClickListener {  showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Repot("Bearer"+user.userData.token,lang.appLanguage,reason,id,"reservation")
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog?.dismiss()
                        }

                        override fun onResponse(
                            call: Call<TermsResponse>,
                            response: Response<TermsResponse>
                        ) {
                            hideProgressDialog()
                            dialog?.dismiss()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    val dialog = Dialog(mContext)
                                    dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                    dialog ?.setCancelable(true)
                                    dialog ?.setContentView(R.layout.dailog_done)
                                    val text = dialog?.findViewById<TextView>(R.id.text)
                                    val back = dialog?.findViewById<Button>(R.id.ok)

                                    text.text = getString(R.string.report_done)


                                    back.setOnClickListener { dialog.dismiss()
                                        val intent = Intent(this@ReservationDetailsActivity,MainActivity::class.java)
                                        intent.putExtra("state","normal")
                                        startActivity(intent)}
                                    dialog?.show()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })}
            cancel.setOnClickListener { dialog.dismiss() }
            dialog?.show()
//            val dialog = Dialog(mContext)
//            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
//            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
//            dialog ?.setCancelable(true)
//            dialog ?.setContentView(R.layout.cancel_dialog)
//            val reasons = dialog?.findViewById<RecyclerView>(R.id.reasons)
//            val send = dialog?.findViewById<Button>(R.id.send)
//            val cancel = dialog?.findViewById<Button>(R.id.cancel)
//            val titl = dialog?.findViewById<TextView>(R.id.titl)
//            titl.text = getString(R.string.reason_of_reporting)
//            linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
//            reasonsAdapter = ReasonsAdapter(mContext,reasonsModels,R.layout.recycle_reason)
//            reasonsAdapter.setOnItemClickListener(this)
//            reasons.layoutManager = linearLayoutManager
//            reasons.adapter = reasonsAdapter
//            getReasons()
//            send.setOnClickListener {  showProgressDialog(getString(R.string.please_wait))
//                Client.getClient()?.create(Service::class.java)?.ReservationDetails("Bearer"+user.userData.token,lang.appLanguage,id,"report",reason)
//                    ?.enqueue(object :Callback<ReservationDetailsResponse>{
//                        override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
//                            CommonUtil.handleException(mContext,t)
//                            t.printStackTrace()
//                            hideProgressDialog()
//                            dialog?.dismiss()
//                        }
//
//                        override fun onResponse(
//                            call: Call<ReservationDetailsResponse>,
//                            response: Response<ReservationDetailsResponse>
//                        ) {
//                            hideProgressDialog()
//                            dialog?.dismiss()
//                            if (response.isSuccessful){
//                                if (response.body()?.value.equals("1")){
//                                    val dialog = Dialog(mContext)
//                                    dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
//                                    dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
//                                    dialog ?.setCancelable(true)
//                                    dialog ?.setContentView(R.layout.dailog_done)
//                                    val text = dialog?.findViewById<TextView>(R.id.text)
//                                    val back = dialog?.findViewById<Button>(R.id.ok)
//
//                                        text.text = getString(R.string.report_done)
//
//
//                                    back.setOnClickListener { dialog.dismiss()
//                                        val intent = Intent(this@ReservationDetailsActivity,MainActivity::class.java)
//                                        intent.putExtra("state","normal")
//                                        startActivity(intent)}
//                                    dialog?.show()
//                                }else{
//                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
//                                }
//                            }
//                        }
//
//                    })}
//            cancel.setOnClickListener { dialog.dismiss() }
//            dialog?.show()
         }

    ratee.setOnClickListener {  val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_rate)
            val pro_image = dialog?.findViewById<CircleImageView>(R.id.pro_image)
            val pro_name = dialog?.findViewById<TextView>(R.id.pro_name)
            val rate = dialog?.findViewById<RatingBar>(R.id.rate)
            val comment = dialog?.findViewById<EditText>(R.id.comment)
            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            Glide.with(mContext).load(photo).into(pro_image)
            pro_name.text = provider
            confirm.setOnClickListener {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.AddRate("Bearer"+user.userData.token,lang.appLanguage,id,"provider",rate.rating.toInt(),"reservation",comment.text.toString())
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog?.dismiss()
                        }
                        override fun onResponse(
                            call: Call<TermsResponse>,
                            response: Response<TermsResponse>
                        ) {
                            hideProgressDialog()
                            dialog?.dismiss()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    val intent = Intent(this@ReservationDetailsActivity,MainActivity::class.java)
                                    intent.putExtra("state","normal")
                                    startActivity(intent)
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }
                    })
               }
            dialog?.show() }
        cancel.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.cancel_dialog)
            val reasons = dialog?.findViewById<RecyclerView>(R.id.reasons)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            val titl = dialog?.findViewById<TextView>(R.id.titl)
            titl.text = getString(R.string.cancel_reason)
            linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
            reasonsAdapter = ReasonsAdapter(mContext,reasonsModels,R.layout.recycle_reason)
            reasonsAdapter.setOnItemClickListener(this)
            reasons.layoutManager = linearLayoutManager
            reasons.adapter = reasonsAdapter
            getReasons()
            send.setOnClickListener { showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.ReservationDetails("Bearer"+user.userData.token,lang.appLanguage,id,"cancel",reason)
                    ?.enqueue(object :Callback<ReservationDetailsResponse>{
                        override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog?.dismiss()
                        }

                        override fun onResponse(
                            call: Call<ReservationDetailsResponse>,
                            response: Response<ReservationDetailsResponse>
                        ) {
                            hideProgressDialog()
                            dialog?.dismiss()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    val dialog = Dialog(mContext)
                                    dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                    dialog ?.setCancelable(true)
                                    dialog ?.setContentView(R.layout.dailog_done)
                                    val text = dialog?.findViewById<TextView>(R.id.text)
                                    val back = dialog?.findViewById<Button>(R.id.ok)

                                        text.text = getString(R.string.cancel_done)

                                    //getReasons()

                                    back.setOnClickListener { dialog.dismiss()
                                        val intent = Intent(this@ReservationDetailsActivity,MainActivity::class.java)
                                        intent.putExtra("state","normal")
                                        startActivity(intent)}
                                    dialog?.show()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    }) }
            cancel.setOnClickListener { dialog.dismiss() }
            dialog?.show()
        }
    chat.setOnClickListener { getID() }
    swipeRefresh!!.setColorSchemeResources(
        R.color.colorPrimary,
        R.color.colorPrimaryDark,
        R.color.colorAccent
    )
    swipeRefresh!!.setOnRefreshListener {
        getData()

    }
        getData()

    }
    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Conversation("Bearer"+user.userData.token,lang.appLanguage,reciver)?.enqueue(object :
            Callback<ConversationIdResponse>{
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }
            override fun onResponse(
                call: Call<ConversationIdResponse>,
                response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@ReservationDetailsActivity,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ReservationDetails("Bearer"+user.userData.token,lang.appLanguage,id,null,null)
            ?.enqueue(object :Callback<ReservationDetailsResponse>{
                override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                }

                @SuppressLint("ResourceAsColor")
                override fun onResponse(
                    call: Call<ReservationDetailsResponse>,
                    response: Response<ReservationDetailsResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            order_num.text = response.body()?.data?.id.toString()
                            service_type.text = response.body()?.data?.name_service
                            service_price.text = response.body()?.data?.service_price+getString(R.string.rs)
                            paid_amount.text = response.body()?.data?.total+getString(R.string.rs)
                            date.text = response.body()?.data?.created
                            resons.text = getString(R.string.reason_of_cancellation)+response.body()?.data?.cancellation
                            appointment_date.text = response.body()?.data?.date
                            appointment_time.text = response.body()?.data?.time
                            Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                            name.text = response.body()?.data?.name
                            rating.rating = response.body()?.data?.rate!!
                            address.text = response.body()?.data?.address
                            provider = response.body()?.data?.name!!
                            photo = response.body()?.data?.avatar!!
                            reciver  = response.body()?.data?.chat_provider!!
//                            if (response.body()?.data?.payment_free==1){
//                                cancel.visibility = View.VISIBLE
//                            }else{
//                                cancel.visibility = View.GONE
//                            }
                             if (response.body()?.data?.refundable==1){
                                cancel.visibility = View.VISIBLE
                            }else{
                                cancel.visibility = View.GONE
                            }
                             if (response.body()?.data?.order_rate==0){
                                comment_lay.visibility = View.GONE
                            }else{
                                comment_lay.visibility = View.VISIBLE
                                comment.text = response.body()?.data?.order_comment
                                rate.rating = response.body()?.data?.order_rate!!.toFloat()
                            }
                            if (response.body()?.data?.status!!.equals("current")){
                                one.setImageResource(R.mipmap.check)
                               // line.setBackgroundColor(R.color.colorYellow)
                                line.background = mContext.resources.getDrawable(R.color.colorYellow)
                                ratee.visibility = View.GONE
                            }else if (response.body()?.data?.status!!.equals("accepted")){
                                one.setImageResource(R.mipmap.check)
                                // line.setBackgroundColor(R.color.colorYellow)
                                line.background = mContext.resources.getDrawable(R.color.colorYellow)
                                two.setImageResource(R.mipmap.check)
                                // line.setBackgroundColor(R.color.colorYellow)
                                line_1.background = mContext.resources.getDrawable(R.color.colorYellow)
                                ratee.visibility = View.GONE
                            }else if (response.body()?.data?.status!!.equals("completed")){
                                one.setImageResource(R.mipmap.check)
                                // line.setBackgroundColor(R.color.colorYellow)
                                line.background = mContext.resources.getDrawable(R.color.colorYellow)
                                two.setImageResource(R.mipmap.check)
                                // line.setBackgroundColor(R.color.colorYellow)
                                line_1.background = mContext.resources.getDrawable(R.color.colorYellow)
                                three.setImageResource(R.mipmap.check)
                                cancel.visibility = View.GONE
                                ratee.visibility = View.VISIBLE
                            }else if (response.body()?.data?.status!!.equals("refuse")||response.body()?.data?.status.equals("cancel")){
                                cancel.visibility = View.GONE
                                ratee.visibility = View.GONE
                                lay.visibility = View.GONE
                                lay1.visibility = View.GONE
                                text.visibility = View.VISIBLE
                                resons.visibility = View.VISIBLE

                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.reason){
            reasonsAdapter.selected = position
            reasonsModels.get(position).selected = true
            reasonsAdapter.notifyDataSetChanged()
            reason = reasonsModels.get(position).id!!
            Log.e("reason",reason.toString())

        }

    }
    fun getReasons(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cancellation("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object :Callback<CancelResponse>{
            override fun onFailure(call: Call<CancelResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<CancelResponse>,
                response: Response<CancelResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        reasonsAdapter.updateAll(response.body()?.data!!)
                        if (response.body()?.data!!.size!=0){
                           reason = response.body()?.data?.get(0)?.id!!
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getReports(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Reports("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object :Callback<CancelResponse>{
            override fun onFailure(call: Call<CancelResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<CancelResponse>,
                response: Response<CancelResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        reasonsAdapter.updateAll(response.body()?.data!!)
                        if (response.body()?.data!!.size!=0){
                            reason = response.body()?.data?.get(0)?.id!!
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}