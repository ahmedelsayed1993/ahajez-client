package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.ahjezclient.Base.ParentRecyclerAdapter
import com.aait.ahjezclient.Base.ParentRecyclerViewHolder
import com.aait.ahjezclient.Models.CancelModel
import com.aait.ahjezclient.Models.TimesModel
import com.aait.ahjezclient.R

class ReasonsAdapter (context: Context, data: MutableList<CancelModel>, layoutId: Int) :
    ParentRecyclerAdapter<CancelModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected :Int = 0

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        questionModel.selected = (selected==position)

       viewHolder.name.setChecked(selected == position)
        viewHolder.name.setTag(position)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)

        viewHolder.name.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<RadioButton>(R.id.reason)





    }
}