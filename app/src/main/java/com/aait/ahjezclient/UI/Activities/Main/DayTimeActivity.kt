package com.aait.ahjezclient.UI.Activities.Main

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.FavRespopnse
import com.aait.ahjezclient.Models.TimesDatesResponse
import com.aait.ahjezclient.Models.TimesModel
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.TimesAdapter
import com.aait.ahjezclient.Utils.CommonUtil


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class DayTimeActivity:ParentActivity() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_date_time
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var calender: CalendarView
    lateinit var notification:ImageView
    var day = ""
    lateinit var timesAdapter: TimesAdapter
    lateinit var gridLayoutManager: GridLayoutManager
     var timesModels = ArrayList<TimesModel>()
    lateinit var times:RecyclerView
    lateinit var notes:EditText
    lateinit var confirm:Button
    var id = 0
    var provider = 0
    var time = ""
    var type = ""
    var value = ""
    var dis = ""
    var amount = ""
    var ref = 0
    var free = 0
    var dates = ArrayList<String>()
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        provider = intent.getIntExtra("provider",0)
        type = intent.getStringExtra("type")!!
        value = intent.getStringExtra("value")!!
        dis = intent.getStringExtra("dis")!!
        ref = intent.getIntExtra("ref",0)
        free = intent.getIntExtra("free",0)
        Log.e("total",value)
        Log.e("dis",dis)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        calender = findViewById(R.id.calender)
        times = findViewById(R.id.times)
        notes = findViewById(R.id.notes)
        confirm = findViewById(R.id.confirm)
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        back.setOnClickListener { onBackPressed()
        }
        title.text = getString(R.string.date_of_reservation)
        gridLayoutManager = GridLayoutManager(mContext,4)
        timesAdapter = TimesAdapter(mContext,timesModels,R.layout.recycle_time)
        timesAdapter.setOnItemClickListener(this)
        times.layoutManager = gridLayoutManager
        times.adapter = timesAdapter

        if (type.equals("total")){
            amount = value
        }else{
            amount = dis
        }
        Log.e("amount",amount)
        
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy")
        val formatter1 = DateTimeFormatter.ofPattern("EEEE , dd  MMMM")
        val formatter2 = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        var answer: String =  current.format(formatter2)
        Log.e("date",answer)
        day = answer
        getData(day)

        calender.minDate = Calendar.getInstance().timeInMillis


        calender?.setOnDateChangeListener { view, year, month, dayOfMonth ->
            // Note that months are indexed from 0. So, 0 means January, 1 means february, 2 means march etc.
           // years.text = year.toString()
            if (dayOfMonth<10){
                day = "0"+ dayOfMonth + "-" + (month + 1) + "-" + year
                if (month<9){
                    day = "0"+ dayOfMonth + "-0" + (month + 1) + "-" + year
                }else{
                    day = "0"+ dayOfMonth + "-" + (month + 1) + "-" + year
                }
            }else {
                day = "" + dayOfMonth + "-" + (month + 1) + "-" + year
                if (month<9){
                    day = ""+ dayOfMonth + "-0" + (month + 1) + "-" + year
                }else{
                    day = ""+ dayOfMonth + "-" + (month + 1) + "-" + year
                }
            }

            val originalFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            val targetFormat = SimpleDateFormat("EEEE , dd  MMMM")
            val dat = originalFormat.parse(day)
            getData(day)
            val formattedDate = targetFormat.format(dat)
          //  date.text = formattedDate
            //var dates = LocalDate.parse(date.text,DateTimeFormatter.ofPattern("dd/MM/yyyy"))
            // day = getDayStringOld(dates, Locale.getDefault())
            Log.e("month",day)

        }

        confirm.setOnClickListener {
            if (user.loginStatus!!) {
                if (time.equals("")){
                    CommonUtil.makeToast(mContext,getString(R.string.choose_time))
                }else {
                    SendData()
                }
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
            }
    }
    fun getData(dat:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Times("Bearer"+user.userData.token,lang.appLanguage,provider,id,dat)?.enqueue(object :
            Callback<TimesDatesResponse>{
            override fun onFailure(call: Call<TimesDatesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<TimesDatesResponse>,
                response: Response<TimesDatesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        timesAdapter.updateAll(response.body()?.times!!)
                        if (response.body()?.times!!.isEmpty()){

                        }else {
                            if (response.body()?.times?.get(0)?.checked==0) {
                                time = response.body()?.times?.get(0)?.show_time!!
                            }else{
                                time = ""
                            }
                        }
                        for (i in 0..response.body()?.data!!.size-1){
                            dates.add(response.body()?.data?.get(i)!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if(view.id == R.id.name) {
            if (timesModels.get(position)?.checked == 0) {
                timesAdapter.selected = position
                timesModels.get(position).selected = true
                timesAdapter.notifyDataSetChanged()
                time = timesModels.get(position)?.show_time!!
                Log.e("time", time)
            }
        }

    }

    fun  SendData(){
//        if (dates.contains(day)){
//            CommonUtil.makeToast(mContext,getString(R.string.date_booked))
//        }else {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Reservation(
                "Bearer" + user.userData.token,
                lang.appLanguage,
                provider,
                id,
                type,
                amount,
                day,
                time,
                free,
                notes.text.toString()
            )
                ?.enqueue(object : Callback<FavRespopnse> {
                    override fun onFailure(call: Call<FavRespopnse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<FavRespopnse>,
                        response: Response<FavRespopnse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful) {
                            if (response.body()?.value.equals("1")) {
                                if (ref==0) {
                                    if (free==0){


                                        val dialog = Dialog(mContext)
                                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                        dialog?.window!!.setLayout(
                                            ViewGroup.LayoutParams.MATCH_PARENT,
                                            ViewGroup.LayoutParams.MATCH_PARENT
                                        )
                                        dialog?.setCancelable(true)
                                        dialog?.setContentView(R.layout.dialog_reserve)


                                        val ok = dialog?.findViewById<Button>(R.id.ok)


                                        ok?.setOnClickListener {
                                            dialog?.dismiss()

                                                val intent = Intent(
                                                    this@DayTimeActivity,
                                                    ReservationPayActivity::class.java
                                                )
                                                intent.putExtra("id", response.body()?.data)
                                                intent.putExtra("user",response.body()?.user)
                                                startActivity(intent)
                                                finish()

                                        }
                                        dialog?.show()
                                    } else {
                                        val intent = Intent(this@DayTimeActivity,DoneActivity::class.java)
                                        intent.putExtra("id",response.body()?.data)
                                        startActivity(intent)
                                        finish()
                                    }
                                }else{
                                    if (free ==0) {
                                        val intent = Intent(
                                            this@DayTimeActivity,
                                            ReservationPayActivity::class.java
                                        )
                                        intent.putExtra("id", response.body()?.data)
                                        intent.putExtra("user",response.body()?.user)
                                        startActivity(intent)
                                        finish()
                                    }else{
                                        val intent = Intent(this@DayTimeActivity,DoneActivity::class.java)
                                        intent.putExtra("id",response.body()?.data)
                                        startActivity(intent)
                                        finish()
                                    }
                                }

                            } else {
                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            }
                        }
                    }

                })
        //}
    }
}