package com.aait.ahjezclient.UI.Fragments

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.aait.ahjezclient.Base.BaseFragment
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.OrderTapAdapter
import com.aait.ahjezclient.UI.Controllers.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class PurchaseFargment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_purchase
    companion object {
        fun newInstance(): PurchaseFargment {
            val args = Bundle()
            val fragment = PurchaseFargment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: OrderTapAdapter? = null
    override fun initializeComponents(view: View) {
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter = OrderTapAdapter(mContext!!,childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
    }

}