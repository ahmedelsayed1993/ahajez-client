package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.*
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.ProductAdapter
import com.aait.ahjezclient.UI.Controllers.SubCatsAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_product_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_products
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    lateinit var cart:LinearLayout
    internal var layNoItem: RelativeLayout? = null
    lateinit var number:TextView
    lateinit var total:TextView
    internal var tvNoContent: TextView? = null
    lateinit var notification:ImageView
    var providers = ArrayList<ProviderModelOffline>()
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var sub_cat: RecyclerView
    lateinit var subCatsAdapter: SubCatsAdapter
    var subs = ArrayList<SubSectionModel>()
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var productAdapter: ProductAdapter
    var productModels = ArrayList<ProductModel>()
    var productModels1 = ArrayList<ProductModel>()
    lateinit var providerDetailsModel: ProviderDetailsModel
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    var id = 0
    var count = 0
    var price = 0.0
     var cat=0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        cat = intent.getIntExtra("cat",0)
        providerDetailsModel = intent.getSerializableExtra("provider") as ProviderDetailsModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener {
            startActivity(Intent(this,NotificationActivity::class.java))
            finish()
        }


        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.products)
        cart = findViewById(R.id.cart)
        number = findViewById(R.id.number)
        total = findViewById(R.id.total)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        sub_cat = findViewById(R.id.sub_cat)
        subCatsAdapter = SubCatsAdapter(mContext, subs as MutableList<SubSectionModel>,R.layout.recycler_subsection)
        subCatsAdapter.setOnItemClickListener(this)
        sub_cat.layoutManager = linearLayoutManager
        sub_cat.adapter = subCatsAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        productAdapter = ProductAdapter(mContext,productModels,R.layout.recycler_products)
        productAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = productAdapter
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)

        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){
                    cart.visibility = View.GONE
                }else {
                    cart.visibility = View.VISIBLE
                    price = 0.0
                    count = 0
                    for (i in 0..cartModels.size-1){
                        providers.add(cartModels.get(i))
                        count = count+cartModels.get(i).count!!.toInt()
                        price = price+(cartModels.get(i).price!!.toDouble()*cartModels.get(i).count!!)
                    }
                    number.text = cartModels.size.toString()
                    total.text = getString(R.string.basket)+":"+price.toString()+getString(R.string.rs)
                  Log.e("prov",Gson().toJson(providers.distinctBy { it.provider_id }))

                  Log.e("cart",Gson().toJson(cartModels))


                }


            }
        })
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData(null)

        }
        getData(null)
        cart.setOnClickListener { startActivity(Intent(this,BasketActivity::class.java))
        finish()}
    }
    fun getData(sub:Int?){
        showProgressDialog(getString(R.string.please_wait))
//        subs.clear()
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Products(lang.appLanguage,id,cat,sub)?.enqueue(object:
            Callback<ProductsResponse>{
            override fun onFailure(call: Call<ProductsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ProductsResponse>,
                response: Response<ProductsResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
                            subs = response.body()?.subsections!!
                            subs.add(0,SubSectionModel(0,getString(R.string.all)))
                            subCatsAdapter.updateAll(subs)
//
                            productAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            Log.e("size",subs.size.toString())
            Log.e("pos",position.toString())
            subCatsAdapter.selected = position
            subs.get(position).selected = true
            subCatsAdapter.notifyDataSetChanged()
            if(subs.get(position).id==0){
                getData(null)
            }else{
                getData(subs.get(position).id!!)
            }

        }else{
            if (providerDetailsModel.maintenance==0) {
                val intent = Intent(this, ProductDetailsActivity::class.java)
                intent.putExtra("id", productModels.get(position).id)
                intent.putExtra("provider", providerDetailsModel)
                startActivity(intent)
            }else{

            }
        }
    }
}