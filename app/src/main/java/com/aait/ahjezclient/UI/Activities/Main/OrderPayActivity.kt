package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.Models.CartModel
import com.aait.ahjezclient.Models.TermsResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderPayActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_pay
    lateinit var back: ImageView
    lateinit var title: TextView

    lateinit var notification: ImageView
    lateinit var confirm: Button
    lateinit var visa: LinearLayout
    lateinit var vi: ImageView
    lateinit var master: LinearLayout
    lateinit var mas: ImageView
    lateinit var mada: LinearLayout
    lateinit var mad: ImageView
    lateinit var receipt: LinearLayout
    lateinit var recip: ImageView
    lateinit var wallet: LinearLayout
    lateinit var wall: ImageView
    lateinit var stc:LinearLayout
    lateinit var st:ImageView
    lateinit var paypal:LinearLayout
    lateinit var payp:ImageView
    lateinit var express:LinearLayout
    lateinit var expressp:ImageView
    var id = 0
    var user_id = 0
    var pay = "online"
    var provider = 0

    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    var providers = ArrayList<ProviderModelOffline>()
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        user_id = intent.getIntExtra("user",0)
        provider = intent.getIntExtra("provider",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        express = findViewById(R.id.express)
        expressp = findViewById(R.id.expressp)
        notification  = findViewById(R.id.notification)
        confirm = findViewById(R.id.confirm)
        visa = findViewById(R.id.visa)
        vi = findViewById(R.id.vi)
        master = findViewById(R.id.master)
        mas = findViewById(R.id.mas)
        mada = findViewById(R.id.mada)
        mad = findViewById(R.id.mad)
        receipt = findViewById(R.id.receipt)
        recip = findViewById(R.id.recip)
        wallet = findViewById(R.id.wallet)
        wall = findViewById(R.id.wall)
        stc = findViewById(R.id.stc)
        st = findViewById(R.id.st)
        paypal = findViewById(R.id.paypal)
        payp = findViewById(R.id.payp)
        back.setOnClickListener { Delete()}
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        title.text = getString(R.string.payment_method)
        paypal.visibility = View.GONE

        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        //allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)


        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){

                }else {
                    for (i in 0..cartModels.size-1){
                        // providers.clear()
                        // cart.clear()
                        if (cartModels.get(i).provider_id==provider) {
                            providers.add(cartModels.get(i))

                        }

                    }
                    Log.e("car", Gson().toJson(providers))


                }


            }
        })


        vi.setOnClickListener {pay = "online"
            vi.background = mContext.resources.getDrawable(R.mipmap.check)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","visa")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        mas.setOnClickListener {pay = "online"
            mas.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","visa")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        mad.setOnClickListener {pay = "online"
            mad.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","mada")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        st.setOnClickListener {
            pay = "online"
            st.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","stc_pay")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        payp.setOnClickListener {pay = ""
//            payp.background = mContext.resources.getDrawable(R.mipmap.check)
//            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
//            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
//            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
//            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            //expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        stc.setOnClickListener {
            pay = "online"
            st.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","stc_pay")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        paypal.setOnClickListener {pay = ""
//            payp.background = mContext.resources.getDrawable(R.mipmap.check)
//            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
//            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
//            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
//            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            //expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        recip.setOnClickListener {pay = "cash"
            recip.background = mContext.resources.getDrawable(R.mipmap.check)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        wall.setOnClickListener {
            pay = "balance"
            wall.background = mContext.resources.getDrawable(R.mipmap.check)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        visa.setOnClickListener { pay = "online"
            vi.background = mContext.resources.getDrawable(R.mipmap.check)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","visa")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        master.setOnClickListener { pay = "online"
            mas.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","visa")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        mada.setOnClickListener { pay = "online"
            mad.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","mada")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        expressp.setOnClickListener { pay = "online"
            expressp.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","amex")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        express.setOnClickListener { pay = "online"
            expressp.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOrderOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","amex")
            intent.putExtra("provider",provider)
            startActivity(intent)
        }
        receipt.setOnClickListener { pay = "cash"
            recip.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            wall.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        wallet.setOnClickListener { pay = "balance"
            wall.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        confirm.setOnClickListener { if (!pay.equals("")){
            if (pay.equals("online")){
                val intent = Intent(this,PayOrderOnlineActivity::class.java)
                intent.putExtra("id",id)
                intent.putExtra("user",user_id)
                intent.putExtra("type","visa")
                intent.putExtra("provider",provider)
                startActivity(intent)
            }else {
                Pay()
            }
        } }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        Delete()
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        Delete()
//    }

    fun Pay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.PayOrder("Bearer"+user.userData.token,lang.appLanguage,id,pay)?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        //CommonUtil.makeToast(mContext,response.body()?.data!!)
                            if (pay.equals("cash")){

                            }else {
                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            }
                        val intent = Intent(this@OrderPayActivity,OrderDoneActivity::class.java)
                        intent.putExtra("id",id)
                        startActivity(intent)
                        finish()
                        allCartViewModel!!.allCart.observe(this@OrderPayActivity, object : Observer<List<ProviderModelOffline>> {
                                override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                                    cartModelOfflines = cartModels

                                    if(cartModels.size==0){


                                    }else {

                                        for (i in 0..cartModels.size-1){
                                            // providers.clear()
                                            // cart.clear()
                                            if (cartModels.get(i).provider_id==provider) {
                                                allCartViewModel!!.deleteItem(cartModels.get(i))

                                            }


                                        }
                                    }


                                }
                            })
                    }else
                    {
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun Delete(){
       // showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DeleteOrder(
            "Bearer"+user.userData.token,lang.appLanguage
            ,id
        )?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!, t)
                t.printStackTrace()
               // hideProgressDialog()
            }

            override fun onResponse(
                call: Call<TermsResponse>,
                response: Response<TermsResponse>
            ) {
               // hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        val intent = Intent(this@OrderPayActivity,MainActivity::class.java)
                        intent.putExtra("state","normal")
                        startActivity(intent)
                        finish()
                    } else {
                       // CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                    }
                }
            }

        })
    }
}