package com.aait.ahjezclient.UI.Activities.Main

import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AddCartViewModel
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.Models.*
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.ReasonsAdapter
import com.aait.ahjezclient.UI.Controllers.SlidersAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_order_pay.view.*
import kotlinx.android.synthetic.main.activity_product_details.*
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class ProductDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_product_details
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var viewPager: CardSliderViewPager
    lateinit var circleIndicator: CircleIndicator
    lateinit var name: TextView
    lateinit var price: TextView
    lateinit var description: TextView
    lateinit var minus: ImageView
    lateinit var count: TextView
    lateinit var add: ImageView
    lateinit var favs: EditText
    lateinit var add_to_cart:Button
    lateinit var offer:TextView
    lateinit var providerDetailsModel: ProviderDetailsModel
    lateinit var face:ImageView
    lateinit var snap:ImageView
    lateinit var twitter:ImageView
    lateinit var whats:ImageView
    lateinit var massenger:ImageView
    var id = 0
    var max = 0
    override lateinit var addCartViewModel: AddCartViewModel
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartDataBase: CartDataBase
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    lateinit var cartModelOffline: ProviderModelOffline
    lateinit var productDetailsModel: ProductDetailsModel
    var link = ""
    var valu = 0.0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        providerDetailsModel = intent.getSerializableExtra("provider") as ProviderDetailsModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        viewPager = findViewById(R.id.viewPager)
        circleIndicator = findViewById(R.id.indicator)
        offer = findViewById(R.id.offer)
        name = findViewById(R.id.name)
        price = findViewById(R.id.price)
        description = findViewById(R.id.description)
        minus = findViewById(R.id.minus)
        count = findViewById(R.id.count)
        add = findViewById(R.id.add)
        favs = findViewById(R.id.favs)
        face = findViewById(R.id.face)
        twitter = findViewById(R.id.twitter)
        snap = findViewById(R.id.snap)
        whats = findViewById(R.id.whats)
        massenger = findViewById(R.id.massenger)
        add_to_cart = findViewById(R.id.add_to_cart)
        back.setOnClickListener { onBackPressed()
        }
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        title.text = getString(R.string.product_details)
        cartDataBase = CartDataBase.getDataBase(mContext)
        addCartViewModel = ViewModelProviders.of(this).get(AddCartViewModel::class.java)
        addCartViewModel = AddCartViewModel(application)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        //allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)



        getData()
        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if(cartModels.size==0){

                }else {
                    for (i in 0..cartModels.size-1){
                        if (cartModels[i].product_id==id){
                            count.text = (cartModelOfflines.get(i).count!!+1).toString()

                        }
                    }
                }
            }
        })
        add.setOnClickListener {
            if (max==0){

            }else {
                if (count.text.toString().toInt() == max) {
                } else {
                    count.text = (count.text.toString().toInt() + 1).toString()
                }
            }
        }
        minus.setOnClickListener {
            if (max<=0){

            }else {
                if (count.text.toString().toInt() == 1) {

                } else {
                    count.text = (count.text.toString().toInt() - 1).toString()
                }
            }
        }
        face.setOnClickListener { CommonUtil.ShareProductName(applicationContext,link,"com.facebook.katana") }
        snap.setOnClickListener { CommonUtil.ShareProductName(mContext,link,"com.snapchat.android") }
        twitter.setOnClickListener { CommonUtil.ShareProductName(mContext,link,"com.twitter.android") }
        whats.setOnClickListener { CommonUtil.ShareProductName(mContext,link,"com.whatsapp") }
        massenger.setOnClickListener { CommonUtil.ShareProductName(mContext,link,"com.facebook.orca") }
        add_to_cart.setOnClickListener {
            if (max<=0){
                CommonUtil.makeToast(mContext,getString(R.string.quantity_not_enough))
            }else {

                addCartViewModel.addItem(
                    ProviderModelOffline(
                        providerDetailsModel.id!!,
                        providerDetailsModel.name,
                        providerDetailsModel.avatar,
                        providerDetailsModel.address,
                        providerDetailsModel.distance,
                        productDetailsModel.id!!,
                        productDetailsModel.name,
                        productDetailsModel.image?.get(0),
                        favs.text.toString(),
                        (valu).toString(),
                        count.text.toString().toInt(),
                        productDetailsModel.quantity!!.toInt(),
                        providerDetailsModel.delivery_service
                       /* , null,null,0*/
                    )
                )
                val dialog = Dialog(mContext)
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog?.window!!.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                dialog?.setCancelable(true)
                dialog?.setContentView(R.layout.dialog_added_done)

                val done = dialog?.findViewById<Button>(R.id.done)
                val another = dialog?.findViewById<Button>(R.id.other)


                another.setOnClickListener {
                    dialog.dismiss()
                    onBackPressed()
                }
                done.setOnClickListener {
                    startActivity(Intent(this, BasketActivity::class.java))
                    dialog?.dismiss()
                    finish()
                }
                dialog?.show()
            }
         }
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProduct(lang.appLanguage,id)?.enqueue(object:
            Callback<ProductDetailsResponse> {
            override fun onFailure(call: Call<ProductDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ProductDetailsResponse>,
                response: Response<ProductDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        productDetailsModel = response.body()?.data!!
                        initSliderAds(response.body()?.data?.image!!)
                        name.text = response.body()?.data?.name

                        description.text = response.body()?.data?.desc

                        max = response.body()?.data?.quantity!!.toInt()
                        link = response.body()?.data?.share_link!!
                        if (response.body()?.data?.discount.equals("")){
                            price.text = response.body()?.data?.price+ " "+getString(R.string.rs)
                            offer.text = ""
                            valu = response.body()?.data?.price!!.toDouble()
                        }else{
                            price.text = response.body()?.data?.discount + " "+getString(R.string.rs)
                            offer.text = response.body()?.data?.price+ " "+getString(R.string.rs)
                            valu = response.body()?.data?.discount!!.toDouble()
                            CommonUtil.setStrokInText(offer)
                        }

                        if (max==0){
                            count.text = "0"
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun initSliderAds(list: ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
            circleIndicator.visibility = View.GONE
        }
        else{
            viewPager.visibility= View.VISIBLE
            circleIndicator.visibility = View.VISIBLE
            viewPager.adapter= SlidersAdapter(mContext!!,list)
            circleIndicator.setViewPager(viewPager)
        }
    }
}