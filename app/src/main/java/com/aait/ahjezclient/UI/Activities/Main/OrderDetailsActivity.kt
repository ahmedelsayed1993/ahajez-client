package com.aait.ahjezclient.UI.Activities.Main

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.*
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.ProdsAdapter
import com.aait.ahjezclient.UI.Controllers.ReasonsAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderDetailsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_orders_details
    lateinit var show:LinearLayout
    lateinit var down:ImageView
    lateinit var details:LinearLayout
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var report:ImageView
    lateinit var notification:ImageView
    lateinit var one:ImageView
    lateinit var line:TextView
    lateinit var two:ImageView
    lateinit var line_1:TextView
    lateinit var three:ImageView
    lateinit var line_2:TextView
    lateinit var four:ImageView
    lateinit var order_num:TextView
    lateinit var address_lay:LinearLayout
    lateinit var delivery_address:TextView
    lateinit var phone_lay:LinearLayout
    lateinit var phone:TextView
    lateinit var payment_method:TextView
    lateinit var order_date:TextView
    lateinit var notes:TextView
    lateinit var preferences:TextView
    lateinit var products:RecyclerView
    lateinit var products_value:TextView
    lateinit var delivery_lay:LinearLayout
    lateinit var delivery_price:TextView
    lateinit var tax:TextView
    lateinit var tax_value:TextView
    lateinit var total:TextView
    lateinit var image: CircleImageView
    lateinit var name:TextView
    lateinit var rating: RatingBar
    lateinit var address:TextView
    lateinit var onway:TextView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var prodsAdapter: ProdsAdapter
    var Products = ArrayList<ProdModel>()
    lateinit var received:Button
    lateinit var chat:ImageView
    lateinit var comment_lay:LinearLayout
    lateinit var rate:RatingBar
    lateinit var comment:TextView
    var id = 0
    var photo = ""
    var provider = ""
   var reciver = 0
    private var lat = ""
    private var lng =""
    lateinit var reasonsAdapter: ReasonsAdapter
    lateinit var linearLayoutManager1: LinearLayoutManager
    var reasonsModels = ArrayList<CancelModel>()
    var reason = 0
    lateinit var text:TextView
    lateinit var resons:TextView
    lateinit var lay:LinearLayout
    lateinit var lay1:LinearLayout
    var swipeRefresh: SwipeRefreshLayout? = null
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        comment_lay = findViewById(R.id.comment_lay)
        comment = findViewById(R.id.comment)
        rate = findViewById(R.id.rate)
        report = findViewById(R.id.report)
        notification = findViewById(R.id.notification)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        one = findViewById(R.id.one)
        line = findViewById(R.id.line)
        two = findViewById(R.id.two)
        text = findViewById(R.id.text)
        resons = findViewById(R.id.reason)
        lay = findViewById(R.id.lay)
        lay1 = findViewById(R.id.lay1)
        line_1 = findViewById(R.id.line_1)
        three = findViewById(R.id.three)
        line_2 = findViewById(R.id.line_2)
        four = findViewById(R.id.four)
        order_num = findViewById(R.id.order_num)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        address = findViewById(R.id.address)
        order_date = findViewById(R.id.order_date)
        payment_method = findViewById(R.id.payment_method)
        delivery_address = findViewById(R.id.delivery_address)
        onway = findViewById(R.id.onway)
        phone = findViewById(R.id.phone)
        notes = findViewById(R.id.notes)
        preferences = findViewById(R.id.preferences)
        delivery_lay = findViewById(R.id.delivery_lay)
        phone_lay = findViewById(R.id.phone_lay)
        products = findViewById(R.id.products)
        products_value = findViewById(R.id.products_value)
        delivery_price = findViewById(R.id.delivery_price)
        address_lay = findViewById(R.id.address_lay)
        chat = findViewById(R.id.chat)
        tax = findViewById(R.id.tax)
        tax_value = findViewById(R.id.tax_value)
        total = findViewById(R.id.total)
        show = findViewById(R.id.show)
        down = findViewById(R.id.down)
        details = findViewById(R.id.details)
        received = findViewById(R.id.received)
        details.visibility = View.GONE

        show.setOnClickListener {
            if (details.visibility == View.VISIBLE){
                down.setImageResource(R.mipmap.noun_down)
                details.visibility = View.GONE
            }else{
                down.setImageResource(R.mipmap.arrowtop)
                details.visibility = View.VISIBLE
            }
        }
        title.text = getString(R.string.order_details)

        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        back.setOnClickListener { onBackPressed()
            finish()}
        report.setOnClickListener {  val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.cancel_dialog)
            val reasons = dialog?.findViewById<RecyclerView>(R.id.reasons)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            val titl = dialog?.findViewById<TextView>(R.id.titl)
            titl.text = getString(R.string.reason_of_reporting)
            linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
            reasonsAdapter = ReasonsAdapter(mContext,reasonsModels,R.layout.recycle_reason)
            reasonsAdapter.setOnItemClickListener(this)
            reasons.layoutManager = linearLayoutManager1
            reasons.adapter = reasonsAdapter
            getReports()
            send.setOnClickListener {  showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Repot("Bearer"+user.userData.token,lang.appLanguage,reason,id,"order")
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog?.dismiss()
                        }

                        override fun onResponse(
                            call: Call<TermsResponse>,
                            response: Response<TermsResponse>
                        ) {
                            hideProgressDialog()
                            dialog?.dismiss()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    val dialog = Dialog(mContext)
                                    dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                    dialog ?.setCancelable(true)
                                    dialog ?.setContentView(R.layout.dailog_done)
                                    val text = dialog?.findViewById<TextView>(R.id.text)
                                    val back = dialog?.findViewById<Button>(R.id.ok)

                                    text.text = getString(R.string.report_done)


                                    back.setOnClickListener { dialog.dismiss()
                                        val intent = Intent(this@OrderDetailsActivity,MainActivity::class.java)
                                        intent.putExtra("state","normal")
                                        startActivity(intent)}
                                    dialog?.show()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })}
            cancel.setOnClickListener { dialog.dismiss() }
            dialog?.show()
        }
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        prodsAdapter = ProdsAdapter(mContext,Products,R.layout.recycle_prod)
        products.layoutManager = linearLayoutManager
        products.adapter = prodsAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
        chat.setOnClickListener { getID() }
        received.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
             Client.getClient()?.create(Service::class.java)?.getOrder("Bearer"+user.userData.token,lang.appLanguage,id,"user","finished")
            ?.enqueue(object : Callback<OrderDetailsResponse> {
                override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<OrderDetailsResponse>,
                    response: Response<OrderDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_rate)
            val pro_image = dialog?.findViewById<CircleImageView>(R.id.pro_image)
            val pro_name = dialog?.findViewById<TextView>(R.id.pro_name)
            val rate = dialog?.findViewById<RatingBar>(R.id.rate)
            val comment = dialog?.findViewById<EditText>(R.id.comment)
            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            Glide.with(mContext).load(photo).into(pro_image)
            pro_name.text = provider
            confirm.setOnClickListener {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.AddRate("Bearer"+user.userData.token,lang.appLanguage,id,"provider",rate.rating.toInt(),"order",comment.text.toString())
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog?.dismiss()
                        }
                        override fun onResponse(
                            call: Call<TermsResponse>,
                            response: Response<TermsResponse>
                        ) {
                            hideProgressDialog()
                            dialog?.dismiss()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    val intent = Intent(this@OrderDetailsActivity,MainActivity::class.java)
                                    intent.putExtra("state","normal")
                                    startActivity(intent)
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }
                    })
               }
            dialog?.show()
                             }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
        }
        address_lay.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?"+"&daddr="+lat+","+lng))
            )
        }
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOrder("Bearer"+user.userData.token,lang.appLanguage,id,"user",null)
            ?.enqueue(object : Callback<OrderDetailsResponse> {
                override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                }

                override fun onResponse(
                    call: Call<OrderDetailsResponse>,
                    response: Response<OrderDetailsResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            address.text = response.body()?.data?.address
                            phone.text = response.body()?.data?.phone
                            notes.text = response.body()?.data?.notes
                            preferences.text = response.body()?.data?.preferences
                            prodsAdapter.updateAll(response.body()?.data?.products!!)
                            products_value.text = response.body()?.data?.total+getString(R.string.rs)
                            tax.text = getString(R.string.The_tax_amount)+response.body()?.data?.tax+"%"
                            tax_value.text = response.body()?.data?.total_tax+getString(R.string.rs)
                            lat = response.body()?.data?.lat!!
                            lng = response.body()?.data?.lng!!
                            order_date.text = response.body()?.data?.created
                            order_num.text = response.body()?.data?.id.toString()
                            if (response.body()?.data?.payment!!.equals("cash")){
                                payment_method.text = getString(R.string.cash)
                            }else if (response.body()?.data?.payment!!.equals("online")){
                                payment_method.text = getString(R.string.online)
                            }else{
                                payment_method.text = getString(R.string.balance)
                            }
                             resons.text = response.body()?.data?.cancellation
                            address.text = response.body()?.data?.provider_address
                            rating.rating = response.body()?.data?.rate!!
                            Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                            name.text = response.body()?.data?.name
                            provider = response.body()?.data?.name!!
                            photo = response.body()?.data?.avatar!!
                            reciver  = response.body()?.data?.chat_provider!!
                            delivery_address.text = response.body()?.data?.address
                            if (response.body()?.data?.order_rate==0){
                                comment_lay.visibility = View.GONE
                            }else{
                                comment_lay.visibility = View.VISIBLE
                                comment.text = response.body()?.data?.order_comment
                                rate.rating = response.body()?.data?.order_rate!!.toFloat()
                            }
                            if (response.body()?.data?.address.equals("")){
                                address_lay.visibility = View.GONE
                            }else{
                                address_lay.visibility = View.VISIBLE
                            }
                            if (response.body()?.data?.phone.equals("")){
                                phone_lay.visibility = View.GONE
                            }else{
                                phone_lay.visibility = View.VISIBLE
                            }
                            if (response.body()?.data?.delivery_price.equals("")){
                                delivery_lay.visibility = View.GONE
                                line_2.visibility = View.GONE
                                three.visibility = View.GONE
                                onway.visibility = View.GONE
                            }else{
                                delivery_lay.visibility = View.VISIBLE
                                delivery_price.text = response.body()?.data?.delivery_price+getString(R.string.rs)
                                line_2.visibility = View.VISIBLE
                                three.visibility = View.VISIBLE
                                onway.visibility = View.VISIBLE
                            }
                            total.text = response.body()?.data?.final_total+getString(R.string.rs)
                            if (response.body()?.data?.status.equals("current")||response.body()?.data?.status.equals("pending")){
                                one.setImageResource(R.drawable.white_circle)
                                line.background = mContext.resources.getDrawable(R.color.colorGray)
                                received.visibility = View.GONE
                            }else if(response.body()?.data?.status.equals("accepted")){
                                one.setImageResource(R.mipmap.check)
                                line.background = mContext.resources.getDrawable(R.color.colorYellow)
                                received.visibility = View.GONE
                            }else if (response.body()?.data?.status.equals("prepared")){
                                one.setImageResource(R.mipmap.check)
                                line.background = mContext.resources.getDrawable(R.color.colorYellow)
                                two.setImageResource(R.mipmap.check)
                                received.visibility = View.GONE
                            }else if (response.body()?.data?.status.equals("on_way")) {
                                one.setImageResource(R.mipmap.check)
                                line.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                two.setImageResource(R.mipmap.check)
                                line_1.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                three.setImageResource(R.mipmap.check)
                                line_2.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                received.visibility = View.GONE
                            }else if (response.body()?.data?.status.equals("completed")) {
                                one.setImageResource(R.mipmap.check)
                                line.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                two.setImageResource(R.mipmap.check)
                                line_1.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                three.setImageResource(R.mipmap.check)
                                line_2.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                four.setImageResource(R.mipmap.check)
                                received.visibility = View.VISIBLE
                            }else if (response.body()?.data?.status.equals("refuse")||response.body()?.data?.status.equals("cancel")){
                                received.visibility = View.GONE

                                lay.visibility = View.GONE
                                lay1.visibility = View.GONE
                                text.visibility = View.VISIBLE
                                resons.visibility = View.VISIBLE
                            }else{
                                one.setImageResource(R.mipmap.check)
                                line.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                two.setImageResource(R.mipmap.check)
                                line_1.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                three.setImageResource(R.mipmap.check)
                                line_2.background =
                                    mContext.resources.getDrawable(R.color.colorYellow)
                                four.setImageResource(R.mipmap.check)
                                received.visibility = View.GONE
                            }
                            //current
                            //accepted

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Conversation("Bearer"+user.userData.token,lang.appLanguage,reciver)?.enqueue(object :
            Callback<ConversationIdResponse>{
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }
            override fun onResponse(
                call: Call<ConversationIdResponse>,
                response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@OrderDetailsActivity,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.reason){
            reasonsAdapter.selected = position
            reasonsModels.get(position).selected = true
            reasonsAdapter.notifyDataSetChanged()
            reason = reasonsModels.get(position).id!!
            Log.e("reason",reason.toString())

        }
    }
    fun getReports(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Reports("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object :Callback<CancelResponse>{
            override fun onFailure(call: Call<CancelResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<CancelResponse>,
                response: Response<CancelResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        reasonsAdapter.updateAll(response.body()?.data!!)
                        if (response.body()?.data!!.size!=0){
                            reason = response.body()?.data?.get(0)?.id!!
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}