package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.widget.Button
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.R

class OrderDoneActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_done
    lateinit var back: Button
    lateinit var follow: Button
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        follow = findViewById(R.id.follow)
        back.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("state","normal")
            startActivity(intent)
            finish()}
        follow.setOnClickListener { val intent = Intent(this,OrderDetailsActivity::class.java)
        intent.putExtra("id",id)
        startActivity(intent)
        finish()}

    }
}