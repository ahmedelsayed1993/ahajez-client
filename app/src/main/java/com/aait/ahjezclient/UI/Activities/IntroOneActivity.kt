package com.aait.ahjezclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.SliderModel
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.MainActivity
import java.util.*

class IntroOneActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_intro_one
    lateinit var image:ImageView
    lateinit var title:TextView
   // lateinit var content:TextView
    lateinit var next:Button
    lateinit var skip:TextView
    var sliders = ArrayList<SliderModel>()
    override fun initializeComponents() {
        image = findViewById(R.id.image)
        title =findViewById(R.id.title)
       // content = findViewById(R.id.content)
        next = findViewById(R.id.next)
        skip = findViewById(R.id.skip)
        sliders.add(SliderModel(R.mipmap.one,getString(R.string.Ease_of_use_as_if_you_are_preparing)))
        sliders.add(SliderModel(R.mipmap.seven,getString(R.string.Organize_your_life_smartly)))
        sliders.add(SliderModel(R.mipmap.five,getString(R.string.Book_your_service_with_hundreds_of_agencies)))
        sliders.add(SliderModel(R.mipmap.three,getString(R.string.Search_and_ask_while_you_are_comfortable)))
        sliders.add(SliderModel(R.mipmap.four,getString(R.string.Continue_to_deliver_your_order_moment_by_moment)))
        sliders.add(SliderModel(R.mipmap.six,getString(R.string.Order_and_we_will_prepare_your_order_before_you_arrive)))
        image.setImageResource(R.mipmap.one)
        title.text = getString(R.string.Ease_of_use_as_if_you_are_preparing)
        next.setOnClickListener { startActivity(Intent(this,IntroTwoActivity::class.java)) }
        skip.setOnClickListener { val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("state","normal")
            startActivity(intent)
            finish()  }
    }
}