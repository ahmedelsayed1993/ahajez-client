package com.aait.ahjezclient.UI.Activities.Main

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Fragments.HomeFragment
import com.aait.ahjezclient.UI.Fragments.MoreFragment
import com.aait.ahjezclient.UI.Fragments.PurchaseFargment
import com.aait.ahjezclient.UI.Fragments.ReservationFragment
import com.aait.ahjezclient.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView

class MainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var line:TextView
    lateinit var my_reservations: LinearLayout
    lateinit var line1:TextView
    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var purchase: LinearLayout
    lateinit var line2:TextView
    lateinit var purchase_image: ImageView
    lateinit var purchase_text: TextView
    lateinit var more: LinearLayout
    lateinit var line3:TextView
    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var state = ""
    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var purchaseFargment: PurchaseFargment
    internal lateinit var reservationFragment: ReservationFragment
    internal lateinit var moreFragment: MoreFragment
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var basket:ImageView
    lateinit var count:TextView
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    var coun = 0
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initializeComponents() {
        state = intent.getStringExtra("state")!!
        if (state.equals("login")){
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(false)
            dialog ?.setContentView(R.layout.dialog_wlcome)
            val image = dialog?.findViewById<CircleImageView>(R.id.image)
            val back = dialog?.findViewById<Button>(R.id.confirm)
            Glide.with(mContext).load(user.userData.avatar).into(image)


            back.setOnClickListener { dialog.dismiss()}
            dialog?.show()
        }else{

        }
        line = findViewById(R.id.line)
        line1 = findViewById(R.id.line1)
        line2 = findViewById(R.id.line2)
        line3 = findViewById(R.id.line3)
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        purchase = findViewById(R.id.purchase)
        purchase_image = findViewById(R.id.purchase_image)
        purchase_text = findViewById(R.id.purchase_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        basket = findViewById(R.id.basket)
        count = findViewById(R.id.count)
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.VISIBLE
        }
        notification.setOnClickListener { if (user.loginStatus!!){
            startActivity(Intent(this,NotificationActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }
        }

        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)

        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels


//                    for (i in 0..cartModels.size-1){
//                        coun = coun+cartModels.get(i).count!!.toInt()
//                    }
                    if (cartModels.size==0){
                        count.visibility = View.GONE
                    }else {
                        count.visibility = View.VISIBLE
                        count.text = cartModels.size.toString()
                    }
                    Log.e("cart", Gson().toJson(cartModels))

            }
        })
        homeFragment = HomeFragment.newInstance()
        reservationFragment = ReservationFragment.newInstance()
        purchaseFargment = PurchaseFargment.newInstance()
        moreFragment = MoreFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, homeFragment)
        transaction!!.add(R.id.home_fragment_container, reservationFragment)
        transaction!!.add(R.id.home_fragment_container,purchaseFargment)
        transaction!!.add(R.id.home_fragment_container,moreFragment)
        transaction!!.commit()
        basket.setOnClickListener { startActivity(Intent(this,BasketActivity::class.java)) }

        showhome()
        home.setOnClickListener { showhome() }
        my_reservations.setOnClickListener {
            if (user.loginStatus!!) {
                showreservation()
            }else{

            }
        }
        purchase.setOnClickListener {
            if (user.loginStatus!!) {
                showpurchase()
            }else{

            }
            }
        more.setOnClickListener { showmore() }

    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showhome(){
        selected = 1
        home_image.setImageResource(R.mipmap.homeactive)
        home_text.textColor = Color.parseColor("#2B726F")
        reservation_text.textColor = Color.parseColor("#CFCFCF")
        purchase_text.textColor = Color.parseColor("#CFCFCF")
        more_text.textColor = Color.parseColor("#CFCFCF")
        line.background = mContext.getDrawable(R.color.colorGreen)
        line1.background = mContext.getDrawable(R.color.colorWhite)
        line2.background = mContext.getDrawable(R.color.colorWhite)
        line3.background = mContext.getDrawable(R.color.colorWhite)
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) {
            reservation_image.setImageResource(R.mipmap.booking)
            purchase_image.setImageResource(R.mipmap.bag)
            more_image.setImageResource(R.mipmap.more)
        }else{
            reservation_image.setImageResource(R.mipmap.bookingdak)
            purchase_image.setImageResource(R.mipmap.bagdark)
            more_image.setImageResource(R.mipmap.moredark)
        }
        title.text = getString(R.string.home)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showreservation(){
        selected = 1
        reservation_image.setImageResource(R.mipmap.bookingactive)

        reservation_text.textColor = Color.parseColor("#2B726F")
        home_text.textColor = Color.parseColor("#CFCFCF")
        purchase_text.textColor = Color.parseColor("#CFCFCF")
        more_text.textColor = Color.parseColor("#CFCFCF")
        line1.background = mContext.getDrawable(R.color.colorGreen)
        line.background = mContext.getDrawable(R.color.colorWhite)
        line2.background = mContext.getDrawable(R.color.colorWhite)
        line3.background = mContext.getDrawable(R.color.colorWhite)
        if (AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_NO) {
            home_image.setImageResource(R.mipmap.homee)
            purchase_image.setImageResource(R.mipmap.bag)
            more_image.setImageResource(R.mipmap.more)
        }else{
            home_image.setImageResource(R.mipmap.homeedark)
            purchase_image.setImageResource(R.mipmap.bagdark)
            more_image.setImageResource(R.mipmap.moredark)
        }
        title.text = getString(R.string.my_reservations)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, reservationFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showpurchase(){
        selected = 1
        purchase_image.setImageResource(R.mipmap.bagactive)

        purchase_text.textColor = Color.parseColor("#2B726F")
        reservation_text.textColor = Color.parseColor("#CFCFCF")
        home_text.textColor = Color.parseColor("#CFCFCF")
        more_text.textColor = Color.parseColor("#CFCFCF")
        line2.background = mContext.getDrawable(R.color.colorGreen)
        line1.background = mContext.getDrawable(R.color.colorWhite)
        line.background = mContext.getDrawable(R.color.colorWhite)
        line3.background = mContext.getDrawable(R.color.colorWhite)
        if (AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_NO) {
            reservation_image.setImageResource(R.mipmap.booking)
            home_image.setImageResource(R.mipmap.homee)
            more_image.setImageResource(R.mipmap.more)
        }else{
            reservation_image.setImageResource(R.mipmap.bookingdak)
            home_image.setImageResource(R.mipmap.homeedark)
            more_image.setImageResource(R.mipmap.moredark)
        }
        title.text = getString(R.string.My_purchases)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, purchaseFargment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showmore(){
        selected = 1
        more_image.setImageResource(R.mipmap.moreactive)

        more_text.textColor = Color.parseColor("#2B726F")
        reservation_text.textColor = Color.parseColor("#CFCFCF")
        purchase_text.textColor = Color.parseColor("#CFCFCF")
        home_text.textColor = Color.parseColor("#CFCFCF")
        line3.background = mContext.getDrawable(R.color.colorGreen)
        line1.background = mContext.getDrawable(R.color.colorWhite)
        line2.background = mContext.getDrawable(R.color.colorWhite)
        line.background = mContext.getDrawable(R.color.colorWhite)
        if (AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_NO){
        reservation_image.setImageResource(R.mipmap.booking)
        purchase_image.setImageResource(R.mipmap.bag)
        home_image.setImageResource(R.mipmap.homee)
        }else{
            reservation_image.setImageResource(R.mipmap.bookingdak)
            purchase_image.setImageResource(R.mipmap.bagdark)
            home_image.setImageResource(R.mipmap.homeedark)
        }
        title.text = getString(R.string.more)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, moreFragment)
        transaction!!.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}