package com.aait.ahjezclient.UI.Activities.AppInfo

import android.content.Intent
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.ChatsModel
import com.aait.ahjezclient.Models.ChatsResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.ChatActivity
import com.aait.ahjezclient.UI.Activities.Main.NotificationActivity
import com.aait.ahjezclient.UI.Controllers.ConversationsAdapter
import com.aait.ahjezclient.Utils.CommonUtil

import com.bumptech.glide.Glide

import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatsActivity :ParentActivity(), OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this@ChatsActivity, ChatActivity::class.java)
        intent.putExtra("id",chatModels.get(position).conversation_id)
        intent.putExtra("receiver",chatModels.get(position).user_id)
        intent.putExtra("lastpage",chatModels.get(position).lastPage)
        startActivity(intent)

    }

    override val layoutResource: Int
        get() = R.layout.activity_chats
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var chatModels = java.util.ArrayList<ChatsModel>()
    internal lateinit var conversationsAdapter: ConversationsAdapter
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView


    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        notification = findViewById(R.id.notification)
        title.text = getString(R.string.chats)
        back.setOnClickListener { onBackPressed()
        finish()}
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        conversationsAdapter =  ConversationsAdapter(mContext,chatModels,R.layout.recycler_conversations)
        conversationsAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = conversationsAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()


    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Conversations(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object :
            Callback<ChatsResponse> {
            override fun onResponse(call: Call<ChatsResponse>, response: Response<ChatsResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText("لا يوجد محادثات")
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                conversationsAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<ChatsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }
//    fun sideMenu(){
//        drawer_layout.useCustomBehavior(Gravity.START)
//        drawer_layout.useCustomBehavior(Gravity.END)
//        drawer_layout.setRadius(Gravity.START, 100f)
//        drawer_layout.setRadius(Gravity.END, 100f)
//        drawer_layout.setViewScale(Gravity.START, 0.95f) //set height scale for main view (0f to 1f)
//        drawer_layout.setViewScale(Gravity.END, 0.95f) //set height scale for main view (0f to 1f)
//        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
////        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
////        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
//        drawer_layout.setDrawerElevation(Gravity.START, 100f) //set drawer elevation (dimension)
//        drawer_layout.setDrawerElevation(Gravity.END, 100f) //set drawer elevation (dimension)
//        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
//        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)
//
//        drawer_layout.setRadius(Gravity.END, 0f)
//        about_app = drawer_layout.findViewById(R.id.about_app)
//        about_app.setOnClickListener { startActivity(Intent(this,AboutAppActivity::class.java)) }
//        terms = drawer_layout.findViewById(R.id.terms)
//        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
//        call_us = drawer_layout.findViewById(R.id.call_us)
//        call_us.setOnClickListener { startActivity(Intent(this,CallUsActivity::class.java))
//        }
//        settings = drawer_layout.findViewById(R.id.settings)
//        settings.setOnClickListener { startActivity(Intent(this,SettingsActivity::class.java)) }
//        chats = drawer_layout.findViewById(R.id.chats)
//        chats.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
//        profile = drawer_layout.findViewById(R.id.profile)
//        profile.setOnClickListener { startActivity(Intent(this,ProfileActivity::class.java)) }
////        notification = drawer_layout.findViewById(R.id.notifications)
////        notification.setOnClickListener {startActivity(Intent(this,NotificationActivity::class.java))
////        }
//        image = drawer_layout.findViewById(R.id.image)
//        name = drawer_layout.findViewById(R.id.name)
//        address = drawer_layout.findViewById(R.id.address)
//        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
//        name.text = mSharedPrefManager.userData.name!!
//        address.text = mSharedPrefManager.userData.email!!
//        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
//        orders = drawer_layout.findViewById(R.id.orders)
//        orders.setOnClickListener { startActivity(Intent(this,OrdersActivity::class.java)) }
//        home = drawer_layout.findViewById(R.id.home)
//        home.setOnClickListener { startActivity(Intent(this,MainActivity::class.java)) }
//        logout = drawer_layout.findViewById(R.id.logout)
//        logout.setOnClickListener { logout() }
//    }
//
//    fun logout(){
//        showProgressDialog(getString(R.string.please_wait))
//        Client.getClient()?.create(Service::class.java)?.logOut(mSharedPrefManager.userData.id!!,deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
//            Callback<AboutAppResponse> {
//            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
//                CommonUtil.handleException(mContext,t)
//                t.printStackTrace()
//                hideProgressDialog()
//            }
//
//            override fun onResponse(
//                call: Call<AboutAppResponse>,
//                response: Response<AboutAppResponse>
//            ) {
//                hideProgressDialog()
//                if (response.isSuccessful){
//                    if (response.body()?.value.equals("1")){
//                        mSharedPrefManager.loginStatus=false
//                        mSharedPrefManager.Logout()
//                        CommonUtil.makeToast(applicationContext,response.body()?.data!!)
//                        startActivity(Intent(this@ChatsActivity, SplashActivity::class.java))
//                        finish()
//                    }else{
//                        CommonUtil.makeToast(applicationContext,response.body()?.msg!!)
//
//                    }
//                }
//            }
//        })
//    }
}