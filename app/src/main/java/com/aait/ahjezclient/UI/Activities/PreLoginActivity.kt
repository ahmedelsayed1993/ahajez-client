package com.aait.ahjezclient.UI.Activities

import android.content.Intent
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Auth.LoginActivity
import com.aait.ahjezclient.UI.Activities.Auth.RegisterActivity
import com.aait.ahjezclient.UI.Activities.Main.MainActivity

class PreLoginActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_pre_login
    lateinit var login:LinearLayout
    lateinit var register:LinearLayout
    lateinit var skip:TextView

    override fun initializeComponents() {
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        skip = findViewById(R.id.skip)
        login.setOnClickListener { startActivity(Intent(this, LoginActivity::class.java)) }
        register.setOnClickListener { startActivity(Intent(this, RegisterActivity::class.java)) }
        skip.setOnClickListener { user.loginStatus = false
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("state","normal")
            startActivity(intent)}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}