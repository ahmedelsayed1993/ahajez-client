package com.aait.ahjezclient.UI.Activities.Main

import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.R
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView

class Images:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.image
    lateinit var image: PhotoView
    var link = ""
    override fun initializeComponents() {
        image = findViewById(R.id.image)
        link = intent.getStringExtra("link")!!
        Glide.with(mContext).load(link).into(image)

    }
}