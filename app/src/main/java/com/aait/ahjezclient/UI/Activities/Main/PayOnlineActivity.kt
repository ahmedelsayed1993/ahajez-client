package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.os.Handler
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient




import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.TermsResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PayOnlineActivity : ParentActivity() {

    internal var web: WebView? = null
    internal var id: Int = 0
    internal var user_id = 0

    internal lateinit var pay: String

    protected override val layoutResource: Int
        get() = R.layout.activity_online



    protected override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        user_id = intent.getIntExtra("user",0)
        pay = getIntent().getStringExtra("type")!!
        web = findViewById(R.id.web)
        web!!.settings.javaScriptEnabled = true
        Log.e(
            "link",
            "https://aihjazwanjuz.aait-sa.com/online-payment?user=" + user_id+"&reservation_id="+id+"&methods="+pay
        )
        web!!.loadUrl("https://aihjazwanjuz.aait-sa.com/online-payment?user=" + user_id+"&reservation_id="+id+"&methods="+pay)
        Log.e("url", web!!.url!!)
        web!!.webViewClient = MyWebVew()

        if (web!!.url!!.contains("https://aihjazwanjuz.aait-sa.com/payment-success")) {
            Pay()

        }else if (web!!.url!!.contains("https://aihjazwanjuz.aait-sa.com/payment-fail")){

            onBackPressed()
            finish()
           // CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
        }
    }

    protected fun hideInputType(): Boolean {
        return false
    }

    inner class MyWebVew : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: String): Boolean {
            Log.e("link",request)
            if (request.contains("https://aihjazwanjuz.aait-sa.com/payment-fail")) {
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        finish()
                      //  CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            } else if (request.contains("https://aihjazwanjuz.aait-sa.com/payment-success")) {


                Pay()

            }else{
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        finish()
                        //CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            }
            return true
        }
    }

    fun Pay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Pay("Bearer"+user.userData.token,lang.appLanguage,id,"online")?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        val intent = Intent(this@PayOnlineActivity,DoneActivity::class.java)
                        intent.putExtra("id",id)
                        startActivity(intent)
                        finish()
                    }else
                    {
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}
