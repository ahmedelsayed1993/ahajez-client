package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.ImageView
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.ImagesActivity
import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderAdapter
import kotlinx.android.synthetic.main.card_image_slider.view.*

class SlidersAdapter (context: Context, list : ArrayList<String>) : CardSliderAdapter<String>(list) {


    var list = list
    var context=context

    lateinit var image: ImageView
    override fun bindView(position: Int, itemContentView: View, item: String?) {
        image = itemContentView.image
        Glide.with(context).load(item).into(image)
        itemContentView.setOnClickListener {
                            val intent  = Intent(context, ImagesActivity::class.java)
                intent.putExtra("link",list)
                context.startActivity(intent)
        }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }

}