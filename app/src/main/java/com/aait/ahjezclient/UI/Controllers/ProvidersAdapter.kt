package com.aait.ahjezclient.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentRecyclerAdapter
import com.aait.ahjezclient.Base.ParentRecyclerViewHolder
import com.aait.ahjezclient.Models.CatsModel
import com.aait.ahjezclient.Models.ProviderModel
import com.aait.ahjezclient.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class ProvidersAdapter (context: Context, data: MutableList<ProviderModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProviderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)
        viewHolder.address.text = questionModel.address
        viewHolder.distance.text = questionModel.distance.toString()+mcontext.getString(R.string.km)
        viewHolder.rating.rating = questionModel.rate!!
        if(questionModel.closed==0&&questionModel.maintenance==0){
            viewHolder.closed.visibility = View.GONE
            viewHolder.maintenance.visibility = View.GONE
        }else if(questionModel.closed==1&&questionModel.maintenance==0){
            viewHolder.closed.visibility = View.VISIBLE
            viewHolder.maintenance.visibility = View.GONE
        }else if (questionModel.closed==0&&questionModel.maintenance==1){
            viewHolder.closed.visibility = View.GONE
            viewHolder.maintenance.visibility = View.VISIBLE
        }else{
            viewHolder.closed.visibility = View.VISIBLE
            viewHolder.maintenance.visibility = View.GONE
        }
        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_fall_down)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<CircleImageView>(R.id.image)
        internal var address = itemView.findViewById<TextView>(R.id.address)
        internal var distance = itemView.findViewById<TextView>(R.id.distance)
        internal var closed = itemView.findViewById<TextView>(R.id.closed)
        internal var maintenance = itemView.findViewById<TextView>(R.id.maintenance)
        internal var rating = itemView.findViewById<RatingBar>(R.id.rating)



    }
}