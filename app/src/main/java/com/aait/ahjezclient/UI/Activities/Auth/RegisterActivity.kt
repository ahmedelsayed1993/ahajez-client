package com.aait.ahjezclient.UI.Activities.Auth

import android.content.Intent
import android.provider.Settings
import android.text.InputType
import android.util.Log
import android.widget.*
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.UserResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.AppInfo.TermsActivity
import com.aait.ahjezclient.UI.Activities.Main.MainActivity
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var register:Button
    lateinit var user_name:EditText
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var confirm_pass:EditText
    lateinit var view_confirm:ImageView
    lateinit var check:CheckBox
    lateinit var terms:TextView
    lateinit var skip:TextView
    lateinit var login:LinearLayout

    var deviceID = ""
    var ID = ""
    override fun initializeComponents() {
        ID =  Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })

        register = findViewById(R.id.register)
        user_name = findViewById(R.id.user_name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        confirm_pass = findViewById(R.id.confirm_pass)
        view_confirm = findViewById(R.id.view_confirm)
        check = findViewById(R.id.check)
        terms = findViewById(R.id.terms)
        skip = findViewById(R.id.skip)
        login = findViewById(R.id.login)
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java))
        finish()}
        skip.setOnClickListener { user.loginStatus = false
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("state","normal")
            startActivity(intent)}

        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        register.setOnClickListener {
            if(CommonUtil.checkEditError(user_name,getString(R.string.user_name))||
            CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
            CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
            CommonUtil.checkEditError(password,getString(R.string.password))||
            CommonUtil.checkLength(password,getString(R.string.password_length),6)||
            CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password))){
            return@setOnClickListener
          }else{
                if (!email.text.toString().equals("")){
                    if (!CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
                        return@setOnClickListener
                    }else {
                        if (!password.text.toString().equals(confirm_pass.text.toString())) {
                            CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                        } else {
                            if (!check.isChecked) {
                                CommonUtil.makeToast(mContext, getString(R.string.agree_terms))
                            } else {

                                Register(email.text.toString())
                            }

                        }
                    }
                }else{
                    if (!password.text.toString().equals(confirm_pass.text.toString())) {
                        CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                    } else {
                        if (!check.isChecked) {
                            CommonUtil.makeToast(mContext, getString(R.string.agree_terms))
                        } else {

                            Register(null)
                        }

                    }
                }
          }
        }

        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        view_confirm.setOnClickListener { if (confirm_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            confirm_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }

    }

    fun Register(mail:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(user_name.text.toString(),phone.text.toString(),mail
            ,password.text.toString(),deviceID,"android",lang.appLanguage,ID)?.enqueue(object:
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivationCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}