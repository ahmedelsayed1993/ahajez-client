package com.aait.ahjezclient.UI.Activities.Main

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Models.BannersModel
import com.aait.ahjezclient.Models.ServiceDetailsResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.SliderAdapter
import com.aait.ahjezclient.UI.Controllers.SlidersAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class ServiceDetailsActivity :ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_service_details

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var viewPager:CardSliderViewPager
    lateinit var circleIndicator: CircleIndicator
    lateinit var name:TextView
    lateinit var price:TextView
    lateinit var description:TextView
    lateinit var total:CheckBox
    lateinit var discount:TextView
    lateinit var part:CheckBox
    lateinit var amount:TextView
    lateinit var offer:TextView
    lateinit var reserve:Button
    var id = 0
    var provider = 0
    var type = "total"
    var value = ""
    var dis = ""
    var ref = 0
    var free = 0
    @SuppressLint("ResourceAsColor")
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        provider = intent.getIntExtra("provider",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        viewPager = findViewById(R.id.viewPager)
        circleIndicator = findViewById(R.id.indicator)
        name = findViewById(R.id.name)
        price = findViewById(R.id.price)
        offer = findViewById(R.id.offer)
        description = findViewById(R.id.description)
        total = findViewById(R.id.total)
        discount = findViewById(R.id.discount)
        part = findViewById(R.id.part)
        amount = findViewById(R.id.amount)
        reserve = findViewById(R.id.reserve)
        back.setOnClickListener { onBackPressed()
        }
        title.text = getString(R.string.service_details)
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        total.isChecked = true
        discount.visibility = View.VISIBLE
        total.setTextColor(Color.parseColor("#2B726F"))
        part.isChecked = false
        amount.visibility = View.GONE
       // part.setTextColor(Color.parseColor("#2B726F"))
        getData()


        total.setOnClickListener {
            type = "total"
            total.isChecked = true
            discount.visibility = View.VISIBLE
            total.setTextColor(Color.parseColor("#2B726F"))
            part.isChecked = false
            amount.visibility = View.GONE
           // part.setTextColor(Color.parseColor("#0B2A3C"))
        }
        part.setOnClickListener {
            type = "part"
            total.isChecked = false
            discount.visibility = View.GONE
           // total.setTextColor(Color.parseColor("#0B2A3C"))
            part.isChecked = true
            amount.visibility = View.VISIBLE
            part.setTextColor(Color.parseColor("#2B726F"))
        }

        reserve.setOnClickListener {
            if (user.loginStatus!!) {
                if (free==0) {
                    val intent = Intent(this, DayTimeActivity::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("provider", provider)
                    intent.putExtra("type", type)
                    intent.putExtra("value", value)
                    intent.putExtra("dis", dis)
                    intent.putExtra("ref", ref)
                    intent.putExtra("free", free)
                    startActivity(intent)
                }else{
                    val intent = Intent(this, DayTimeActivity::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("provider", provider)
                    intent.putExtra("type", "total")
                    intent.putExtra("value", "0")
                    intent.putExtra("dis", "0")
                    intent.putExtra("ref", ref)
                    intent.putExtra("free", free)
                    startActivity(intent)
                }
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }





    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ServiceDetails(lang.appLanguage,id)?.enqueue(object:
            Callback<ServiceDetailsResponse>{
            override fun onFailure(call: Call<ServiceDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ServiceDetailsResponse>,
                response: Response<ServiceDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        initSliderAds(response.body()?.data?.images!!)
                        name.text = response.body()?.data?.name
                        //price.text = response.body()?.data?.price + " "+getString(R.string.rs)
                        description.text = response.body()?.data?.desc
                        value = response.body()?.data?.after_discount.toString()
                        dis = response.body()?.data?.part_amount.toString()
                        discount.text = "("+ getString(R.string.discount)+response.body()?.data?.discount + "%" + "   "+getString(R.string.price_after_discount)+response.body()?.data?.after_discount+getString(R.string.rs)+")"
                        amount.text = getString(R.string.amount_paid) + response.body()?.data?.part_amount+getString(R.string.rs)
                        ref = response.body()?.data?.refundable!!
                        free = response.body()?.data?.free_reservation!!
                        if (response.body()?.data?.discount.equals("")){
                            total.visibility = View.GONE
                            discount.visibility = View.GONE
                            type = "part"
                        }else{
                            total.visibility = View.VISIBLE
                            discount.visibility = View.VISIBLE
                        }
                        if (response.body()?.data?.part_amount.equals("")){
                            part.visibility = View.GONE
                            amount.visibility = View.GONE
                            type = "total"
                        }else{
                            part.visibility = View.VISIBLE
                            amount.visibility = View.VISIBLE
                        }
                        if (response.body()?.data?.total_discount.equals("")){
                            price.text = response.body()?.data?.price+ " "+getString(R.string.rs)
                            offer.text = ""

                        }else{
                            price.text = response.body()?.data?.total_discount + " "+getString(R.string.rs)
                            offer.text = response.body()?.data?.price+ " "+getString(R.string.rs)

                            CommonUtil.setStrokInText(offer)
                        }
                        if (response.body()?.data?.free_reservation==0){
                            reserve.text = getString(R.string.Book_the_service_now)
                            amount.visibility = View.GONE
//                            total.visibility = View.VISIBLE
//                            part.visibility = View.VISIBLE
//                            discount.visibility = View.VISIBLE
                        }else{
                            reserve.text = getString(R.string.Book_the_service_free)
                            amount.visibility = View.GONE
                            total.visibility = View.GONE
                            part.visibility = View.GONE
                            discount.visibility = View.GONE
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun initSliderAds(list: ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
            circleIndicator.visibility = View.GONE
        }
        else{
            viewPager.visibility= View.VISIBLE
            circleIndicator.visibility = View.VISIBLE
            viewPager.adapter= SlidersAdapter(mContext!!,list)
            circleIndicator.setViewPager(viewPager)
        }
    }
}