package com.aait.ahjezclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.Main.MainActivity

class IntroThreeActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_intro_three
    lateinit var image: ImageView
    lateinit var title: TextView
    lateinit var next: Button


    override fun initializeComponents() {
        image = findViewById(R.id.image)
        title =findViewById(R.id.title)
        next = findViewById(R.id.next)
        image.setImageResource(R.mipmap.six)
        title.text = getString(R.string.Order_and_we_will_prepare_your_order_before_you_arrive)
        next.setOnClickListener {
            val intent = Intent(this@IntroThreeActivity, PreLoginActivity::class.java)
            startActivity(intent)
            finish()
        }


    }
}