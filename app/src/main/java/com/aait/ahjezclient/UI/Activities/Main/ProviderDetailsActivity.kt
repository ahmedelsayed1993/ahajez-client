package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.media.Image
import android.util.Log
import android.view.View
import android.widget.*
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.GPS.GpsTrakerListener
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.*
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Views.ListDialog
import com.aait.ahjezclient.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_order_pay.view.*
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProviderDetailsActivity:ParentActivity(),OnItemClickListener,GpsTrakerListener {
    override fun onItemClick(view: View, position: Int) {

    }

    override val layoutResource: Int
        get() = R.layout.activity_provider_details
     var id = 0
     lateinit var back:ImageView
     lateinit var title:TextView

    lateinit var notification:ImageView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var rating:RatingBar
    lateinit var rate:TextView
    lateinit var address:TextView
    lateinit var heart:ImageView
    lateinit var description:TextView
    lateinit var status:TextView
    lateinit var times:TextView
    lateinit var show:TextView
    lateinit var services:LinearLayout
    lateinit var products:LinearLayout
    lateinit var chat:LinearLayout
    var lat = ""
    var lng = ""
    lateinit var listDialog: ListDialog
     var workTimesModels = ArrayList<WorkTimesModel>()
    lateinit var cat: CatsModel
    var reciver = 0
   var providerDetailsModel: ProviderDetailsModel?=null
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        cat = intent.getSerializableExtra("cat") as CatsModel
        lat = intent.getStringExtra("lat")!!
        lng = intent.getStringExtra("lng")!!
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)

        title.text = cat.name
        notification = findViewById(R.id.notification)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        rate = findViewById(R.id.rate)
        address = findViewById(R.id.address)
        heart = findViewById(R.id.heart)
        description = findViewById(R.id.description)
        status = findViewById(R.id.status)
        times = findViewById(R.id.times)
        show = findViewById(R.id.show)
        services = findViewById(R.id.services)
        products = findViewById(R.id.products)
        chat = findViewById(R.id.chat)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = cat.name
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        if (user.loginStatus!!) {
            getData("Bearer " + user.userData.token)
        }else{
            getData(null)
        }
        show.setOnClickListener { listDialog.show() }
        rate.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, CommentsActivity::class.java)
                intent.putExtra("id", id)
                startActivity(intent)
            }}
        rating.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, CommentsActivity::class.java)
                intent.putExtra("id", id)
                startActivity(intent)
            }}
        products.setOnClickListener {
//            if (providerDetailsModel?.maintenance==1){
//
//            }else {
                val intent = Intent(this, ProductsActivity::class.java)
                intent.putExtra("id", id)
                intent.putExtra("cat", cat.id)
                intent.putExtra("provider", providerDetailsModel)
                startActivity(intent)
           // }
        }
        services.setOnClickListener {
//            if (providerDetailsModel?.maintenance==1){
//
//            }else {
                val intent = Intent(this, ServicesActivity::class.java)
                intent.putExtra("id", id)
               intent.putExtra("cat", cat.id)
                intent.putExtra("provider", providerDetailsModel)
                startActivity(intent)
           // }
        }

        chat.setOnClickListener {
            if (user.loginStatus!!){
                getID()
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }

        heart.setOnClickListener { if(user.loginStatus!!){

            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddFav("Bearer "+user.userData.token!!,lang.appLanguage,id,cat.id!!)?.enqueue(object:
                Callback<FavRespopnse>{
                override fun onFailure(call: Call<FavRespopnse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

                override fun onResponse(
                    call: Call<FavRespopnse>,
                    response: Response<FavRespopnse>
                ) {
                    hideProgressDialog()
                    if(response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                             CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            if (response.body()?.data == 0){
                                heart.setImageResource(R.mipmap.heart)
                            }else{
                                heart.setImageResource(R.mipmap.like)
                            }

                        }
                        else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
        }
           else{
            CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }

    }

    fun getData(token:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderDetails(lang.appLanguage,id,token,lat,lng)?.enqueue(object:
            Callback<ProviderDetailsResponse>{
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                Log.e("response",Gson().toJson(t))
            }

            override fun onResponse(
                call: Call<ProviderDetailsResponse>,
                response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        providerDetailsModel = response.body()?.data!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        reciver = response.body()?.data?.user_id!!
                        name.text = response.body()?.data?.name
                        rating.rating = response.body()?.data?.rate!!
                        rate.text = "("+response.body()?.data?.comments.toString()+")"
                        address.text = response.body()?.data?.address
                        description.text = response.body()?.data?.desc
                        if (response.body()?.data?.favorite == 0){
                            heart.setImageResource(R.mipmap.heart)
                        }else{
                            heart.setImageResource(R.mipmap.like)
                        }
                        if (response.body()?.data?.closed == 0){
                            status.text = getString(R.string.opened)
                        }else{
                            status.text = getString(R.string.closed)
                        }
                        times.text = response.body()?.data?.work_time_from+" _ "+response.body()?.data?.work_time_to
                        if (response.body()?.data?.products==0){
                            products.visibility = View.GONE
                        }else{
                            products.visibility = View.VISIBLE
                        }
                        if (response.body()?.data?.services==0){
                            services.visibility = View.GONE
                        }else{
                            services.visibility = View.VISIBLE
                        }
                        workTimesModels = response.body()?.data?.work_times!!
                        listDialog = ListDialog(mContext,this@ProviderDetailsActivity,workTimesModels,getString(R.string.work_times))
                    }
                    else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Conversation("Bearer"+user.userData.token,lang.appLanguage,reciver)?.enqueue(object :
            Callback<ConversationIdResponse>{
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ConversationIdResponse>,
                response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@ProviderDetailsActivity,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {

    }

    override fun onStartTracker() {

    }
}