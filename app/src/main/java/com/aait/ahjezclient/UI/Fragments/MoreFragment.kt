package com.aait.ahjezclient.UI.Fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatDelegate
import com.aait.ahjezclient.Base.BaseFragment
import com.aait.ahjezclient.Models.TermsResponse
import com.aait.ahjezclient.Models.UserModel
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Activities.AppInfo.*
import com.aait.ahjezclient.UI.Activities.Auth.FavouriteActivity
import com.aait.ahjezclient.UI.Activities.Auth.LoginActivity
import com.aait.ahjezclient.UI.Activities.Auth.ProfileActivity
import com.aait.ahjezclient.UI.Activities.SplashActivity
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MoreFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_more
    companion object {
        fun newInstance(): MoreFragment {
            val args = Bundle()
            val fragment = MoreFragment()
            fragment.setArguments(args)
            return fragment
        }
    }

    lateinit var profile:LinearLayout
    lateinit var wallet:LinearLayout
    lateinit var favourite:LinearLayout
    lateinit var call_us:LinearLayout
    lateinit var about:LinearLayout
    lateinit var terms:LinearLayout
    lateinit var app_lang:LinearLayout
    lateinit var night: Switch
    lateinit var logout:LinearLayout
    lateinit var text:TextView
    lateinit var chats:LinearLayout
    var the = ""

    override fun initializeComponents(view: View) {
        profile = view.findViewById(R.id.profile)
        chats = view.findViewById(R.id.chats)
        wallet = view.findViewById(R.id.wallet)
        favourite = view.findViewById(R.id.favourite)
        call_us = view.findViewById(R.id.call_us)
        about = view.findViewById(R.id.about)
        terms = view.findViewById(R.id.terms)
        app_lang = view.findViewById(R.id.app_lang)
        night = view.findViewById(R.id.night)
        logout = view.findViewById(R.id.logout)
        text = view.findViewById(R.id.text)
        if (user.loginStatus!!){
            text.text = getString(R.string.logout)
        }else{
            text.text = getString(R.string.login)
        }

        profile.setOnClickListener {
            if (user.loginStatus!!) {
                startActivity(Intent(activity, ProfileActivity::class.java))
            }else{
                 CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }
        chats.setOnClickListener {
            if (user.loginStatus!!){
                startActivity(Intent(activity,ChatsActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }
        about.setOnClickListener { startActivity(Intent(activity, AboutActivity::class.java)) }
        terms.setOnClickListener { startActivity(Intent(activity, TermsActivity::class.java)) }
        call_us.setOnClickListener { startActivity(Intent(activity, CallUsActivity::class.java))  }
        wallet.setOnClickListener {
            if (user.loginStatus!!) {
                startActivity(Intent(activity, WalletActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }
        app_lang.setOnClickListener { startActivity(Intent(activity, AppLangActivity::class.java))  }
        favourite.setOnClickListener {
            if (user.loginStatus!!) {
                startActivity(Intent(activity, FavouriteActivity::class.java))
            }
            else{
                CommonUtil.makeToast(mContext!!,getString(R.string.you_visitor))
            }
        }
        logout.setOnClickListener { if (user.loginStatus!!){
            val dialog = Dialog(mContext!!)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_logout)
            val yes = dialog?.findViewById<Button>(R.id.yes)
            val no = dialog?.findViewById<Button>(R.id.no)
            yes.setOnClickListener { logout()
            }
            no.setOnClickListener { dialog?.dismiss() }
            dialog?.show()
        }else{
            startActivity(Intent(activity,LoginActivity::class.java))
            activity?.finish()
        }
        }

        if (AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_NO){
            night.isChecked = false
        }else{
            night.isChecked = true
        }


        night.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (isChecked) {
               // theme.appTheme =
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }else{
              //  theme.appTheme =
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }
        })

//        night.setOnClickListener {
////            if (theme.appTheme = AppCompatDelegate.MODE_NIGHT_NO){
////
////                }
////                finishAffinity()
////                startActivity(Intent(this, SplashActivity::class.java)))
//
//            if (night.isChecked){
//
//                theme.appTheme = AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
//               activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
//
//            }else{
//                theme.appTheme = AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
//            }
//        }


    }

    fun logout(){
//        user.loginStatus=false
//        user.Logout()
//      //  CommonUtil.makeToast(mContext!!,response.body()?.data!!)
//        startActivity(Intent(activity, SplashActivity::class.java))
        showProgressDialog(getString(R.string.please_wait))
       // Log.e("device",Gson().toJson(user.userData))
        Client.getClient()?.create(Service::class.java)
            ?.logOut("Bearer"+user.userData.token
                ,user.userData.device_id!!,"android",lang.appLanguage,user.userData.mac_address_id!!)
            ?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<TermsResponse>,
                response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()
                        CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity?.finish()
                    }else{
                        user.loginStatus=false
                        user.Logout()
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity?.finish()

                    }
                }
            }
        })
    }
}