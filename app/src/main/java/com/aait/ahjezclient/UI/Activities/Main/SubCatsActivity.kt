package com.aait.ahjezclient.UI.Activities.Main

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezclient.Base.ParentActivity
import com.aait.ahjezclient.Cart.AllCartViewModel
import com.aait.ahjezclient.Cart.CartDataBase
import com.aait.ahjezclient.Cart.ProviderModelOffline
import com.aait.ahjezclient.Listeners.OnItemClickListener
import com.aait.ahjezclient.Models.CatsModel
import com.aait.ahjezclient.Models.CatsResponse
import com.aait.ahjezclient.Network.Client
import com.aait.ahjezclient.Network.Service
import com.aait.ahjezclient.R
import com.aait.ahjezclient.UI.Controllers.CatsAdapter
import com.aait.ahjezclient.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubCatsActivity:ParentActivity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this, ProvidersActivity::class.java)
        intent.putExtra("cat",cataModels.get(position))
        startActivity(intent)
    }

    override val layoutResource: Int
        get() = R.layout.activity_subcts
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var gridLayoutManager: GridLayoutManager
    var cataModels = ArrayList<CatsModel>()
    lateinit var catsAdapter: CatsAdapter
    lateinit var Cat:CatsModel
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    var coun = 0
    lateinit var count:TextView
    lateinit var basket:ImageView
    override fun initializeComponents() {
        Cat = intent.getSerializableExtra("cat") as CatsModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        count = findViewById(R.id.count)
        basket = findViewById(R.id.basket)
        back.setOnClickListener { onBackPressed()
        finish()}
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.VISIBLE
        }
        notification.setOnClickListener {  if (user.loginStatus!!){
            startActivity(Intent(this,NotificationActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
        }}
        basket.setOnClickListener { startActivity(Intent(this,BasketActivity::class.java))
        finish()}
        title.text = Cat.name
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        gridLayoutManager = GridLayoutManager(mContext!!,2)
        catsAdapter = CatsAdapter(mContext!!,cataModels,R.layout.recycler_category)
        catsAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = gridLayoutManager
        rv_recycle.adapter = catsAdapter
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)

        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels

                if (cartModels.size==0){
                    count.visibility = View.GONE
                }else {
                    count.visibility = View.VISIBLE
                    count.text = cartModels.size.toString()
                }
            }
        })
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()


    }

    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.SubCats(lang.appLanguage,Cat.id!!)?.enqueue(object:
            Callback<CatsResponse> {
            override fun onFailure(call: Call<CatsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<CatsResponse>,
                response: Response<CatsResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            catsAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}