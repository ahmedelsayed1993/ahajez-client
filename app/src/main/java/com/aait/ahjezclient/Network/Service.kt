package com.aait.ahjezclient.Network

import com.aait.ahjezclient.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Service {

    @FormUrlEncoded
    @POST("sign-up")
    fun SignUp(@Field("name") name:String,
               @Field("phone") phone:String,
               @Field("email") email:String?,
               @Field("password") password:String,
               @Field("device_id") device_id:String,
               @Field("device_type") device_type:String,
               @Header("lang") lang:String,
               @Field("mac_address_id") mac_address_id:String): Call<UserResponse>

    @FormUrlEncoded
    @POST("check-code")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Field("code") code:String,
                  @Header("lang") lang: String):Call<UserResponse>

    @POST("resend-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String):Call<UserResponse>
    @POST("send-code-email")
    fun ResendEmail(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String):Call<UserResponse>
    @FormUrlEncoded
    @POST("sign-in")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Header("lang") lang: String,
              @Field("mac_address_id") mac_address_id:String):Call<UserResponse>

    @POST("forget-password")
    fun ForGot(@Query("phone") phone:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Query("password") password:String,
                @Query("code") code:String,
                @Query("lang") lang: String):Call<UserResponse>
    @FormUrlEncoded
    @POST("reset-password")
    fun resetPassword(@Header("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Field("current_password") current_password:String,
                      @Field("password") password:String):Call<BaseResponse>

    @POST("about")
    fun About(@Header("lang") lang: String):Call<TermsResponse>

    @POST("terms")
    fun Terms(@Header("lang") lang: String,
             @Query("type") type: String):Call<TermsResponse>

    @FormUrlEncoded
    @POST("contact-us")
    fun Contact(@Header("lang") lang:String,
                @Field("name") name:String,
                @Field("phone") phone:String,
                @Field("message") message:String):Call<TermsResponse>

    @POST("portfolio")
    fun Balance(@Header("Authorization") Authorization:String,
                    @Header("lang") lang: String):Call<BalanceResponse>

    @POST("categories")
    fun Cats(@Header("lang") lang:String):Call<CatsResponse>
    @POST("subcategories")
    fun SubCats(@Header("lang") lang:String,
                @Query("category_id") category_id:Int):Call<CatsResponse>


    @FormUrlEncoded
    @POST("edit-profile")
    fun getProfile(@Header("Authorization") Authorization:String,
             @Header("lang") lang:String,
             @Field("name") name:String?,
             @Field("phone") phone: String?,
                   @Field("email") email: String?,
                   @Field("mac_address_id") mac_address_id:String):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun AddImage(@Header("Authorization") Authorization:String,
                 @Header("lang") lang:String,
                 @Part avatar: MultipartBody.Part,
                 @Query("mac_address_id") mac_address_id:String):Call<UserResponse>

    @FormUrlEncoded
    @POST("user-providers")
    fun getProviders(@Header("lang") lang:String,
                     @Field("subcategory_id") subcategory_id:Int,
                     @Field("lat") lat:String,
                     @Field("lng") lng:String,
                     @Field("rate") rate:Int?,
                     @Field("distance") distance:Int?,
                     @Field("orders") orders:Int?,
                     @Field("text") text:String?):Call<ProvidersResponse>

    @FormUrlEncoded
    @POST("details-provider")
    fun ProviderDetails(@Header("lang") lang:String,
                        @Field("provider_id") provider_id:Int
                       ,@Header("Authorization") Authorization:String?,
    @Field("lat") lat: String,
    @Field("lng") lng: String):Call<ProviderDetailsResponse>
    @FormUrlEncoded
    @POST("add-favorite")
    fun AddFav(@Header("Authorization") Authorization:String,
                 @Header("lang") lang:String,
                 @Field("provider_id") provider_id:Int,
                 @Field("subcategory_id") subcategory_id: Int):Call<FavRespopnse>

    @FormUrlEncoded
    @POST("provider-products")
    fun Products(@Header("lang") lang: String,
                 @Field("provider_id") provider_id:Int,
                 @Field("subcategory_id") subcategory_id:Int?,
                 @Field("subsection_id") subsection_id:Int?):Call<ProductsResponse>

    @FormUrlEncoded
    @POST("provider-services")
    fun Services(@Header("lang") lang: String,
                 @Field("provider_id") provider_id:Int,
                 @Field("subcategory_id") subcategory_id:Int?):Call<ServicesReponse>


    @FormUrlEncoded
    @POST("user-details-service")
    fun ServiceDetails(@Header("lang") lang: String,
                       @Field("service_id") service_id:Int):Call<ServiceDetailsResponse>

    @FormUrlEncoded
    @POST("dates-service-provider")
    fun Times(@Header("Authorization") Authorization:String,
              @Header("lang") lang:String,
              @Field("provider_id") provider_id:Int,
              @Field("service_id") service_id:Int,
              @Field("date") date:String):Call<TimesDatesResponse>

    @FormUrlEncoded
    @POST("reservation-service")
    fun Reservation(@Header("Authorization") Authorization:String,
                    @Header("lang") lang:String,
                    @Field("provider_id") provider_id:Int,
                    @Field("service_id") service_id:Int,
                    @Field("payment") payment:String,
                    @Field("total") total:String,
                    @Field("date") date:String,
                    @Field("time") time:String,
                    @Field("payment_free") payment_free:Int,
                    @Field("notes") notes:String?):Call<FavRespopnse>

    @FormUrlEncoded
    @POST("payment-reservation")
    fun Pay(@Header("Authorization") Authorization:String,
            @Header("lang") lang:String,
            @Field("reservation_id") reservation_id:Int,
            @Field("payment_method") payment_method:String):Call<TermsResponse>
    @FormUrlEncoded
    @POST("payment-order")
    fun PayOrder(@Header("Authorization") Authorization:String,
            @Header("lang") lang:String,
            @Field("order_id") reservation_id:Int,
            @Field("payment") payment_method:String):Call<TermsResponse>

    @FormUrlEncoded
    @POST("my-orders")
    fun Orders(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String,
                     @Field("status") status:String):Call<ReservationResponse>

    @FormUrlEncoded
    @POST("my-reservations")
    fun Reservations(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String,
                     @Field("status") status:String):Call<ReservationResponse>

    @FormUrlEncoded
    @POST("details-reservation")
    fun ReservationDetails(@Header("Authorization") Authorization:String,
                           @Header("lang") lang:String,
                           @Field("reservation_id") reservation_id:Int,
                           @Field("status") status:String?,
                           @Field("cancel_id") cancel_id:Int?):Call<ReservationDetailsResponse>
    @POST("cancellations")
    fun Cancellation(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String):Call<CancelResponse>
    @POST("reports")
    fun Reports(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String):Call<CancelResponse>

    @POST("order-report")
    fun Repot(@Header("Authorization") Authorization:String,
              @Header("lang") lang:String,
              @Query("report_id") report_id:Int,
              @Query("order_id") order_id:Int,
              @Query("type") type:String):Call<TermsResponse>
    @POST("user-details-product")
    fun getProduct(@Header("lang") lang:String,
                   @Query("product_id") provider_id: Int):Call<ProductDetailsResponse>

    @FormUrlEncoded
    @POST("conversation-id")
    fun Conversation(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String,
                     @Field("receiver_id") receiver_id:Int):Call<ConversationIdResponse>
    @POST("send-message")
    fun Send(@Header("lang") lang: String,
             @Header("Authorization") Authorization:String,
             @Query("receiver_id") receiver_id:Int,
             @Query("conversation_id") conversation_id:Int,
             @Query("type") type:String,
             @Query("message") message:String):Call<SendChatResponse>

    @Multipart
    @POST("send-message")
    fun SendImage(@Header("lang") lang: String,
             @Header("Authorization") Authorization:String,
             @Query("receiver_id") receiver_id:Int,
             @Query("conversation_id") conversation_id:Int,
             @Query("type") type:String,
             @Part message: MultipartBody.Part):Call<SendChatResponse>

    @POST("conversation")
    fun Conversation(@Query("lang") lang: String,
                     @Query("conversation_id") conversation_id:Int,
                     @Header("Authorization") Authorization:String,
                     @Query("page") page:Int,
                     @Query("app_type") app_type:String):Call<ChatResponse>

    @POST("my-conversations")
    fun Conversations(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String):Call<ChatsResponse>
    @POST("notifications")
    fun Notification(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String):Call<NotificationResponse>
    @POST("bank-accounts")
    fun Banks(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String):Call<BanksResponse>
    @Multipart
    @POST("payment-bank-transfer")
    fun Transfer(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String,
                 @Query("account_name") account_name:String,
                 @Query("bank_name") bank_name:String,
                 @Query("amount") amount:String,
                 @Part image:MultipartBody.Part):Call<TermsResponse>

    @POST("balance-recovery")
    fun Recover(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String,
                 @Query("message") message:String,
                @Query("bank_name") bank_name: String,
                @Query("account_name") account_name:String,
                @Query("account_number") account_number:String,
                @Query("iban_number") iban:String,
                 @Query("amount") amount:String):Call<TermsResponse>
    @POST("my-favorites")
    fun MyFavs(@Header("Authorization") Authorization:String,
    @Header("lang") lang:String):Call<MyFavResponse>

    @POST("log-out")
    fun logOut(@Header("Authorization") Authorization:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Header("lang") lang:String,
               @Query("mac_address_id") mac_address_id:String):Call<TermsResponse>

    @POST("add-order")
    fun AddOrder(@Header("Authorization") Authorization:String,
                 @Header("lang") lang:String,
                 @Query("provider_id") provider_id: Int,
                 @Query("carts") carts:String,
                 @Query("total") total:String,
                 @Query("total_tax") total_tax:String,
                 @Query("tax") tax:String,
                 @Query("final_total") final_total:String,
                 @Query("phone") phone: String?,
                 @Query("lat") lat: String?,
                 @Query("lng") lng: String?,
                 @Query("address") address:String?,
                 @Query("notes") notes:String,
                 @Query("check_delivery") check_delivery:Int?):Call<FavRespopnse>

    @POST("costs")
    fun Coasts():Call<TaxResponse>

    @POST("order-details")
    fun getOrder(@Header("Authorization") Authorization:String,
                 @Header("lang") lang:String,
                 @Query("order_id") order_id:Int,
                 @Query("type") type: String,
                  @Query("action") action:String?):Call<OrderDetailsResponse>

    @POST("add-rate")
    fun AddRate(@Header("Authorization") Authorization:String,
                @Header("lang") lang:String,
                @Query("order_id") order_id:Int,
                @Query("type") type: String,
                @Query("rate") rate: Int,
                @Query("order_type") order_type:String,
                @Query("comment") comment:String?):Call<TermsResponse>

    @POST("delete-notification")
    fun delete(
        @Header("Authorization") Authorization:String,
        @Header("lang") lang:String,
        @Query("notification_id") notification_id:Int):Call<TermsResponse>

    @POST("provider-comments")
    fun Comments(@Header("Authorization") Authorization:String,@Header("lang") lang:String,
                 @Query("provider_id") provider_id: Int):Call<CommentsResponse>

    @POST("delete-order")
    fun DeleteOrder(@Header("Authorization") Authorization:String,@Header("lang") lang:String,
                    @Query("order_id") order_id: Int):Call<TermsResponse>
}