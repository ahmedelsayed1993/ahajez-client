package com.aait.ahjezproviderapp.UI.Fragments

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezproviderapp.Base.BaseFragment
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.HiddenResponse
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.Models.MainProductsResponse
import com.aait.ahjezproviderapp.Models.ProductModel
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.ShowProductActivity
import com.aait.ahjezproviderapp.UI.Controllers.ProductAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HidenProductsFragment:BaseFragment(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.app_recycle

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    var productModels = ArrayList<ProductModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var productAdapter: ProductAdapter
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext!!,LinearLayoutManager.VERTICAL,false)
        productAdapter = ProductAdapter(mContext!!,productModels,R.layout.recycler_products)
        productAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = productAdapter

        swipeRefresh!!.setColorSchemeResources(
                R.color.colorGreen,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()
    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getHidden(lang.appLanguage,"Bearer"+user.userData.token,"products")?.enqueue(object :
                Callback<HiddenResponse> {
            override fun onResponse(call: Call<HiddenResponse>, response: Response<HiddenResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            // Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                            } else {
//                            initSliderAds(response.body()?.slider!!)

                                productAdapter.updateAll(response.body()!!.data!!)

                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<HiddenResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }
    override fun onItemClick(view: View, position: Int) {
      val intent = Intent(activity,ShowProductActivity::class.java)
        intent.putExtra("id",productModels.get(position).id)
        startActivity(intent)

    }
}