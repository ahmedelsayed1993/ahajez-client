package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.ahjezproviderapp.Base.ParentRecyclerAdapter
import com.aait.ahjezproviderapp.Base.ParentRecyclerViewHolder
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.R

class SectionsAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
        ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected :Int = 0

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.sub!!.setText(questionModel.name)
        questionModel.checked = (selected==position)
        if(questionModel.checked!!){
            viewHolder.line.background = mcontext.getDrawable(R.color.colorGreen)
            viewHolder.sub.textColor = mcontext.resources.getColor(R.color.colorGreen)
        }else{
            viewHolder.line.background = mcontext.getDrawable(R.color.colorGray)
            viewHolder.sub.textColor = mcontext.resources.getColor(R.color.colorPrimaryDark)
        }
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)

        viewHolder.sub.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var sub=itemView.findViewById<TextView>(R.id.sub)
        internal var line = itemView.findViewById<TextView>(R.id.line)





    }
}