package com.aait.ahjezproviderapp.UI.Activities

import android.content.Intent
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Auth.LoginActivity
import com.aait.ahjezproviderapp.UI.Activities.Auth.RegisterActivity


class PreLoginActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_pre_login
    lateinit var login:LinearLayout
    lateinit var register:LinearLayout


    override fun initializeComponents() {
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)

        login.setOnClickListener { startActivity(Intent(this, LoginActivity::class.java)) }
        register.setOnClickListener { startActivity(Intent(this, RegisterActivity::class.java)) }
//

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}