package com.aait.ahjezproviderapp.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R

class IntroFourActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_intro_one
    lateinit var image: ImageView
    lateinit var title: TextView

    lateinit var next: Button
    lateinit var skip: TextView
    lateinit var one:ImageView
    lateinit var three:ImageView

    override fun initializeComponents() {
        image = findViewById(R.id.image)
        title =findViewById(R.id.title)
        next = findViewById(R.id.next)
        skip = findViewById(R.id.skip)
        one = findViewById(R.id.one)
        three = findViewById(R.id.three)
        one.setImageResource(R.drawable.gray_circle)
        three.setImageResource(R.drawable.black_circle)
        image.setImageResource(R.mipmap.fourteen)
        title.text = getString(R.string.Book_your_service_with_hundreds_of_agencies)
        next.setOnClickListener { startActivity(Intent(this,IntroFiveActivity::class.java)) }
        skip.setOnClickListener { val intent = Intent(this, PreLoginActivity::class.java)
            startActivity(intent)
            finish() }
    }
}