package com.aait.ahjezproviderapp.UI.Activities.Auth

import android.app.Dialog
import android.content.Intent
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.UserModel
import com.aait.ahjezproviderapp.Models.UserResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.IntroOneActivity
import com.aait.ahjezproviderapp.UI.Activities.IntroThreeActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivationCodeActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_activation_code
    lateinit var confirm:Button
    lateinit var one:EditText
    lateinit var two:EditText
    lateinit var three:EditText
    lateinit var four:EditText
    lateinit var resend:TextView
    lateinit var send_mail:LinearLayout
   lateinit var userModel:UserModel
    lateinit var text:TextView
    var code = ""
    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        confirm = findViewById(R.id.confirm)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        text = findViewById(R.id.text)
        resend = findViewById(R.id.resend)
        send_mail = findViewById(R.id.send_mail)
        resend.isEnabled = false
        val timer = object: CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                text.text = millisUntilFinished.div(1000) .toString()
            }

            override fun onFinish() {
                resend.isEnabled = true
            }
        }
        timer.start()
        code = one.text.toString()+two.text.toString()+three.text.toString()+four.text.toString()
        Log.e("code",code)
        one.setSelection(0)
        two.setSelection(0)
        three.setSelection(0)
        four.setSelection(0)
        one.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                two.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        two.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                three.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        three.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                four.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(one,getString(R.string.activation_code))||
                CommonUtil.checkEditError(two,getString(R.string.activation_code))||
                CommonUtil.checkEditError(three,getString(R.string.activation_code))||
                CommonUtil.checkEditError(four,getString(R.string.activation_code))){
                return@setOnClickListener
            }else{
                var code = one.text.toString()+two.text.toString()+three.text.toString()+four.text.toString()
                check(code)

            } }
        resend.setOnClickListener { Resend() }
        send_mail.setOnClickListener {
            if (userModel.email.equals("")){
                val dialog = Dialog(mContext)
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog?.window!!.setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                )
                dialog?.setCancelable(true)
                dialog?.setContentView(R.layout.dialog_sorry)


                val ok = dialog?.findViewById<Button>(R.id.ok)


                ok?.setOnClickListener {
                    dialog?.dismiss()
                }
                dialog?.show()
            }else {
                ResendEmail()
            }
        }

    }
    fun check(code:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode("Bearer "+userModel.token!!,code,lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
//                        user.loginStatus = true
//                        user.userData = response.body()?.data!!
                        startActivity(Intent(this@ActivationCodeActivity, DoneActivity::class.java))
                        this@ActivationCodeActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Resend("Bearer "+userModel.token!!,lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<UserResponse>,
                response: Response<UserResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        resend.isEnabled = false
                        val timer = object: CountDownTimer(60000, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                                text.text = millisUntilFinished.div(1000) .toString()
                            }

                            override fun onFinish() {
                                resend.isEnabled = true
                            }
                        }
                        timer.start()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun ResendEmail(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ResendEmail("Bearer "+userModel.token!!,lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<UserResponse>,
                response: Response<UserResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        val intent = Intent(this@ActivationCodeActivity,CheckCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}