package com.aait.ahjezproviderapp.UI.Activities.Main

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Fragments.MoreFragment
import com.aait.ahjezproviderapp.UI.Fragments.OrdersFragment
import com.aait.ahjezproviderapp.UI.Fragments.ProductFragments
import com.aait.ahjezproviderapp.UI.Fragments.ServicesFragment

class MainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_main

    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var line: TextView
    lateinit var my_reservations: LinearLayout
    lateinit var line1: TextView
    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var purchase: LinearLayout
    lateinit var line2: TextView
    lateinit var purchase_image: ImageView
    lateinit var purchase_text: TextView
    lateinit var more: LinearLayout
    lateinit var line3: TextView
    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var state = ""
    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var productFragments: ProductFragments
    internal lateinit var servicesFragment: ServicesFragment
    internal lateinit var ordersFragment: OrdersFragment
    internal lateinit var moreFragment: MoreFragment
    lateinit var title: TextView
    lateinit var notification: ImageView


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initializeComponents() {
        line = findViewById(R.id.line)
        line1 = findViewById(R.id.line1)
        line2 = findViewById(R.id.line2)
        line3 = findViewById(R.id.line3)
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        purchase = findViewById(R.id.purchase)
        purchase_image = findViewById(R.id.purchase_image)
        purchase_text = findViewById(R.id.purchase_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        productFragments = ProductFragments.newInstance()
        servicesFragment = ServicesFragment.newInstance()
        ordersFragment = OrdersFragment.newInstance()
        moreFragment = MoreFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, productFragments)
        transaction!!.add(R.id.home_fragment_container, servicesFragment)
        transaction!!.add(R.id.home_fragment_container,ordersFragment)
        transaction!!.add(R.id.home_fragment_container,moreFragment)
        transaction!!.commit()
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java)) }
        showhome()
        home.setOnClickListener { showhome() }
        my_reservations.setOnClickListener {
            if (user.loginStatus!!) {
                showreservation()
            }else{

            }
        }
        purchase.setOnClickListener {
            if (user.loginStatus!!) {
                showpurchase()
            }else{

            }
        }
        more.setOnClickListener { showmore() }
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showhome(){
        selected = 1
        home_image.setImageResource(R.mipmap.homeactive)
        home_text.textColor = Color.parseColor("#2B726F")
        reservation_text.textColor = Color.parseColor("#CFCFCF")
        purchase_text.textColor = Color.parseColor("#CFCFCF")
        more_text.textColor = Color.parseColor("#CFCFCF")
        line.background = mContext.getDrawable(R.color.colorGreen)
        line1.background = mContext.getDrawable(R.color.colorWhite)
        line2.background = mContext.getDrawable(R.color.colorWhite)
        line3.background = mContext.getDrawable(R.color.colorWhite)
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) {
            reservation_image.setImageResource(R.mipmap.booking)
            purchase_image.setImageResource(R.mipmap.group)
            more_image.setImageResource(R.mipmap.more)
        }else{
            reservation_image.setImageResource(R.mipmap.bookingdak)
            purchase_image.setImageResource(R.mipmap.groupdark)
            more_image.setImageResource(R.mipmap.moredark)
        }
        title.text = getString(R.string.products)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, productFragments)
        transaction!!.commit()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showreservation(){
        selected = 1
        reservation_image.setImageResource(R.mipmap.bookingactive)
        reservation_text.textColor = Color.parseColor("#2B726F")
        home_text.textColor = Color.parseColor("#CFCFCF")
        purchase_text.textColor = Color.parseColor("#CFCFCF")
        more_text.textColor = Color.parseColor("#CFCFCF")
        line1.background = mContext.getDrawable(R.color.colorGreen)
        line.background = mContext.getDrawable(R.color.colorWhite)
        line2.background = mContext.getDrawable(R.color.colorWhite)
        line3.background = mContext.getDrawable(R.color.colorWhite)
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) {
            home_image.setImageResource(R.mipmap.homee)
            purchase_image.setImageResource(R.mipmap.group)
            more_image.setImageResource(R.mipmap.more)
        }else{
            home_image.setImageResource(R.mipmap.homeedark)
            purchase_image.setImageResource(R.mipmap.groupdark)
            more_image.setImageResource(R.mipmap.moredark)
        }
        title.text = getString(R.string.Appointments)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, servicesFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showpurchase(){
        selected = 1
        purchase_image.setImageResource(R.mipmap.groupactive)
        purchase_text.textColor = Color.parseColor("#2B726F")
        reservation_text.textColor = Color.parseColor("#CFCFCF")
        home_text.textColor = Color.parseColor("#CFCFCF")
        more_text.textColor = Color.parseColor("#CFCFCF")
        line2.background = mContext.getDrawable(R.color.colorGreen)
        line1.background = mContext.getDrawable(R.color.colorWhite)
        line.background = mContext.getDrawable(R.color.colorWhite)
        line3.background = mContext.getDrawable(R.color.colorWhite)
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) {
            reservation_image.setImageResource(R.mipmap.booking)
            home_image.setImageResource(R.mipmap.homee)
            more_image.setImageResource(R.mipmap.more)
        }else{
            reservation_image.setImageResource(R.mipmap.bookingdak)
            home_image.setImageResource(R.mipmap.homeedark)
            more_image.setImageResource(R.mipmap.moredark)
        }
        title.text = getString(R.string.orders)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, ordersFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showmore(){
        selected = 1
        more_image.setImageResource(R.mipmap.moreactive)
        more_text.textColor = Color.parseColor("#2B726F")
        reservation_text.textColor = Color.parseColor("#CFCFCF")
        purchase_text.textColor = Color.parseColor("#CFCFCF")
        home_text.textColor = Color.parseColor("#CFCFCF")
        line3.background = mContext.getDrawable(R.color.colorGreen)
        line1.background = mContext.getDrawable(R.color.colorWhite)
        line2.background = mContext.getDrawable(R.color.colorWhite)
        line.background = mContext.getDrawable(R.color.colorWhite)
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) {
            reservation_image.setImageResource(R.mipmap.booking)
            purchase_image.setImageResource(R.mipmap.group)
            home_image.setImageResource(R.mipmap.homee)
        }else{
            reservation_image.setImageResource(R.mipmap.bookingdak)
            purchase_image.setImageResource(R.mipmap.groupdark)
            home_image.setImageResource(R.mipmap.homeedark)
        }
        title.text = getString(R.string.more)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, moreFragment)
        transaction!!.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}