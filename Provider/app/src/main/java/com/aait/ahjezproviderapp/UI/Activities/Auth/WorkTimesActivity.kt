package com.aait.ahjezproviderapp.UI.Activities.Auth

import android.app.TimePickerDialog
import android.content.Intent

import android.util.Log
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.BaseResponse
import com.aait.ahjezproviderapp.Models.WorkTimesResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.MainActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class WorkTimesActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_work_times
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var time_from:TextView
    lateinit var time_to:TextView
    lateinit var saturday:CheckBox
    lateinit var sunday:CheckBox
    lateinit var monday:CheckBox
    lateinit var tuesday:CheckBox
    lateinit var wednesday:CheckBox
    lateinit var thursday:CheckBox
    lateinit var friday:CheckBox
    lateinit var extra_from:TextView
    lateinit var extra_to:TextView
    lateinit var save:Button
    var days = ArrayList<String>()
    override fun initializeComponents() {
        days.clear()
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        time_from = findViewById(R.id.time_from)
        time_to = findViewById(R.id.time_to)
        saturday = findViewById(R.id.saturday)
        sunday = findViewById(R.id.sunday)
        monday = findViewById(R.id.monday)
        tuesday = findViewById(R.id.tuesday)
        wednesday = findViewById(R.id.wednesday)
        thursday = findViewById(R.id.thursday)
        friday = findViewById(R.id.friday)
        extra_from = findViewById(R.id.extra_from)
        extra_to = findViewById(R.id.extra_to)
        save = findViewById(R.id.save)
        title.text = getString(R.string.work_times)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        saturday.setOnClickListener {
            if (saturday.isChecked){
                days.add("saturday")
            }else{
                if (days.contains("saturday")){
                    days.remove("saturday")
                }else{

                }
            }
        }
        sunday.setOnClickListener {
            if (sunday.isChecked){
                days.add("sunday")
            }else{
                if (days.contains("sunday")){
                    days.remove("sunday")
                }else{

                }
            }
        }
        monday.setOnClickListener {
            if (monday.isChecked){
                days.add("monday")
            }else{
                if (days.contains("monday")){
                    days.remove("monday")
                }else{

                }
            }
        }
        tuesday.setOnClickListener {
            if (tuesday.isChecked){
                days.add("tuesday")
            }else{
                if (days.contains("tuesday")){
                    days.remove("tuesday")
                }else{

                }
            }
        }
        wednesday.setOnClickListener {
            if (wednesday.isChecked){
                days.add("wednesday")
            }else{
                if (days.contains("wednesday")){
                    days.remove("wednesday")
                }else{

                }
            }
        }
        thursday.setOnClickListener {
            if (thursday.isChecked){
                days.add("thursday")
            }else{
                if (days.contains("thursday")){
                    days.remove("thursday")
                }else{

                }

            }
        }
        friday.setOnClickListener {
            if (friday.isChecked){
                days.add("friday")
            }else{
                if (days.contains("friday")){
                    days.remove("friday")
                }else{

                }
            }
        }
        time_from.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)



            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)

                            var hour = hourOfDay
                            var minute = minute
                            // hour1 = hourOfDay.toString()
                            var am_pm = ""
                            // AM_PM decider logic
//                            when {
//                                hour == 0 -> {
//                                    hour += 12
//                                    am_pm = "AM"
//                                }
//                                hour == 12 -> am_pm = "PM"
//                                hour > 12 -> {
//                                    hour -= 12
//                                    am_pm = "PM"
//                                }
//                                else -> am_pm = "AM"
//                            }

                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            time_from.text = hours.toString()+":"+minutes.toString()

                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.from))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)

            timePickerDialog.show()
        }
        time_to.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)



            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)

                            var hour = hourOfDay
                            var minute = minute
                            // hour1 = hourOfDay.toString()
                            var am_pm = ""
                            // AM_PM decider logic
//                            when {
//                                hour == 0 -> {
//                                    hour += 12
//                                    am_pm = "AM"
//                                }
//                                hour == 12 -> am_pm = "PM"
//                                hour > 12 -> {
//                                    hour -= 12
//                                    am_pm = "PM"
//                                }
//                                else -> am_pm = "AM"
//                            }

                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            time_to.text = hours.toString()+":"+minutes.toString()

                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.to))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)

            timePickerDialog.show()
        }
        extra_from.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)



            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)

                            var hour = hourOfDay
                            var minute = minute
                            // hour1 = hourOfDay.toString()
                            var am_pm = ""
                            // AM_PM decider logic
//                            when {
//                                hour == 0 -> {
//                                    hour += 12
//                                    am_pm = "AM"
//                                }
//                                hour == 12 -> am_pm = "PM"
//                                hour > 12 -> {
//                                    hour -= 12
//                                    am_pm = "PM"
//                                }
//                                else -> am_pm = "AM"
//                            }

                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            extra_from.text = hours.toString()+":"+minutes.toString()

                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.from))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)

            timePickerDialog.show()
        }
        extra_to.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)



            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)

                            var hour = hourOfDay
                            var minute = minute
                            // hour1 = hourOfDay.toString()
                            var am_pm = ""
                            // AM_PM decider logic
//                            when {
//                                hour == 0 -> {
//                                    hour += 12
//                                    am_pm = "AM"
//                                }
//                                hour == 12 -> am_pm = "PM"
//                                hour > 12 -> {
//                                    hour -= 12
//                                    am_pm = "PM"
//                                }
//                                else -> am_pm = "AM"
//                            }

                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            extra_to.text = hours.toString()+":"+minutes.toString()

                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.from))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)

            timePickerDialog.show()
        }
        getTimes()
        save.setOnClickListener {
            if (CommonUtil.checkTextError(time_from,getString(R.string.from))||
                    CommonUtil.checkTextError(time_to,getString(R.string.to))){
                return@setOnClickListener
            }else{
                if (days.isEmpty()){
                    CommonUtil.makeToast(mContext,getString(R.string.work_days))
                }else{
                   Add()
                }
            }
        }

    }
    fun getTimes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddTimes(lang.appLanguage,"Bearer"+user.userData.token,null
                ,null,null,null,null)
                ?.enqueue(object : Callback<WorkTimesResponse>{
                    override fun onFailure(call: Call<WorkTimesResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(call: Call<WorkTimesResponse>, response: Response<WorkTimesResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                time_from.text = response.body()?.data?.time_from
                                time_to.text = response.body()?.data?.time_to
                                extra_from.text = response.body()?.data?.extra_from
                                extra_to.text = response.body()?.data?.extra_to
                                for (i in 0..response.body()?.data?.days!!.size-1){
                                    days.add(response.body()?.data?.days?.get(i)?.day!!)
                                    if (response.body()?.data?.days?.get(i)?.day.equals("saturday")){
                                        saturday.isChecked = true
                                    }else if (response.body()?.data?.days?.get(i)?.day.equals("sunday")){
                                        sunday.isChecked = true
                                    }else if (response.body()?.data?.days?.get(i)?.day.equals("monday")){
                                        monday.isChecked = true
                                    }
                                    else if (response.body()?.data?.days?.get(i)?.day.equals("tuesday")){
                                        tuesday.isChecked = true
                                    }
                                    else if (response.body()?.data?.days?.get(i)?.day.equals("wednesday")){
                                        wednesday.isChecked = true
                                    }
                                    else if (response.body()?.data?.days?.get(i)?.day.equals("thursday")){
                                        thursday.isChecked = true
                                    }
                                    else if (response.body()?.data?.days?.get(i)?.day.equals("friday")){
                                        friday.isChecked = true
                                    }
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun Add(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddTimes(lang.appLanguage,"Bearer"+user.userData.token,time_from.text.toString()
        ,time_to.text.toString(),days.joinToString(postfix = "",prefix = "" ,separator = ","),extra_from.text.toString(),extra_to.text.toString())
                ?.enqueue(object : Callback<WorkTimesResponse>{
                    override fun onFailure(call: Call<WorkTimesResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(call: Call<WorkTimesResponse>, response: Response<WorkTimesResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                startActivity(Intent(this@WorkTimesActivity,MainActivity::class.java))
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}