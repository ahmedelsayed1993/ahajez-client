package com.aait.ahjezproviderapp.UI.Activities.Main

import android.content.Intent
import android.widget.Button
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R

class DeleteServiceActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_delete_service
    lateinit var back: Button
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}