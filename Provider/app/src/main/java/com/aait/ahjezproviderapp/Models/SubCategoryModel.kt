package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class SubCategoryModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var checked:Int?=null
}