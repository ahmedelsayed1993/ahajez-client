package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class PackagesResponse : BaseResponse(),Serializable {
    var subscription:String?=null
    var data:ArrayList<PackagesModel>?=null
    var user:Int?=null
}