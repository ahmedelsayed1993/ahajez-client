package com.aait.ahjezproviderapp.UI.Activities.Main

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.*
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Controllers.ReasonsAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReservationDetailsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_reservation_details

    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var order_num: TextView
    lateinit var service_type: TextView
    lateinit var service_price: TextView
    lateinit var paid_amount: TextView
    lateinit var date: TextView
    lateinit var appointment_date: TextView
    lateinit var appointment_time: TextView
    lateinit var image: CircleImageView
    lateinit var name: TextView
    lateinit var rating: RatingBar
    lateinit var chat: ImageView



    lateinit var address: TextView
    lateinit var cancel: Button
    var id = 0

    var swipeRefresh: SwipeRefreshLayout? = null
    var photo = ""
    var provider = ""
    var reciver = 0
    lateinit var comment_lay: LinearLayout
    lateinit var rate: RatingBar
    lateinit var comment: TextView

    lateinit var lay_new:LinearLayout
    lateinit var accept:Button
    lateinit var refuse:Button
    lateinit var lay_current:LinearLayout
    lateinit var complete:Button
    lateinit var text:TextView
    lateinit var reason:TextView
    lateinit var ratee:TextView
    var reasonsModels = ArrayList<CancelModel>()
    var reasons = 0
    lateinit var reasonsAdapter: ReasonsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        chat = findViewById(R.id.chat)
        comment_lay = findViewById(R.id.comment_lay)
        comment = findViewById(R.id.comment)
        rate = findViewById(R.id.rate)
        text = findViewById(R.id.text)
        reason = findViewById(R.id.reason)
        notification = findViewById(R.id.notification)
        order_num = findViewById(R.id.order_num)
        service_type = findViewById(R.id.service_type)
        service_price = findViewById(R.id.service_price)
        paid_amount = findViewById(R.id.paid_amount)
        date = findViewById(R.id.date)
        appointment_date = findViewById(R.id.appointment_date)
        appointment_time = findViewById(R.id.appointment_time)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        address = findViewById(R.id.address)
        cancel = findViewById(R.id.cancel)
        lay_new = findViewById(R.id.lay_new)
        lay_current = findViewById(R.id.lay_current)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        complete = findViewById(R.id.complete)
        ratee = findViewById(R.id.ratee)

        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.reservation_details)
        ratee.setOnClickListener { val intent = Intent(this,CommentsActivity::class.java)
        intent.putExtra("id",id)
            intent.putExtra("type","reservations")
        startActivity(intent)}
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}

        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
        chat.setOnClickListener { getID() }
        accept.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.ReservationDetails("Bearer"+user.userData.token,lang.appLanguage,"reservations",id,"accepted",null)
                    ?.enqueue(object : Callback<ReservationDetailsResponse> {
                        override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                        }

                        @SuppressLint("ResourceAsColor")
                        override fun onResponse(
                                call: Call<ReservationDetailsResponse>,
                                response: Response<ReservationDetailsResponse>
                        ) {
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                     getData()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        complete.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.ReservationDetails("Bearer"+user.userData.token,lang.appLanguage,"reservations",id,"completed",null)
                    ?.enqueue(object : Callback<ReservationDetailsResponse> {
                        override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                        }

                        @SuppressLint("ResourceAsColor")
                        override fun onResponse(
                                call: Call<ReservationDetailsResponse>,
                                response: Response<ReservationDetailsResponse>
                        ) {
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    val dialog = Dialog(mContext)
                                    dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                    dialog ?.setCancelable(true)
                                    dialog ?.setContentView(R.layout.dialog_rate)
                                    val pro_image = dialog?.findViewById<CircleImageView>(R.id.pro_image)
                                    val pro_name = dialog?.findViewById<TextView>(R.id.pro_name)
                                    val rate = dialog?.findViewById<RatingBar>(R.id.rate)
                                    val comment = dialog?.findViewById<EditText>(R.id.comment)
                                    val confirm = dialog?.findViewById<Button>(R.id.confirm)
                                    Glide.with(mContext).load(photo).into(pro_image)
                                    pro_name.text = provider
                                    confirm.setOnClickListener {
                                        showProgressDialog(getString(R.string.please_wait))
                                        Client.getClient()?.create(Service::class.java)?.AddRate("Bearer"+user.userData.token,lang.appLanguage,id,"user",rate.rating.toInt(),comment.text.toString(),"reservation")
                                                ?.enqueue(object :Callback<TermsResponse>{
                                                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                                                        CommonUtil.handleException(mContext,t)
                                                        t.printStackTrace()
                                                        hideProgressDialog()
                                                        dialog?.dismiss()
                                                    }
                                                    override fun onResponse(
                                                            call: Call<TermsResponse>,
                                                            response: Response<TermsResponse>
                                                    ) {
                                                        hideProgressDialog()
                                                        dialog?.dismiss()
                                                        if (response.isSuccessful){
                                                            if (response.body()?.value.equals("1")){
                                                                CommonUtil.makeToast(mContext,response.body()?.data!!)
                                                                val intent = Intent(this@ReservationDetailsActivity,OrderDoneActivity::class.java)

                                                                startActivity(intent)
                                                                finish()
                                                            }else{
                                                                onBackPressed()
                                                                finish()
                                                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                                            }
                                                        }
                                                    }
                                                })
                                    }
                                    dialog?.show()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        refuse.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.cancel_dialog)
            val reasonss = dialog?.findViewById<RecyclerView>(R.id.reasons)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            val titl = dialog?.findViewById<TextView>(R.id.titl)
            titl.text = getString(R.string.reason_of_refusing)
            linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
            reasonsAdapter = ReasonsAdapter(mContext,reasonsModels,R.layout.recycle_reason)
            reasonsAdapter.setOnItemClickListener(this)
            reasonss.layoutManager = linearLayoutManager
            reasonss.adapter = reasonsAdapter
            getReasons()
            send.setOnClickListener { showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.ReservationDetails("Bearer"+user.userData.token,lang.appLanguage,"reservations",id,"refuse",reasons)
                        ?.enqueue(object :Callback<ReservationDetailsResponse>{
                            override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                hideProgressDialog()
                                dialog?.dismiss()
                            }

                            override fun onResponse(
                                    call: Call<ReservationDetailsResponse>,
                                    response: Response<ReservationDetailsResponse>
                            ) {
                                hideProgressDialog()
                                dialog?.dismiss()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        val dialog = Dialog(mContext)
                                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                        dialog ?.setCancelable(true)
                                        dialog ?.setContentView(R.layout.dailog_done)
                                        val text = dialog?.findViewById<TextView>(R.id.text)
                                        val back = dialog?.findViewById<Button>(R.id.ok)

                                        text.text = getString(R.string.refuse_done)

                                        //getReasons()

                                        back.setOnClickListener { dialog.dismiss()
                                            val intent = Intent(this@ReservationDetailsActivity,MainActivity::class.java)

                                            startActivity(intent)}
                                        dialog?.show()
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                        }) }
            cancel.setOnClickListener { dialog.dismiss() }
            dialog?.show()
        }

        cancel.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.cancel_dialog)
            val reasonss = dialog?.findViewById<RecyclerView>(R.id.reasons)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            val titl = dialog?.findViewById<TextView>(R.id.titl)
            titl.text = getString(R.string.reason_of_cancellation)
            linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
            reasonsAdapter = ReasonsAdapter(mContext,reasonsModels,R.layout.recycle_reason)
            reasonsAdapter.setOnItemClickListener(this)
            reasonss.layoutManager = linearLayoutManager
            reasonss.adapter = reasonsAdapter
            getReasons()
            send.setOnClickListener { showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.ReservationDetails("Bearer"+user.userData.token,lang.appLanguage,"reservations",id,"cancel",reasons)
                        ?.enqueue(object :Callback<ReservationDetailsResponse>{
                            override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                hideProgressDialog()
                                dialog?.dismiss()
                            }

                            override fun onResponse(
                                    call: Call<ReservationDetailsResponse>,
                                    response: Response<ReservationDetailsResponse>
                            ) {
                                hideProgressDialog()
                                dialog?.dismiss()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        val dialog = Dialog(mContext)
                                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                        dialog ?.setCancelable(true)
                                        dialog ?.setContentView(R.layout.dailog_done)
                                        val text = dialog?.findViewById<TextView>(R.id.text)
                                        val back = dialog?.findViewById<Button>(R.id.ok)

                                        text.text = getString(R.string.cancel_done)

                                        //getReasons()

                                        back.setOnClickListener { dialog.dismiss()
                                            val intent = Intent(this@ReservationDetailsActivity,MainActivity::class.java)

                                            startActivity(intent)}
                                        dialog?.show()
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                        }) }
            cancel.setOnClickListener { dialog.dismiss() }
            dialog?.show()
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ReservationDetails("Bearer"+user.userData.token,lang.appLanguage,"reservations",id,null,null)
                ?.enqueue(object : Callback<ReservationDetailsResponse> {
                    override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                        swipeRefresh!!.isRefreshing = false
                    }

                    @SuppressLint("ResourceAsColor")
                    override fun onResponse(
                            call: Call<ReservationDetailsResponse>,
                            response: Response<ReservationDetailsResponse>
                    ) {
                        hideProgressDialog()
                        swipeRefresh!!.isRefreshing = false
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                order_num.text = response.body()?.data?.id.toString()
                                service_type.text = response.body()?.data?.name_service
                                service_price.text = response.body()?.data?.service_price+getString(R.string.rs)
                                paid_amount.text = response.body()?.data?.total+getString(R.string.rs)
                                date.text = response.body()?.data?.created
                                reason.text = getString(R.string.reason_of_cancellation)+response.body()?.data?.cancellation
                                appointment_date.text = response.body()?.data?.date
                                appointment_time.text = response.body()?.data?.time
                                Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                                ratee.text = "("+response.body()?.data?.user_rates_count.toString()+")"
                                name.text = response.body()?.data?.name
                                rating.rating = response.body()?.data?.rate!!
                                address.text = getString(R.string.order_num)+": "+response.body()?.data?.id.toString()
                                provider = response.body()?.data?.name!!
                                photo = response.body()?.data?.avatar!!
                                reciver  = response.body()?.data?.chat_provider!!
                                if (response.body()?.data?.payment_free==0){
                                    cancel.visibility = View.VISIBLE
                                }else{
                                    cancel.visibility = View.GONE
                                }
                                if (response.body()?.data?.order_rate==0){
                                    comment_lay.visibility = View.GONE
                                }else{
                                    comment_lay.visibility = View.VISIBLE
                                    comment.text = response.body()?.data?.order_comment
                                    rate.rating = response.body()?.data?.order_rate!!.toFloat()
                                }
                                if (response.body()?.data?.status!!.equals("pending")){
                                    lay_new.visibility = View.VISIBLE
                                    lay_current.visibility = View.GONE

                                }else if (response.body()?.data?.status!!.equals("accepted")){
                                    lay_new.visibility = View.GONE
                                    lay_current.visibility = View.VISIBLE

                                }else if (response.body()?.data?.status!!.equals("completed")){

                                    lay_new.visibility = View.GONE
                                    lay_current.visibility = View.GONE
                                }else if (response.body()?.data?.status!!.equals("refuse")||response.body()?.data?.status.equals("cancel")){
                                    lay_new.visibility = View.GONE
                                    lay_current.visibility = View.GONE
                                    comment_lay.visibility = View.GONE
                                    text.visibility = View.VISIBLE
                                    reason.visibility = View.VISIBLE

                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }


    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Conversation("Bearer"+user.userData.token,lang.appLanguage,reciver)?.enqueue(object :
                Callback<ConversationIdResponse>{
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }
            override fun onResponse(
                    call: Call<ConversationIdResponse>,
                    response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@ReservationDetailsActivity,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.reason){
            reasonsAdapter.selected = position
            reasonsModels.get(position).selected = true
            reasonsAdapter.notifyDataSetChanged()
            reasons = reasonsModels.get(position).id!!
            Log.e("reason",reason.toString())

        }

    }
    fun getReasons(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cancellation("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object :Callback<CancelResponse>{
            override fun onFailure(call: Call<CancelResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                    call: Call<CancelResponse>,
                    response: Response<CancelResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        reasonsAdapter.updateAll(response.body()?.data!!)
                        if (response.body()?.data!!.size!=0){
                            reasons = response.body()?.data?.get(0)?.id!!
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}