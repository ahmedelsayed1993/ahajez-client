package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class CommentsResponse:BaseResponse(),Serializable {
    var data:ArrayList<CommentsModel>?=null
}