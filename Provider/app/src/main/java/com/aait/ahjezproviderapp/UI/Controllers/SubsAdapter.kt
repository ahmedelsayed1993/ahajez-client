package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentRecyclerAdapter
import com.aait.ahjezproviderapp.Base.ParentRecyclerViewHolder
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.R

class SubsAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
        ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        if (viewHolder.text.isChecked){
            listModel.checked = false
        }else{
            listModel.checked= true
        }
        viewHolder.text!!.setText(listModel.name)
        viewHolder.text.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var text=itemView.findViewById<CheckBox>(R.id.text)


    }
}