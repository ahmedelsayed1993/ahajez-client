package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


import com.aait.ahjezproviderapp.Models.ChatModel
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.Images
import com.bumptech.glide.Glide

import de.hdodenhof.circleimageview.CircleImageView

class ChatAdapter(
    internal var context: Context,
    internal var messageModels: List<ChatModel>,
    internal var id: Int
) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    internal var paramsMsg: LinearLayout.LayoutParams? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View
        return if (viewType == 0) {
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.card_chat_send,
                    parent,
                    false
                )
            )
        } else
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.card_chat_recieve,
                    parent,
                    false
                )
            )
    }

    override fun getItemViewType(position: Int): Int {

        return if (messageModels[position].receiver_id == id) {

            1
        } else {

            0
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (messageModels[position].messageType.equals("text")) {
            holder.chat.visibility = View.VISIBLE
            holder.image_lay.visibility = View.GONE
            holder.msg.text = messageModels[position].message
            holder.date.text = messageModels[position].created
            Glide.with(context).load(messageModels[position].avatar).into(holder.sender)
        }
        if (messageModels[position].messageType.equals("file")){
            holder.chat.visibility = View.GONE
            holder.image_lay.visibility = View.VISIBLE
            holder.time.text = messageModels[position].created
            Glide.with(context).load(messageModels[position].avatar).into(holder.sender)
            Glide.with(context).load(messageModels[position].message).into(holder.image)
        }
        holder.image.setOnClickListener {
           val intent = Intent(context,Images::class.java)
            intent.putExtra("link",messageModels[position].message)
            context.startActivity(intent)
        }



    }

    override fun getItemCount(): Int {
        return messageModels.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var msg: TextView
        internal var date: TextView
        internal var sender: CircleImageView
        internal var image_lay : RelativeLayout
        internal var chat :LinearLayout
        internal var image : ImageView
        internal var time:TextView


        init {
            msg = itemView.findViewById(R.id.tv_name)
            date = itemView.findViewById(R.id.tv_date)
            sender = itemView.findViewById(R.id.sender)
            image_lay = itemView.findViewById(R.id.image_lay)
            chat = itemView.findViewById(R.id.caht)
            image = itemView.findViewById(R.id.image)
            time = itemView.findViewById(R.id.time)


        }
    }
}
