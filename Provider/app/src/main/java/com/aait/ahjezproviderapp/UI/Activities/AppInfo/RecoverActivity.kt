package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.TermsResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class RecoverActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_recover

    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var amount:EditText
    lateinit var message:EditText
    lateinit var account_number:EditText
    lateinit var bank_name:EditText
    lateinit var account_name:EditText
    lateinit var iban:EditText
    lateinit var send:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        amount = findViewById(R.id.amount)
        account_number = findViewById(R.id.account_number)
        bank_name = findViewById(R.id.bank_name)
        iban = findViewById(R.id.iban)
        account_name = findViewById(R.id.account_name)
        message = findViewById(R.id.message)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Balance_recovery)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        send.setOnClickListener {
            if (CommonUtil.checkEditError(amount,getString(R.string.enter_amount_to_be))||
                CommonUtil.checkEditError(bank_name,getString(R.string.bank_name))||
                CommonUtil.checkEditError(account_name,getString(R.string.account_name))||
                CommonUtil.checkEditError(account_number,getString(R.string.account_number))||
                CommonUtil.checkEditError(iban,getString(R.string.iban_name))||
                    CommonUtil.checkEditError(message,getString(R.string.enter_message))){
                return@setOnClickListener
            }else{
                if (amount.text.toString().equals("0")){
                    amount.error = getString(R.string.enter_amount_to_be)
                }else{
                    upLoad()
                }

            }
        }

    }
    fun upLoad(){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.Recover(lang.appLanguage,"Bearer "+user.userData.token,message.text.toString(),bank_name.text.toString(),account_name.text.toString(),account_number.text.toString(),iban.text.toString(),amount.text.toString())?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        onBackPressed()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}