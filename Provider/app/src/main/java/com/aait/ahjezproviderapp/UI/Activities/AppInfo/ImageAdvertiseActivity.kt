package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.Models.ListResponse
import com.aait.ahjezproviderapp.Models.TermsResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.MainActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.ServiceDetailsActivity
import com.aait.ahjezproviderapp.UI.Controllers.ListAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.aait.ahjezproviderapp.Utils.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ImageAdvertiseActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_advertise_image
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var subcategory:TextView
    lateinit var subcategories:RecyclerView
    lateinit var image:TextView
    lateinit var confirm:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var listAdapter: ListAdapter
    lateinit var listModel: ListModel
    var cats = ArrayList<ListModel>()
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        subcategory = findViewById(R.id.subcategory)
        subcategories = findViewById(R.id.subcategories)
        image = findViewById(R.id.image)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Ad_image)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = ListAdapter(mContext,cats,R.layout.recycle_list)
        listAdapter.setOnItemClickListener(this)
        subcategories.layoutManager = linearLayoutManager
        subcategories.adapter = listAdapter
        subcategory.setOnClickListener { getCategories() }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        confirm.setOnClickListener { if (CommonUtil.checkTextError(subcategory,getString(R.string.Please_enter_the_section))||
                CommonUtil.checkTextError(image,getString(R.string.please_attach_image))){
            return@setOnClickListener
        }else{
            Add(ImageBasePath!!)
        }
        }

    }

    fun getCategories(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderSubCats(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object : Callback<ListResponse> {
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                subcategories.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){

            subcategories.visibility = View.GONE
            listModel = cats.get(position)
            subcategory.text = listModel.name

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]
//                for (i in 0..returnValue!!.size-1){
//                    paths.add(returnValue!![i])
//                }
//                if(paths.size==1){
//
//                    image.visibility = View.GONE
//                }else{
//
//                    image.visibility = View.VISIBLE
//                   // imagesAdapter.updateAll(paths)
//                }
                Log.e("pathsss", Gson().toJson(returnValue))



                if (ImageBasePath != null) {
                    // upLoad(ImageBasePath!!)
                    image.text = getString(R.string.attach_image)
                }
            }
        }
    }

    fun Add(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var imgs = ArrayList<MultipartBody.Part>()
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.AddImage(lang.appLanguage,"Bearer"+user.userData.token,id,listModel.id!!,filePart)
                ?.enqueue(object :Callback<TermsResponse>{
                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                val dialog = Dialog(mContext)
                                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                dialog ?.setCancelable(true)
                                dialog ?.setContentView(R.layout.dialog_image_added)
                                dialog?.show()
                                Handler().postDelayed({
                                    // logo.startAnimation(logoAnimation2)
                                    Handler().postDelayed({
                                        val  intent = Intent(this@ImageAdvertiseActivity, MainActivity::class.java)
                                        startActivity(intent)
                                        finish()
                                    }, 2100)
                                }, 1800)


                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}