package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class BalanceResponse:BaseResponse(),Serializable {
    var data:Double?=null
}