package com.aait.ahjezproviderapp.UI.Activities.Main

import android.view.View
import android.widget.ImageView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.ImagesModel
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Controllers.SliderAdpaters
import com.github.islamkhsh.CardSliderViewPager

class
ImagesActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_show_image
    lateinit var viewpager: CardSliderViewPager
    var list = ArrayList<ImagesModel>()
    lateinit var back:ImageView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        viewpager = findViewById(R.id.viewPager)
        list = intent.getStringArrayListExtra("link") as ArrayList<ImagesModel>
        initSliderAds(list)

    }
    fun initSliderAds(list:ArrayList<ImagesModel>){
        if(list.isEmpty()){
            viewpager.visibility= View.GONE


        }
        else{
            viewpager.visibility= View.VISIBLE


            viewpager.adapter= SliderAdpaters(mContext!!,list)

        }
    }
}