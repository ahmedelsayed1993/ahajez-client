package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.PayOnlineActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.packageOnlinePayActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PackagePayActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_reservation_pay
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var confirm: Button
    lateinit var visa: LinearLayout
    lateinit var vi: ImageView
    lateinit var master: LinearLayout
    lateinit var mas: ImageView
    lateinit var mada: LinearLayout
    lateinit var mad: ImageView
    lateinit var stc: LinearLayout
    lateinit var st: ImageView
    lateinit var paypal: LinearLayout
    lateinit var payp: ImageView
    lateinit var receipt: LinearLayout
    lateinit var recip: ImageView
    lateinit var express:LinearLayout
    lateinit var expressp:ImageView
    var id = 0
    var pay = "online"
    var user_id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        user_id = intent.getIntExtra("user",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        express = findViewById(R.id.express)
        expressp = findViewById(R.id.expressp)
        notification  = findViewById(R.id.notification)
        confirm = findViewById(R.id.confirm)
        visa = findViewById(R.id.visa)
        vi = findViewById(R.id.vi)
        master = findViewById(R.id.master)
        mas = findViewById(R.id.mas)
        mada = findViewById(R.id.mada)
        mad = findViewById(R.id.mad)
        stc = findViewById(R.id.stc)
        st = findViewById(R.id.st)
        paypal = findViewById(R.id.paypal)
        payp = findViewById(R.id.payp)
        receipt = findViewById(R.id.receipt)
        recip = findViewById(R.id.recip)
        back.setOnClickListener { onBackPressed()
            finish()}
        notification.setOnClickListener { startActivity(Intent(this, NotificationActivity::class.java))
            finish()}
        title.text = getString(R.string.payment_method)
        paypal.visibility = View.GONE
        vi.setOnClickListener {
            pay = "online"
            vi.background = mContext.resources.getDrawable(R.mipmap.check)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this, packageOnlinePayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","visa")
            startActivity(intent)
        }
        mas.setOnClickListener {
            pay = "online"
            mas.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this, packageOnlinePayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","visa")
            startActivity(intent)
        }
        mad.setOnClickListener {
            pay = "online"
            mad.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this, packageOnlinePayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","mada")
            startActivity(intent)
        }
        st.setOnClickListener {
            pay = "online"
            st.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this, packageOnlinePayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","stc_pay")
            startActivity(intent)
        }
        expressp.setOnClickListener { pay = "online"
            expressp.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)

            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","amex")
            startActivity(intent)
        }
        express.setOnClickListener { pay = "online"
            expressp.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)

            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this,PayOnlineActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","amex")
            startActivity(intent)
        }
        payp.setOnClickListener {
            pay = ""
//            payp.background = mContext.resources.getDrawable(R.mipmap.check)
//            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
//            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
//            st.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        recip.setOnClickListener {
            pay = "cash"
//            recip.background = mContext.resources.getDrawable(R.mipmap.check)
//            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
//            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
//            st.background = mContext.resources.getDrawable(R.drawable.white_black)
//            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        visa.setOnClickListener { pay = "online"
            vi.background = mContext.resources.getDrawable(R.mipmap.check)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this, packageOnlinePayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","visa")
            startActivity(intent)
        }
        master.setOnClickListener { pay = "online"
            mas.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this, packageOnlinePayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","visa")
            startActivity(intent)
        }
        mada.setOnClickListener { pay = "online"
            mad.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            st.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this, packageOnlinePayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","mada")
            startActivity(intent)
        }
        receipt.setOnClickListener { pay = "cash"
//            recip.background = mContext.resources.getDrawable(R.mipmap.check)
//            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
//            st.background = mContext.resources.getDrawable(R.drawable.white_black)
//            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        stc.setOnClickListener { pay = "online"
            st.background = mContext.resources.getDrawable(R.mipmap.check)
            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
            payp.background = mContext.resources.getDrawable(R.drawable.white_black)
            expressp.background = mContext.resources.getDrawable(R.drawable.white_black)
            val intent = Intent(this, packageOnlinePayActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("user",user_id)
            intent.putExtra("type","STC_PAY")
            startActivity(intent)
        }
        paypal.setOnClickListener { pay = "online"
//            payp.background = mContext.resources.getDrawable(R.mipmap.check)
//            vi.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mad.background = mContext.resources.getDrawable(R.drawable.white_black)
//            mas.background = mContext.resources.getDrawable(R.drawable.white_black)
//            st.background = mContext.resources.getDrawable(R.drawable.white_black)
//            recip.background = mContext.resources.getDrawable(R.drawable.white_black)
        }
        confirm.setOnClickListener {  }


    }


}