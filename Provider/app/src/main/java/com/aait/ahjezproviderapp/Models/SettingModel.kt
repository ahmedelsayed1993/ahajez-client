package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class SettingModel:Serializable {
    var closed:Int?=null
    var maintenance:Int?=null
}