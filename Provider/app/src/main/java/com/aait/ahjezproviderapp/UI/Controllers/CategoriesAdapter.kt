package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentRecyclerAdapter
import com.aait.ahjezproviderapp.Base.ParentRecyclerViewHolder
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.Models.SubCategoryModel
import com.aait.ahjezproviderapp.R

class CategoriesAdapter (context: Context, data: MutableList<SubCategoryModel>, layoutId: Int) :
        ParentRecyclerAdapter<SubCategoryModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
       if (listModel.checked==0){
           viewHolder.cat.isChecked = false
       }else{
           viewHolder.cat.isChecked = true
       }
        viewHolder.cat!!.setText(listModel.name)
        viewHolder.cat.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var cat=itemView.findViewById<CheckBox>(R.id.cat)


    }
}