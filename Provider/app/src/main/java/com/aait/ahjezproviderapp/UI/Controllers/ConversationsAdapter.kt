package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentRecyclerAdapter
import com.aait.ahjezproviderapp.Base.ParentRecyclerViewHolder
import com.aait.ahjezproviderapp.Models.ChatsModel
import com.aait.ahjezproviderapp.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class ConversationsAdapter (context: Context, data: MutableList<ChatsModel>, layoutId: Int) :
    ParentRecyclerAdapter<ChatsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val chatsModel = data.get(position)
        viewHolder.name!!.setText(chatsModel.username)
        if (chatsModel.messageType.equals("file")){
            viewHolder.message!!.text = mcontext.getString(R.string.attach_image)
        }else {
            viewHolder.message!!.text = chatsModel.message!!
        }
        //  Glide.with(mcontext).load(listModel.image!!).into(viewHolder.photo)
        Glide.with(mcontext).load(chatsModel.avatar).into(viewHolder.image)

            viewHolder.time.text = chatsModel.date

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var message = itemView.findViewById<TextView>(R.id.message)
        internal var time = itemView.findViewById<TextView>(R.id.time)



    }
}