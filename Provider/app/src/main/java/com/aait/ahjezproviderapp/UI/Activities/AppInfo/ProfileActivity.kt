package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.Models.ListResponse
import com.aait.ahjezproviderapp.Models.UserResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Auth.ChangePasswordActivity
import com.aait.ahjezproviderapp.UI.Activities.LocationActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.UI.Controllers.ListAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.aait.ahjezproviderapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality

import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_profile
    lateinit var image: CircleImageView
    lateinit var number: TextView

    lateinit var phone: EditText
    lateinit var email: EditText
    lateinit var save: Button
    lateinit var chang_pass: Button
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var name_ar:EditText
    lateinit var name_en:EditText
    lateinit var description_ar:EditText
    lateinit var description_en:EditText
    lateinit var region:TextView
    lateinit var city:TextView
    lateinit var neighbor:TextView
    lateinit var street:EditText
    lateinit var location:TextView
    lateinit var bank_name:EditText
    lateinit var benefit_name:EditText
    lateinit var iban:EditText
    lateinit var regions: RecyclerView
    lateinit var cities: RecyclerView
    lateinit var neighbors: RecyclerView
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var listAdapter2: ListAdapter
    var  Regions = ArrayList<ListModel>()
    lateinit var linearLayoutManager3: LinearLayoutManager
    lateinit var listAdapter3: ListAdapter
    var  Cities = ArrayList<ListModel>()
    lateinit var linearLayoutManager4: LinearLayoutManager
    lateinit var listAdapter4: ListAdapter
    var  Neighboors = ArrayList<ListModel>()
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    var selected = 0
    lateinit var list1:ListModel
    lateinit var list2:ListModel
    lateinit var list3:ListModel
    lateinit var list4:ListModel
    lateinit var list5:ListModel
    //lateinit var list6:ListModel
    var neg:Int?=null
    var lat = ""
    var lng = ""
    var result = ""
    var ID = ""
    override fun initializeComponents() {
        ID =  Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
        image = findViewById(R.id.image)
        number = findViewById(R.id.number)

        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        save = findViewById(R.id.save)
        chang_pass = findViewById(R.id.change_pass)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        name_ar = findViewById(R.id.name_ar)
        name_en = findViewById(R.id.name_en)
        description_ar = findViewById(R.id.description_ar)
        description_en = findViewById(R.id.description_en)

        region = findViewById(R.id.region)
        city = findViewById(R.id.city)
        neighbor = findViewById(R.id.neighbor)
        street = findViewById(R.id.street)
        location = findViewById(R.id.location)
        regions = findViewById(R.id.regions)
        cities = findViewById(R.id.cities)
        neighbors = findViewById(R.id.neighbors)
        bank_name = findViewById(R.id.bank_name)
        benefit_name = findViewById(R.id.benefit_name)
        iban = findViewById(R.id.iban)
        linearLayoutManager2 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter2 = ListAdapter(mContext,Regions,R.layout.recycle_list)
        listAdapter2.setOnItemClickListener(this)
        regions.layoutManager = linearLayoutManager2
        regions.adapter = listAdapter2
        linearLayoutManager3 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter3 = ListAdapter(mContext,Cities,R.layout.recycle_list)
        listAdapter3.setOnItemClickListener(this)
        cities.layoutManager = linearLayoutManager3
        cities.adapter = listAdapter3
        linearLayoutManager4 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter4 = ListAdapter(mContext,Neighboors,R.layout.recycle_list)
        listAdapter4.setOnItemClickListener(this)
        neighbors.layoutManager = linearLayoutManager4
        neighbors.adapter = listAdapter4
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.profile)
        notification.setOnClickListener { startActivity(Intent(this, NotificationActivity::class.java))
            finish()}
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        chang_pass.setOnClickListener { startActivity(Intent(this, ChangePasswordActivity::class.java)) }
        location.setOnClickListener { startActivityForResult(Intent(this, LocationActivity::class.java),1)  }
        region.setOnClickListener {
            selected = 2
            getRegions()
        }
        city.setOnClickListener {
            if (region.text.toString().equals("")){
                region.error = getString(R.string.Please_select_a_region)
            }else {
                selected = 3
                getCities()
            }
        }
        neighbor.setOnClickListener {
            if (city.text.toString().equals("")){
                city.error = getString(R.string.Please_select_a_city)
            }else {
                selected = 4
                getNeighboors()
            }
        }
        getData()
        save.setOnClickListener {
            if (
                    CommonUtil.checkEditError(name_ar,getString(R.string.Please_enter_the_trade_name_in_Arabic))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(description_ar,getString(R.string.Please_enter_the_description_in_Arabic))||
                    CommonUtil.checkTextError(region,getString(R.string.Please_select_a_region))||
                    CommonUtil.checkTextError(city,getString(R.string.Please_select_a_city))||
                    CommonUtil.checkTextError(location,getString(R.string.Please_select_a_location_on_the_map))||
                    CommonUtil.checkEditError(bank_name,getString(R.string.Please_enter_the_name_of_the_bank))||
                    CommonUtil.checkEditError(benefit_name,getString(R.string.Please_enter_the_name_of_the_beneficiary))||
                    CommonUtil.checkEditError(iban,getString(R.string.Please_enter_the_IBAN_number))
                    ){
                return@setOnClickListener
            }else{
                if (!email.text.toString().equals("")){
                    if (!CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
                        return@setOnClickListener
                    }else {
                        updateData(email.text.toString())
                    }
                }else{
                    updateData(null)
                }
            }
        }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProfile("Bearer "+user.userData.token!!,lang.appLanguage,null,null,null,null,null,null,null,null,null,null,null,null
                ,null,null,null,null,ID)
                ?.enqueue(object: Callback<UserResponse> {
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                            call: Call<UserResponse>,
                            response: Response<UserResponse>
                    ) {
                        hideProgressDialog()
                        if(response.isSuccessful){
                            if(response.body()?.value.equals("1")){
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                list3 = ListModel(response.body()?.data?.region_id,response.body()?.data?.region)
                                list4 = ListModel(response.body()?.data?.city_id,response.body()?.data?.city)
                                list5 = ListModel(response.body()?.data?.neighborhood_id,response.body()?.data?.neighborhood)
                                city.text = response.body()?.data?.city
                                region.text = response.body()?.data?.region
                                neighbor.text = response.body()?.data?.neighborhood
                                name_ar.setText(response.body()?.data?.name_ar)
                                name_en.setText(response.body()?.data?.name_en)
                                description_ar.setText(response.body()?.data?.desc_ar)
                                description_en.setText(response.body()?.data?.desc_en)
                                street.setText(response.body()?.data?.street)
                                location.text = response.body()?.data?.address
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!
                                bank_name.setText(response.body()?.data?.bank_name)
                                benefit_name.setText(response.body()?.data?.account_name)
                                iban.setText(response.body()?.data?.iban_number)
                                number.text = response.body()?.data?.number.toString()
                                phone.setText(response.body()?.data?.phone)
                                email.setText(response.body()?.data?.email)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }
                })
    }

    fun updateData(mail:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProfile("Bearer "+user.userData.token!!,lang.appLanguage,phone.text.toString(),mail
        ,name_ar.text.toString(),name_en.text.toString(),description_ar.text.toString(),description_en.text.toString(),list3.id!!,list4.id!!,list5.id!!,street.text.toString()
                ,lat
        ,lng,location.text.toString(),bank_name.text.toString(),benefit_name.text.toString(),iban.text.toString(),ID)
                ?.enqueue(object: Callback<UserResponse> {
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                            call: Call<UserResponse>,
                            response: Response<UserResponse>
                    ) {
                        hideProgressDialog()
                        if(response.isSuccessful){
                            if(response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,getString(R.string.data_updated))
                                Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                list3 = ListModel(response.body()?.data?.region_id,response.body()?.data?.region)
                                list4 = ListModel(response.body()?.data?.city_id,response.body()?.data?.city)
                                list5 = ListModel(response.body()?.data?.neighborhood_id,response.body()?.data?.neighborhood)
                                city.text = response.body()?.data?.city
                                region.text = response.body()?.data?.region
                                neighbor.text = response.body()?.data?.neighborhood
                                name_ar.setText(response.body()?.data?.name_ar)
                                name_en.setText(response.body()?.data?.name_en)
                                description_ar.setText(response.body()?.data?.desc_ar)
                                description_en.setText(response.body()?.data?.desc_en)
                                street.setText(response.body()?.data?.street)
                                location.text = response.body()?.data?.address
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!
                                bank_name.setText(response.body()?.data?.bank_name)
                                benefit_name.setText(response.body()?.data?.account_name)
                                iban.setText(response.body()?.data?.iban_number)
                                number.text = response.body()?.data?.number.toString()
                                phone.setText(response.body()?.data?.phone)
                                email.setText(response.body()?.data?.email)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }
                })
    }
    fun upLoad(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.AddImage("Bearer "+user.userData.token,lang.appLanguage,filePart,ID)?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,getString(R.string.data_updated))
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        list3 = ListModel(response.body()?.data?.region_id,response.body()?.data?.region)
                        list4 = ListModel(response.body()?.data?.city_id,response.body()?.data?.city)
                        list5 = ListModel(response.body()?.data?.neighborhood_id,response.body()?.data?.neighborhood)
                        city.text = response.body()?.data?.city
                        region.text = response.body()?.data?.region
                        neighbor.text = response.body()?.data?.neighborhood
                        name_ar.setText(response.body()?.data?.name_ar)
                        name_en.setText(response.body()?.data?.name_en)
                        description_ar.setText(response.body()?.data?.desc_ar)
                        description_en.setText(response.body()?.data?.desc_en)
                        street.setText(response.body()?.data?.street)
                        location.text = response.body()?.data?.address
                        lat = response.body()?.data?.lat!!
                        lng = response.body()?.data?.lng!!
                        bank_name.setText(response.body()?.data?.bank_name)
                        benefit_name.setText(response.body()?.data?.account_name)
                        iban.setText(response.body()?.data?.iban_number)
                        number.text = response.body()?.data?.number.toString()
                        phone.setText(response.body()?.data?.phone)
                        email.setText(response.body()?.data?.email)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                location.text = ""
            }
        }
        else if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }

    fun getRegions(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Regions(lang.appLanguage)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                regions.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter2.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cities(lang.appLanguage,list3.id!!)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                cities.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter3.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getNeighboors(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Neighborhoods(lang.appLanguage,list4.id!!)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                neighbors.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter4.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name) {
            if (selected == 2) {
                regions.visibility = View.GONE
                list3 = Regions.get(position)
                region.text = list3.name
            } else if (selected == 3) {
                cities.visibility = View.GONE
                list4 = Cities.get(position)
                city.text = list4.name
            } else if (selected == 4) {
                neighbors.visibility = View.GONE
                list5 = Neighboors.get(position)
                neighbor.text = list5.name
                neg = list5.id
            }
        }




    }
}