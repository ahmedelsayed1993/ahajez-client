package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class SubCategoryResponse:BaseResponse(),Serializable {
    var data:ArrayList<SubCategoryModel>?=null
}