package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.PackagesModel
import com.aait.ahjezproviderapp.Models.PackagesResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.UI.Controllers.PackagesAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdvertisesActivity : ParentActivity(), OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_advertising
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var packagesModels = java.util.ArrayList<PackagesModel>()
    internal lateinit var packagesAdapter: PackagesAdapter
    var user_id = 0
    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        notification = findViewById(R.id.notification)
        title.text = getString(R.string.Announce_and_distinguish)
        back.setOnClickListener { onBackPressed()
            finish()}
        notification.setOnClickListener { startActivity(Intent(this, NotificationActivity::class.java))
            finish()}

        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        packagesAdapter =  PackagesAdapter(mContext,packagesModels, R.layout.recycle_advertise)
        packagesAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = packagesAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()
    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Advertises(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object :
                Callback<PackagesResponse> {
            override fun onResponse(call: Call<PackagesResponse>, response: Response<PackagesResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {

                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            user_id = response.body()?.user!!
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                            } else {
//                            initSliderAds(response.body()?.slider!!)

                                packagesAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<PackagesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,AdvertisePayActivity::class.java)
        intent.putExtra("id",packagesModels.get(position).id)
        intent.putExtra("user",user_id)
        startActivity(intent)
    }
}