package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.UI.Controllers.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class HidenItemsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_hiden_items

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Hidden_products_and_services)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        orders = findViewById(R.id.orders)
        ordersViewPager = findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeTapAdapter(mContext!!,supportFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
    }

}