package com.aait.ahjezproviderapp.UI.Activities.Main

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.*
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Controllers.ProdsAdapter
import com.aait.ahjezproviderapp.UI.Controllers.ReasonsAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderDetailsActivity:ParentActivity() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_orders_details
    lateinit var show: LinearLayout
    lateinit var down: ImageView
    lateinit var details: LinearLayout
    lateinit var back: ImageView
    lateinit var title: TextView

    lateinit var notification: ImageView

    lateinit var order_num: TextView
    lateinit var address_lay: LinearLayout
    lateinit var delivery_address: TextView
    lateinit var phone_lay: LinearLayout
    lateinit var phone: TextView
    lateinit var payment_method: TextView
    lateinit var order_date: TextView
    lateinit var notes: TextView
    lateinit var preferences: TextView
    lateinit var products: RecyclerView
    lateinit var products_value: TextView
    lateinit var delivery_lay: LinearLayout
    lateinit var delivery_price: TextView
    lateinit var tax: TextView
    lateinit var tax_value: TextView
    lateinit var total: TextView
    lateinit var image: CircleImageView
    lateinit var name: TextView
    lateinit var rating: RatingBar
    lateinit var address: TextView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var prodsAdapter: ProdsAdapter
    var Products = ArrayList<ProdModel>()
    lateinit var received: Button
    lateinit var chat: ImageView
    lateinit var comment_lay: LinearLayout
    lateinit var rate: RatingBar
    lateinit var comment: TextView
    var id = 0
    var photo = ""
    var provider = ""
    var reciver = 0
    var reasons = 0
    lateinit var reasonsAdapter: ReasonsAdapter
    lateinit var linearLayoutManager1: LinearLayoutManager
    var reasonsModels = ArrayList<CancelModel>()
    var reason = 0
    lateinit var text: TextView
    lateinit var resons: TextView
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var lay_new:LinearLayout
    lateinit var accept:Button
    lateinit var refuse:Button
    lateinit var lay_current:LinearLayout
    lateinit var complete:Button
    lateinit var cancel:Button
    lateinit var prepered:Button
    lateinit var onway:Button
    lateinit var ratee:TextView
    private var lat = ""
    private var lng = ""

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        comment_lay = findViewById(R.id.comment_lay)
        comment = findViewById(R.id.comment)
        rate = findViewById(R.id.rate)

        notification = findViewById(R.id.notification)
        swipeRefresh = findViewById(R.id.swipe_refresh)

        text = findViewById(R.id.text)
        resons = findViewById(R.id.reason)

        order_num = findViewById(R.id.order_num)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        address = findViewById(R.id.address)
        order_date = findViewById(R.id.order_date)
        payment_method = findViewById(R.id.payment_method)
        delivery_address = findViewById(R.id.delivery_address)
        onway = findViewById(R.id.onway)
        phone = findViewById(R.id.phone)
        notes = findViewById(R.id.notes)
        preferences = findViewById(R.id.preferences)
        delivery_lay = findViewById(R.id.delivery_lay)
        phone_lay = findViewById(R.id.phone_lay)
        products = findViewById(R.id.products)
        products_value = findViewById(R.id.products_value)
        delivery_price = findViewById(R.id.delivery_price)
        address_lay = findViewById(R.id.address_lay)
        chat = findViewById(R.id.chat)
        tax = findViewById(R.id.tax)
        tax_value = findViewById(R.id.tax_value)
        total = findViewById(R.id.total)
        show = findViewById(R.id.show)
        details = findViewById(R.id.details)
        details.visibility = View.VISIBLE
        lay_new = findViewById(R.id.lay_new)
        lay_current = findViewById(R.id.lay_current)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        cancel = findViewById(R.id.cancel)
        complete = findViewById(R.id.complete)
        prepered = findViewById(R.id.prepeared)
        ratee = findViewById(R.id.ratee)

        title.text = getString(R.string.order_details)

        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        back.setOnClickListener { onBackPressed()
            finish()}
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        prodsAdapter = ProdsAdapter(mContext,Products,R.layout.recycle_prod)
        products.layoutManager = linearLayoutManager1
        products.adapter = prodsAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
        ratee.setOnClickListener { val intent = Intent(this,CommentsActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("type","products")
            startActivity(intent)}
        accept.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.OrderDetails("Bearer"+user.userData.token,lang.appLanguage,"products",id,"accepted",null)
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                        }

                        @SuppressLint("ResourceAsColor")
                        override fun onResponse(
                                call: Call<OrderDetailsResponse>,
                                response: Response<OrderDetailsResponse>
                        ) {
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    getData()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        prepered.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.OrderDetails("Bearer"+user.userData.token,lang.appLanguage,"products",id,"prepared",null)
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                        }

                        @SuppressLint("ResourceAsColor")
                        override fun onResponse(
                                call: Call<OrderDetailsResponse>,
                                response: Response<OrderDetailsResponse>
                        ) {
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    getData()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        onway.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.OrderDetails("Bearer"+user.userData.token,lang.appLanguage,"products",id,"on_way",null)
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                        }

                        @SuppressLint("ResourceAsColor")
                        override fun onResponse(
                                call: Call<OrderDetailsResponse>,
                                response: Response<OrderDetailsResponse>
                        ) {
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    getData()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        complete.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.OrderDetails("Bearer"+user.userData.token,lang.appLanguage,"products",id,"completed",null)
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                        }

                        @SuppressLint("ResourceAsColor")
                        override fun onResponse(
                                call: Call<OrderDetailsResponse>,
                                response: Response<OrderDetailsResponse>
                        ) {
                            hideProgressDialog()
                            swipeRefresh!!.isRefreshing = false
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    val dialog = Dialog(mContext)
                                    dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                    dialog ?.setCancelable(true)
                                    dialog ?.setContentView(R.layout.dialog_rate)
                                    val pro_image = dialog?.findViewById<CircleImageView>(R.id.pro_image)
                                    val pro_name = dialog?.findViewById<TextView>(R.id.pro_name)
                                    val rate = dialog?.findViewById<RatingBar>(R.id.rate)
                                    val comment = dialog?.findViewById<EditText>(R.id.comment)
                                    val confirm = dialog?.findViewById<Button>(R.id.confirm)
                                    Glide.with(mContext).load(photo).into(pro_image)
                                    pro_name.text = provider
                                    confirm.setOnClickListener {
                                        showProgressDialog(getString(R.string.please_wait))
                                        Client.getClient()?.create(Service::class.java)?.AddRate("Bearer"+user.userData.token,lang.appLanguage,id,"user",rate.rating.toInt(),comment.text.toString(),"order")
                                                ?.enqueue(object :Callback<TermsResponse>{
                                                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                                                        CommonUtil.handleException(mContext,t)
                                                        t.printStackTrace()
                                                        hideProgressDialog()
                                                        dialog?.dismiss()
                                                    }
                                                    override fun onResponse(
                                                            call: Call<TermsResponse>,
                                                            response: Response<TermsResponse>
                                                    ) {
                                                        hideProgressDialog()
                                                        dialog?.dismiss()
                                                        if (response.isSuccessful){
                                                            if (response.body()?.value.equals("1")){
                                                                CommonUtil.makeToast(mContext,response.body()?.data!!)
                                                                val intent = Intent(this@OrderDetailsActivity,OrderDoneActivity::class.java)

                                                                startActivity(intent)
                                                                finish()
                                                            }else{
                                                                onBackPressed()
                                                                finish()
                                                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                                            }
                                                        }
                                                    }
                                                })
                                    }
                                    dialog?.show()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        refuse.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.cancel_dialog)
            val reasonss = dialog?.findViewById<RecyclerView>(R.id.reasons)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            val titl = dialog?.findViewById<TextView>(R.id.titl)
            titl.text = getString(R.string.reason_of_refusing)
            linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
            reasonsAdapter = ReasonsAdapter(mContext,reasonsModels,R.layout.recycle_reason)
            reasonsAdapter.setOnItemClickListener(this)
            reasonss.layoutManager = linearLayoutManager
            reasonss.adapter = reasonsAdapter
            getReasons()
            send.setOnClickListener { showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.OrderDetails("Bearer"+user.userData.token,lang.appLanguage,"products",id,"refuse",reasons)
                        ?.enqueue(object :Callback<OrderDetailsResponse>{
                            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                hideProgressDialog()
                                dialog?.dismiss()
                            }

                            override fun onResponse(
                                    call: Call<OrderDetailsResponse>,
                                    response: Response<OrderDetailsResponse>
                            ) {
                                hideProgressDialog()
                                dialog?.dismiss()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        val dialog = Dialog(mContext)
                                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                        dialog ?.setCancelable(true)
                                        dialog ?.setContentView(R.layout.dailog_done)
                                        val text = dialog?.findViewById<TextView>(R.id.text)
                                        val back = dialog?.findViewById<Button>(R.id.ok)

                                        text.text = getString(R.string.refuse_done)

                                        //getReasons()

                                        back.setOnClickListener { dialog.dismiss()
                                            val intent = Intent(this@OrderDetailsActivity,MainActivity::class.java)

                                            startActivity(intent)}
                                        dialog?.show()
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                        }) }
            cancel.setOnClickListener { dialog.dismiss() }
            dialog?.show()
        }
        address_lay.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?"+"&daddr="+lat+","+lng))
            )
        }
        cancel.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.cancel_dialog)
            val reasonss = dialog?.findViewById<RecyclerView>(R.id.reasons)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            val titl = dialog?.findViewById<TextView>(R.id.titl)
            titl.text = getString(R.string.reason_of_cancellation)
            linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
            reasonsAdapter = ReasonsAdapter(mContext,reasonsModels,R.layout.recycle_reason)
            reasonsAdapter.setOnItemClickListener(this)
            reasonss.layoutManager = linearLayoutManager
            reasonss.adapter = reasonsAdapter
            getReasons()
            send.setOnClickListener { showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.OrderDetails("Bearer"+user.userData.token,lang.appLanguage,"products",id,"cancel",reasons)
                        ?.enqueue(object :Callback<OrderDetailsResponse>{
                            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                hideProgressDialog()
                                dialog?.dismiss()
                            }

                            override fun onResponse(
                                    call: Call<OrderDetailsResponse>,
                                    response: Response<OrderDetailsResponse>
                            ) {
                                hideProgressDialog()
                                dialog?.dismiss()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        val dialog = Dialog(mContext)
                                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                        dialog ?.setCancelable(true)
                                        dialog ?.setContentView(R.layout.dailog_done)
                                        val text = dialog?.findViewById<TextView>(R.id.text)
                                        val back = dialog?.findViewById<Button>(R.id.ok)

                                        text.text = getString(R.string.cancel_done)

                                        //getReasons()

                                        back.setOnClickListener { dialog.dismiss()
                                            val intent = Intent(this@OrderDetailsActivity,MainActivity::class.java)

                                            startActivity(intent)}
                                        dialog?.show()
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    }
                                }
                            }

                        }) }
            cancel.setOnClickListener { dialog.dismiss() }
            dialog?.show()
        }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails("Bearer"+user.userData.token,lang.appLanguage,"products",id,null,null)
                ?.enqueue(object : Callback<OrderDetailsResponse> {
                    override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                        swipeRefresh!!.isRefreshing = false
                    }

                    override fun onResponse(
                            call: Call<OrderDetailsResponse>,
                            response: Response<OrderDetailsResponse>
                    ) {
                        hideProgressDialog()
                        swipeRefresh!!.isRefreshing = false
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                address.text = getString(R.string.order_num)+":"+response.body()?.data?.id.toString()
                                phone.text = response.body()?.data?.phone
                                notes.text = response.body()?.data?.notes
                                preferences.text = response.body()?.data?.preferences
                                prodsAdapter.updateAll(response.body()?.data?.products!!)
                                products_value.text = response.body()?.data?.total+getString(R.string.rs)
                                tax.text = getString(R.string.The_tax_amount)+response.body()?.data?.tax+"%"
                                tax_value.text = response.body()?.data?.total_tax+getString(R.string.rs)
                                order_date.text = response.body()?.data?.created
                                order_num.text = response.body()?.data?.id.toString()
                                ratee.text = "("+response.body()?.data?.user_rates_count.toString()+")"
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!
                                if (response.body()?.data?.payment!!.equals("cash")){
                                    payment_method.text = getString(R.string.cash)
                                }else if (response.body()?.data?.payment!!.equals("online")){
                                    payment_method.text = getString(R.string.online)
                                }else{
                                    payment_method.text = getString(R.string.balance)
                                }
                                resons.text = response.body()?.data?.cancellation
                                rating.rating = response.body()?.data?.rate!!
                                Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                                name.text = response.body()?.data?.name
                                provider = response.body()?.data?.name!!
                                photo = response.body()?.data?.avatar!!
                                reciver  = response.body()?.data?.chat_provider!!
                                delivery_address.text = response.body()?.data?.address
                                if (response.body()?.data?.order_rate==0){
                                    comment_lay.visibility = View.GONE
                                }else{
                                    comment_lay.visibility = View.VISIBLE
                                    comment.text = response.body()?.data?.order_comment
                                    rate.rating = response.body()?.data?.order_rate!!.toFloat()
                                }
                                if (response.body()?.data?.address.equals("")){
                                    address_lay.visibility = View.GONE
                                }else{
                                    address_lay.visibility = View.VISIBLE
                                }
                                if (response.body()?.data?.phone.equals("")){
                                    phone_lay.visibility = View.GONE
                                }else{
                                    phone_lay.visibility = View.VISIBLE
                                }
                                if (response.body()?.data?.delivery_price.equals("")){
                                    delivery_lay.visibility = View.GONE

                                }else{
                                    delivery_lay.visibility = View.VISIBLE
                                    delivery_price.text = response.body()?.data?.delivery_price+getString(R.string.rs)

                                }
                                total.text = response.body()?.data?.final_total+getString(R.string.rs)
                                if (response.body()?.data?.status.equals("current")||response.body()?.data?.status.equals("pending")){
                                   lay_new.visibility = View.VISIBLE
                                    lay_current.visibility = View.GONE
                                }else if(response.body()?.data?.status.equals("accepted")){
                                    lay_new.visibility = View.GONE
                                    lay_current.visibility = View.VISIBLE
                                    prepered.visibility =View.VISIBLE
                                    onway.visibility =View.GONE
                                    complete.visibility =View.GONE
                                }else if (response.body()?.data?.status.equals("prepared")){
                                    lay_new.visibility = View.GONE
                                    lay_current.visibility = View.VISIBLE
                                    prepered.visibility =View.GONE
                                    if (response.body()?.data?.delivery_price.equals("")){
                                        onway.visibility =View.GONE
                                        complete.visibility =View.VISIBLE
                                    }else {
                                        onway.visibility = View.VISIBLE
                                        complete.visibility = View.GONE
                                    }
                                }else if (response.body()?.data?.status.equals("on_way")) {
                                    lay_new.visibility = View.GONE
                                    lay_current.visibility = View.VISIBLE
                                    prepered.visibility =View.GONE
                                    onway.visibility =View.GONE
                                    complete.visibility =View.VISIBLE
                                }else if (response.body()?.data?.status.equals("completed")) {
                                    lay_current.visibility = View.GONE
                                    lay_new.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("refuse")||response.body()?.data?.status.equals("cancel")){
                                    lay_current.visibility = View.GONE
                                    lay_new.visibility = View.GONE
                                    comment_lay.visibility = View.GONE
                                    text.visibility = View.VISIBLE
                                    resons.visibility = View.VISIBLE
                                }else{
                                    lay_current.visibility = View.GONE
                                    lay_new.visibility = View.GONE
                                    comment_lay.visibility = View.GONE
                                }
                                //current
                                //accepted

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.reason){
            reasonsAdapter.selected = position
            reasonsModels.get(position).selected = true
            reasonsAdapter.notifyDataSetChanged()
            reasons = reasonsModels.get(position).id!!
            Log.e("reason",reasons.toString())

        }

    }
    fun getReasons(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cancellation("Bearer"+user.userData.token,lang.appLanguage)?.enqueue(object :Callback<CancelResponse>{
            override fun onFailure(call: Call<CancelResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                    call: Call<CancelResponse>,
                    response: Response<CancelResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        reasonsAdapter.updateAll(response.body()?.data!!)
                        if (response.body()?.data!!.size!=0){
                            reasons = response.body()?.data?.get(0)?.id!!
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}