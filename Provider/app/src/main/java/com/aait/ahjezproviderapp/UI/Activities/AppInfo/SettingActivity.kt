package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.SettingResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Auth.WorkTimesActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SettingActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_settings
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var service_provided:LinearLayout
    lateinit var hidden_products:LinearLayout
    lateinit var subsections:LinearLayout
    lateinit var work_times:LinearLayout
    lateinit var delivery:LinearLayout
    lateinit var close:Switch
    lateinit var maintances:Switch

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        service_provided = findViewById(R.id.service_provided)
        hidden_products = findViewById(R.id.hidden_products)
        subsections = findViewById(R.id.subsections)
        work_times = findViewById(R.id.work_times)
        delivery = findViewById(R.id.delivery)
        close = findViewById(R.id.close)
        maintances = findViewById(R.id.maintances)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.settings)
        service_provided.setOnClickListener {
            startActivity(Intent(this,SubCategoriesActivity::class.java))
        }
        notification.setOnClickListener { 
            startActivity(Intent(this,NotificationActivity::class.java))
            finish()
        }
        delivery.setOnClickListener { startActivity(Intent(this,DeliveryActivity::class.java)) }
        subsections.setOnClickListener { 
            startActivity(Intent(this,SubSectionActivity::class.java))
        }
        work_times.setOnClickListener { startActivity(Intent(this,WorkTimesActivity::class.java)) }
        hidden_products.setOnClickListener { startActivity(Intent(this,HidenItemsActivity::class.java))
        }
        getSetting(null,null)
        close.setOnClickListener {
            if (close.isChecked){
                getSetting(1,null)
            }else{
                getSetting(0,null)
            }
        }
        maintances.setOnClickListener {
            if (maintances.isChecked){
                getSetting(null,1)
            }else{
                getSetting(null,0)
            }
        }

    }
    fun getSetting(clos:Int?,maintain:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Setting(lang.appLanguage,"Bearer"+user.userData.token,clos,maintain)
                ?.enqueue(object : Callback<SettingResponse>{
                    override fun onFailure(call: Call<SettingResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<SettingResponse>, response: Response<SettingResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                if (response.body()?.data?.closed==0){
                                    close.isChecked = false
                                }else{
                                    close.isChecked = true
                                }
                                if (response.body()?.data?.maintenance==0){
                                    maintances.isChecked = false
                                }else{
                                    maintances.isChecked = true
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}