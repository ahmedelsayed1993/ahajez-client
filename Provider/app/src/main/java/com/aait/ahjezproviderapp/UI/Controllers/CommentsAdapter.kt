package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentRecyclerAdapter
import com.aait.ahjezproviderapp.Base.ParentRecyclerViewHolder
import com.aait.ahjezproviderapp.Models.CommentsModel
import com.aait.ahjezproviderapp.Models.ReservationModel
import com.aait.ahjezproviderapp.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class CommentsAdapter  (context: Context, data: MutableList<CommentsModel>, layoutId: Int) :
    ParentRecyclerAdapter<CommentsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)


        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_fall_down)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)
        Glide.with(mcontext).load(questionModel.avatar).into(viewHolder.image)
        viewHolder.name.text = questionModel.username
        viewHolder.comment.text = questionModel.comment
        viewHolder.time.text = questionModel.created
        viewHolder.rate.rating = questionModel.rate!!.toFloat()






    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<CircleImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var rate = itemView.findViewById<RatingBar>(R.id.rating)
        internal var time = itemView.findViewById<TextView>(R.id.time)
        internal var comment = itemView.findViewById<TextView>(R.id.comment)





    }
}