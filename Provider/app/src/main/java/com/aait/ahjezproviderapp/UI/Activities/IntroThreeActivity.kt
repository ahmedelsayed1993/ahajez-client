package com.aait.ahjezproviderapp.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R


class IntroThreeActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_intro_three
    lateinit var image: ImageView
    lateinit var title: TextView
    lateinit var content: TextView
    lateinit var next: Button


    override fun initializeComponents() {
        image = findViewById(R.id.image)
        title =findViewById(R.id.title)
        content = findViewById(R.id.content)
        next = findViewById(R.id.next)
        image.setImageResource(R.mipmap.seventeen)
        title.text = getString(R.string.Order_and_we_will_prepare_your_order_before_you_arrive)
        next.setOnClickListener { val intent = Intent(this@IntroThreeActivity, PreLoginActivity::class.java)
            
            startActivity(intent)
            finish() }


    }
}