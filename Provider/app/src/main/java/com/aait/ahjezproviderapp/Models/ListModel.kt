package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class ListModel:Serializable {
    var id: Int? = null
    var name: String? = null
    var image: String? = null
    var checked:Boolean?=null

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }
}
  //  var selected:Boolean?=null