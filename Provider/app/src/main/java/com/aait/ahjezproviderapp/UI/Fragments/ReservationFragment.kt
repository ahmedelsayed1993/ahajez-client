package com.aait.ahjezproviderapp.UI.Fragments

import android.graphics.Color
import android.os.Build
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentTransaction
import com.aait.ahjezproviderapp.Base.BaseFragment
import com.aait.ahjezproviderapp.R

class ReservationFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_reservation
    lateinit var new_order: LinearLayout
    lateinit var new_text: TextView
    lateinit var line: TextView
    lateinit var current_order: LinearLayout
    lateinit var line1: TextView
    lateinit var current_text: TextView
    lateinit var finished_order: LinearLayout
    lateinit var line2: TextView
    lateinit var finished_text: TextView
    lateinit var cancelled_order: LinearLayout
    lateinit var line3: TextView
    lateinit var cancelled_text: TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var state = ""

    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var newOrderFragment: NewReservationFragment
    internal lateinit var currentOrderFragment: CurrentReservationFragment
    internal lateinit var finishedOrderFragment: FinishedReservationFragment
    internal lateinit var cancelledOrderFragment: CancelledReservationFragment
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initializeComponents(view: View) {
        new_order = view.findViewById(R.id.new_order)
        new_text = view.findViewById(R.id.new_text)
        line = view.findViewById(R.id.line)
        current_order = view.findViewById(R.id.current_order)
        current_text = view.findViewById(R.id.current_text)
        line1 = view.findViewById(R.id.line1)
        finished_order = view.findViewById(R.id.finished_order)
        finished_text = view.findViewById(R.id.finished_text)
        line2 = view.findViewById(R.id.line2)
        cancelled_order = view.findViewById(R.id.cancelled_order)
        cancelled_text = view.findViewById(R.id.cancelled_text)
        line3 = view.findViewById(R.id.line3)
        newOrderFragment = NewReservationFragment.newInstance()
        currentOrderFragment = CurrentReservationFragment.newInstance()
        finishedOrderFragment = FinishedReservationFragment.newInstance()
        cancelledOrderFragment = CancelledReservationFragment.newInstance()
        transaction = childFragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container,newOrderFragment)
        transaction!!.add(R.id.home_fragment_container,currentOrderFragment)
        transaction!!.add(R.id.home_fragment_container,finishedOrderFragment)
        transaction!!.add(R.id.home_fragment_container,cancelledOrderFragment)
        transaction!!.commit()
        showhome()
        new_order.setOnClickListener { showhome() }
        current_order.setOnClickListener { showcurrent() }
        finished_order.setOnClickListener { showfinished() }
        cancelled_order.setOnClickListener { showcancel() }
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showhome(){
        selected = 1

        new_text.textColor = Color.parseColor("#2B726F")
        current_text.textColor = Color.parseColor("#CFCFCF")
        finished_text.textColor = Color.parseColor("#CFCFCF")
        cancelled_text.textColor = Color.parseColor("#CFCFCF")
        line.background = mContext!!.getDrawable(R.color.colorGreen)
        line1.background = mContext!!.getDrawable(R.color.colorWhite)
        line2.background = mContext!!.getDrawable(R.color.colorWhite)
        line3.background = mContext!!.getDrawable(R.color.colorWhite)

        transaction = childFragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, newOrderFragment)
        transaction!!.commit()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showcurrent(){
        selected = 1

        current_text.textColor = Color.parseColor("#2B726F")
        new_text.textColor = Color.parseColor("#CFCFCF")
        finished_text.textColor = Color.parseColor("#CFCFCF")
        cancelled_text.textColor = Color.parseColor("#CFCFCF")
        line1.background = mContext!!.getDrawable(R.color.colorGreen)
        line.background = mContext!!.getDrawable(R.color.colorWhite)
        line2.background = mContext!!.getDrawable(R.color.colorWhite)
        line3.background = mContext!!.getDrawable(R.color.colorWhite)

        transaction = childFragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, currentOrderFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showfinished(){
        selected = 1

        finished_text.textColor = Color.parseColor("#2B726F")
        current_text.textColor = Color.parseColor("#CFCFCF")
        new_text.textColor = Color.parseColor("#CFCFCF")
        cancelled_text.textColor = Color.parseColor("#CFCFCF")
        line2.background = mContext!!.getDrawable(R.color.colorGreen)
        line1.background = mContext!!.getDrawable(R.color.colorWhite)
        line.background = mContext!!.getDrawable(R.color.colorWhite)
        line3.background = mContext!!.getDrawable(R.color.colorWhite)

        transaction = childFragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, finishedOrderFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showcancel(){
        selected = 1

        cancelled_text.textColor = Color.parseColor("#2B726F")
        current_text.textColor = Color.parseColor("#CFCFCF")
        finished_text.textColor = Color.parseColor("#CFCFCF")
        new_text.textColor = Color.parseColor("#CFCFCF")
        line3.background = mContext!!.getDrawable(R.color.colorGreen)
        line1.background = mContext!!.getDrawable(R.color.colorWhite)
        line2.background = mContext!!.getDrawable(R.color.colorWhite)
        line.background = mContext!!.getDrawable(R.color.colorWhite)

        transaction = childFragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, cancelledOrderFragment)
        transaction!!.commit()
    }
}