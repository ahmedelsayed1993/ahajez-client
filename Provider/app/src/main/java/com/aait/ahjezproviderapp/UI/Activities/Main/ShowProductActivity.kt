package com.aait.ahjezproviderapp.UI.Activities.Main

import android.app.Dialog
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.ImagesModel
import com.aait.ahjezproviderapp.Models.ProductDetaildsResponse
import com.aait.ahjezproviderapp.Models.TermsResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Controllers.SliderAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderViewPager
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class ShowProductActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_show_product

    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var image: ImageView
    lateinit var viewPager: CardSliderViewPager
    lateinit var repeat: ImageView
    lateinit var delete: ImageView
    lateinit var hide: ImageView
    lateinit var indicator: CircleIndicator
    lateinit var name: TextView
    lateinit var views: TextView
    lateinit var pure: TextView
    lateinit var pric: TextView
    lateinit var offer: TextView
    lateinit var description: TextView
    lateinit var quantity: TextView
    lateinit var edit: Button
    lateinit var back_main: Button
    var id = 0
    override fun initializeComponents() {
        id  = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        image = findViewById(R.id.image)
        viewPager = findViewById(R.id.viewPager)
        repeat = findViewById(R.id.repeat)
        delete = findViewById(R.id.delete)
        hide = findViewById(R.id.hide)
        indicator = findViewById(R.id.indicator)
        name = findViewById(R.id.name)
        views = findViewById(R.id.views)
        pure = findViewById(R.id.pure)
        pric = findViewById(R.id.price)
        offer = findViewById(R.id.offer)
        description = findViewById(R.id.description)
        quantity = findViewById(R.id.quantity)
        edit = findViewById(R.id.edit)
        back_main = findViewById(R.id.back_main)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        edit.setOnClickListener { val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dailog_show)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener { dialog?.dismiss() }
            send.setOnClickListener {

                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Hide(lang.appLanguage,"Bearer"+user.userData.token
                        ,id,0,"product")?.enqueue(object : Callback<TermsResponse> {
                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                        dialog?.dismiss()
                    }

                    override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,response.body()?.data!!)
                                dialog?.dismiss()
                                startActivity(Intent(this@ShowProductActivity,ShowDoneActivity::class.java))
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }
            dialog?.show()}
        title.text = getString(R.string.product_details)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        getProduct()
        hide.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dailog_show)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener { dialog?.dismiss() }
            send.setOnClickListener {

                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Hide(lang.appLanguage,"Bearer"+user.userData.token
                        ,id,0,"product")?.enqueue(object : Callback<TermsResponse> {
                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                        dialog?.dismiss()
                    }

                    override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,response.body()?.data!!)
                                dialog?.dismiss()
                                startActivity(Intent(this@ShowProductActivity,ShowDoneActivity::class.java))
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }
            dialog?.show()
        }
        delete.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_delete)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener { dialog?.dismiss() }
            send.setOnClickListener {

                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Delete(lang.appLanguage,"Bearer"+user.userData.token
                        ,id,"product")?.enqueue(object : Callback<TermsResponse> {
                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                        dialog?.dismiss()
                    }

                    override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,response.body()?.data!!)
                                dialog?.dismiss()
                                startActivity(Intent(this@ShowProductActivity,DeleteDoneActivity::class.java))
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }
            dialog?.show()
        }
        back_main.setOnClickListener {
            val intent = Intent(this,EditProductActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
            finish()
        }
        repeat.setOnClickListener {
            val intent = Intent(this,RepeatProductActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
            finish()
        }

    }

    fun getProduct(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProduct(lang.appLanguage,"Bearer"+user.userData.token,id)
                ?.enqueue(object : Callback<ProductDetaildsResponse> {
                    override fun onFailure(call: Call<ProductDetaildsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()

                    }

                    override fun onResponse(call: Call<ProductDetaildsResponse>, response: Response<ProductDetaildsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){

                                initSliderAds(response.body()?.data?.images!!)
                                Glide.with(mContext).load(response.body()?.data?.image).into(image)
                                name.text = response.body()?.data?.name
                                description.text = response.body()?.data?.desc
                                quantity.text  = response.body()?.data?.quantity
                                views.text = response.body()?.data?.views.toString()
                                pure.text = response.body()?.data?.purchase.toString()
                                if (response.body()?.data?.discount.equals("")){
                                    pric.text = response.body()?.data?.price+ " "+getString(R.string.rs)
                                    offer.text = ""

                                }else{
                                    pric.text = response.body()?.data?.discount + " "+getString(R.string.rs)
                                    offer.text = response.body()?.data?.price+ " "+getString(R.string.rs)
                                    CommonUtil.setStrokInText(offer)
                                }

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    fun initSliderAds(list: ArrayList<ImagesModel>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
            indicator.visibility = View.GONE
            image.visibility = View.VISIBLE
        }
        else{
            viewPager.visibility= View.VISIBLE
            indicator.visibility = View.VISIBLE
            image.visibility = View.GONE
            viewPager.adapter= SliderAdapter(mContext!!,list)
            indicator.setViewPager(viewPager)
        }
    }
}