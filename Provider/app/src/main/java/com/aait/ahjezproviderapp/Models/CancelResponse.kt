package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class CancelResponse:BaseResponse(),Serializable {
    var data:ArrayList<CancelModel>?=null
}