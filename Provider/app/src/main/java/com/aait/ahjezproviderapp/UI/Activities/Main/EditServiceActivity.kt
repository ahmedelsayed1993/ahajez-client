package com.aait.ahjezproviderapp.UI.Activities.Main

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.*
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Controllers.ImageAdapter
import com.aait.ahjezproviderapp.UI.Controllers.ImagesAdapter
import com.aait.ahjezproviderapp.UI.Controllers.ListAdapter
import com.aait.ahjezproviderapp.UI.Controllers.ServiceTimesAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.aait.ahjezproviderapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import io.fotoapparat.selector.off
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class EditServiceActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_edit_service

    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var subcategory: TextView
    lateinit var subcategories: RecyclerView
    lateinit var name_ar: EditText
    lateinit var name_en: EditText
    lateinit var description_ar: EditText
    lateinit var description_en: EditText
    lateinit var price: EditText
    lateinit var discount: EditText
    lateinit var time_from: TextView
    lateinit var time_to: TextView
    lateinit var add_time: ImageView
    lateinit var free: RadioButton
    lateinit var paid: RadioButton
    lateinit var lay: LinearLayout
    lateinit var full: CheckBox
    lateinit var ratio: EditText
    lateinit var part: CheckBox
    lateinit var amount: EditText
    lateinit var yes: RadioButton
    lateinit var no: RadioButton
    lateinit var image: TextView
    lateinit var photo: ImageView
    lateinit var photos: RecyclerView
    lateinit var add: Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var listAdapter: ListAdapter
    var subs = ArrayList<ListModel>()
    lateinit var listModel: ListModel
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
            .setRequestCode(200)                                                 //Request code for activity results
            .setCount(10)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    var paths = ArrayList<String>()
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var imagesAdapter: ImageAdapter
    var fre_reservation = 1
    var refundable = 0
    var Times = ArrayList<TimesModel>()
    var Time = ArrayList<TimesModel>()
    lateinit var times:RecyclerView
    lateinit var serviceTimesAdapter: ServiceTimesAdapter
    var images = ArrayList<ImagesModel>()
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        subcategory = findViewById(R.id.subcategory)
        subcategories = findViewById(R.id.subcategories)
        name_ar = findViewById(R.id.name_ar)
        name_en = findViewById(R.id.name_en)
        description_ar = findViewById(R.id.description_ar)
        description_en = findViewById(R.id.description_en)
        price = findViewById(R.id.price)
        discount = findViewById(R.id.discount)
        time_from = findViewById(R.id.time_from)
        time_to = findViewById(R.id.time_to)
        add_time = findViewById(R.id.add_time)
        free = findViewById(R.id.free)
        paid = findViewById(R.id.paid)
        lay = findViewById(R.id.lay)
        full = findViewById(R.id.full)
        ratio = findViewById(R.id.ratio)
        part = findViewById(R.id.part)
        amount = findViewById(R.id.amount)
        yes = findViewById(R.id.yes)
        no = findViewById(R.id.no)
        image = findViewById(R.id.image)
        photo = findViewById(R.id.photo)
        photos = findViewById(R.id.photos)
        add = findViewById(R.id.add)
        times = findViewById(R.id.times)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.edit_service)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        linearLayoutManager2 = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        listAdapter = ListAdapter(mContext,subs,R.layout.recycle_list)
        imagesAdapter = ImageAdapter(mContext,images,R.layout.recycler_image)
        serviceTimesAdapter = ServiceTimesAdapter(mContext,Time,R.layout.recycler_service_times)
        listAdapter.setOnItemClickListener(this)
        imagesAdapter.setOnItemClickListener(this)
        serviceTimesAdapter.setOnItemClickListener(this)
        subcategories.layoutManager = linearLayoutManager
        photos.layoutManager = linearLayoutManager2
        times.layoutManager = linearLayoutManager1
        subcategories.adapter = listAdapter
        times.adapter = serviceTimesAdapter
        photos.adapter = imagesAdapter
        subcategory.setOnClickListener { getCategories() }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        photo.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }
        lay.visibility = View.GONE
        free.setOnClickListener {
            fre_reservation = 1
            lay.visibility = View.GONE
            refundable = 1
        }
        paid.setOnClickListener {
            fre_reservation = 0
            lay.visibility = View.VISIBLE
        }
        yes.setOnClickListener {
            refundable = 1
        }
        no.setOnClickListener {
            refundable = 0
        }
        time_to.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)



            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)

                            var hour = hourOfDay
                            var minute = minute
                            // hour1 = hourOfDay.toString()
                            var am_pm = ""
                            // AM_PM decider logic
//                            when {
//                                hour == 0 -> {
//                                    hour += 12
//                                    am_pm = "AM"
//                                }
//                                hour == 12 -> am_pm = "PM"
//                                hour > 12 -> {
//                                    hour -= 12
//                                    am_pm = "PM"
//                                }
//                                else -> am_pm = "AM"
//                            }

                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            time_to.text = hours.toString()+":"+minutes.toString()

                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.to))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)

            timePickerDialog.show()
        }
        time_from.setOnClickListener {   val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)



            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)

                            var hour = hourOfDay
                            var minute = minute
                            // hour1 = hourOfDay.toString()
                            var am_pm = ""
                            // AM_PM decider logic
//                            when {
//                                hour == 0 -> {
//                                    hour += 12
//                                    am_pm = "AM"
//                                }
//                                hour == 12 -> am_pm = "PM"
//                                hour > 12 -> {
//                                    hour -= 12
//                                    am_pm = "PM"
//                                }
//                                else -> am_pm = "AM"
//                            }

                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            time_from.text = hours.toString()+":"+minutes.toString()

                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.from))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)

            timePickerDialog.show() }
        add_time.setOnClickListener {
            if (CommonUtil.checkTextError(time_from,getString(R.string.from))||
                    CommonUtil.checkTextError(time_to,getString(R.string.to))){
                return@setOnClickListener
            }else{
                Times.add(TimesModel(time_from.text.toString(),time_to.text.toString()))
                time_from.text = ""
                time_to.text = ""
            }
        }
        getProduct()

        add.setOnClickListener {
            if (CommonUtil.checkTextError(subcategory,getString(R.string.Please_enter_the_section))||
                    CommonUtil.checkEditError(name_ar,getString(R.string.Please_enter_the_service_name_in_Arabic))||
                    CommonUtil.checkEditError(description_ar,getString(R.string.Please_enter_the_service_description_in_Arabic))||
                    CommonUtil.checkEditError(price,getString(R.string.Please_enter_the_service_price))){
                return@setOnClickListener
            }else{
                if (!Times.isEmpty()) {
                    if (fre_reservation == 1) {
                        if (discount.text.trim().toString().equals("")) {

                            AddProduct(Gson().toJson(Times), "0", "0","0")
                        }else{
                            AddProduct(Gson().toJson(Times), "0", "0",discount.text.toString())
                        }

                    } else {
                        if (full.isChecked&&part.isChecked){
                            if (CommonUtil.checkTextError(ratio,getString(R.string.Discount))||
                                    CommonUtil.checkTextError(amount,getString(R.string.the_amount))){
                                return@setOnClickListener
                            }else{
                                if (discount.text.trim().toString().equals("")) {

                                    AddProduct(
                                        Gson().toJson(Times),
                                        ratio.text.toString(),
                                        amount.text.toString(),
                                        "0"
                                    )
                                }else{
                                    AddProduct(
                                        Gson().toJson(Times),
                                        ratio.text.toString(),
                                        amount.text.toString(),
                                        discount.text.toString()
                                    )
                                }
                                }

                        }else if (full.isChecked&&!part.isChecked){
                            if (CommonUtil.checkTextError(ratio,getString(R.string.Discount))){
                                return@setOnClickListener
                            }else{
                                if (discount.text.trim().toString().equals("")) {

                                    AddProduct(Gson().toJson(Times), ratio.text.toString(), "0","0")
                                }else{
                                    AddProduct(Gson().toJson(Times), ratio.text.toString(), "0",discount.text.toString())
                                }

                            }
                        }else if (part.isChecked&&!full.isChecked){
                            if (
                                    CommonUtil.checkTextError(amount,getString(R.string.the_amount))){
                                return@setOnClickListener
                            }else{
                                if (discount.text.trim().toString().equals("")) {
                                    AddProduct(Gson().toJson(Times), "0", amount.text.toString(),"0")
                                }else{
                                    AddProduct(Gson().toJson(Times), "0", amount.text.toString(),discount.text.toString())
                                }

                            }
                        }else{
                            CommonUtil.makeToast(mContext,getString(R.string.Please_choose_a_method_to_pay_for_your_paid_reservation))
                        }

                    }
                }else{
                    if (fre_reservation == 1) {
                        if (discount.text.trim().toString().equals("")) {
                        AddProduct(null, "0", "0","0")}
                        else{
                            AddProduct(null, "0", "0",discount.text.toString())
                        }

                    } else {
                        if (full.isChecked&&part.isChecked){
                            if (CommonUtil.checkTextError(ratio,getString(R.string.Discount))||
                                    CommonUtil.checkTextError(amount,getString(R.string.the_amount))){
                                return@setOnClickListener
                            }else{
                                if (discount.text.trim().toString().equals("")) {

                                    AddProduct(null, ratio.text.toString(), amount.text.toString(),"0")
                                }else{
                                    AddProduct(null, ratio.text.toString(), amount.text.toString(),discount.text.toString())
                                }
                            }

                        }else if (full.isChecked&&!part.isChecked){
                            if (CommonUtil.checkTextError(ratio,getString(R.string.Discount))){
                                return@setOnClickListener
                            }else{
                                if (discount.text.trim().toString().equals("")) {
                                    AddProduct(null, ratio.text.toString(), "0","0")
                                }else{
                                    AddProduct(null, ratio.text.toString(), "0",discount.text.toString())
                                }

                            }
                        }else if (part.isChecked&&!full.isChecked){
                            if (
                                    CommonUtil.checkTextError(amount,getString(R.string.the_amount))){
                                return@setOnClickListener
                            }else{
                                if (discount.text.trim().toString().equals("")) {
                                    AddProduct(null, "0", amount.text.toString(),"0")
                                }else{
                                    AddProduct(null, "0", amount.text.toString(),discount.text.toString())
                                }

                            }
                        }else{
                            CommonUtil.makeToast(mContext,getString(R.string.Please_choose_a_method_to_pay_for_your_paid_reservation))
                        }

                    }
                }

            }
            Log.e("free",fre_reservation.toString())
            Log.e("times",Gson().toJson(Times))
        }
    }
    fun getCategories(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderSubCats(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object : Callback<ListResponse> {
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                subcategories.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getProduct(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getService(lang.appLanguage,"Bearer"+user.userData.token,id)
                ?.enqueue(object : Callback<ServiceDetailsResponse> {
                    override fun onFailure(call: Call<ServiceDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()

                    }

                    override fun onResponse(call: Call<ServiceDetailsResponse>, response: Response<ServiceDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){


                                if (!response.body()?.data?.image.equals("")){
                                    image.text = getString(R.string.attach_image)
                                }else{
                                    image.text = ""
                                }
                                photos.visibility = View.VISIBLE
                                listModel = ListModel(response.body()?.data?.subcategory_id,response.body()?.data?.subcategory)
                                subcategory.text = response.body()?.data?.subcategory
                                name_ar.setText(response.body()?.data?.name_ar)
                                name_en.setText(response.body()?.data?.name_en)
                                description_ar.setText(response.body()?.data?.desc_ar)
                                description_en.setText(response.body()?.data?.desc_en)
                                price.setText(response.body()?.data?.price)

                                serviceTimesAdapter.updateAll(response.body()?.data?.times!!)
                                imagesAdapter.updateAll(response.body()?.data?.images!!)
                                discount.setText(response.body()?.data?.total_discount)

                                ratio.setText(response.body()?.data?.discount)
                                amount.setText(response.body()?.data?.part_amount)
                                fre_reservation = response.body()?.data?.free_reservation!!.toInt()
                                refundable = response.body()?.data?.refundable!!

                                if (response.body()?.data?.free_reservation.equals("1")){
                                    free.isChecked = true
                                    lay.visibility = View.GONE

                                }else{
                                    paid.isChecked = true
                                    lay.visibility = View.VISIBLE
                                }
                                if (response.body()?.data?.refundable==0){
                                    no.isChecked = true
                                }else{
                                    yes.isChecked = true
                                }
                                if (!response.body()?.data?.part_amount.equals("")){
                                    part.isChecked = true

                                }else{
                                    part.isChecked = false
                                }
                                if (!response.body()?.data?.discount.equals("")){
                                    full.isChecked = true
                                }else{
                                    full.isChecked = false
                                }

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){

            subcategories.visibility = View.GONE
            listModel = subs.get(position)
            subcategory.text = listModel.name

        }else if (view.id == R.id.delete){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DeleteImage(lang.appLanguage,"Bearer"+user.userData.token,images.get(position).id!!,"service")
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    getProduct()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }else if (view.id == R.id.cancel){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DeleteTime(lang.appLanguage,"Bearer"+user.userData.token,Time.get(position).id!!)
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    getProduct()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]
//                for (i in 0..returnValue!!.size-1){
//                    paths.add(returnValue!![i])
//                }
//                if(paths.size==1){
//
//                    image.visibility = View.GONE
//                }else{
//
//                    image.visibility = View.VISIBLE
//                   // imagesAdapter.updateAll(paths)
//                }
                Log.e("pathsss", Gson().toJson(returnValue))



                if (ImageBasePath != null) {
                    AddProduct(ImageBasePath!!)
                    image.text = getString(R.string.attach_image)
                }
            }
        }else if (requestCode == 200){
            if (resultCode == 0) {

            } else {
                returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                // ImageBasePath = returnValue!![0]
                for (i in 0..returnValue1!!.size-1){
                    paths.add(returnValue1!![i])
                }
                if(paths.size!=0){

                    AddProduct(paths)
                    //imagesAdapter.updateAll(paths)
                }
                Log.e("pathsss", Gson().toJson(returnValue1))



//                if (ImageBasePath != null) {
//                    // upLoad(ImageBasePath!!)
//                    image.text = getString(R.string.attach_image)
//                }
            }
        }
    }
    fun AddProduct(imges:ArrayList<String>){
        showProgressDialog(getString(R.string.please_wait))
        var imgs = ArrayList<MultipartBody.Part>()

        for (i in 0..imges.size-1){
            var filePart: MultipartBody.Part? = null
            val ImageFile = File(imges.get(i))
            val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
            imgs.add(filePart)
        }
        Client.getClient()?.create(Service::class.java)?.editServiceImages(lang.appLanguage,"Bearer"+user.userData.token,id,imgs)?.enqueue(object :Callback<ServiceDetailsResponse>{
            override fun onFailure(call: Call<ServiceDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ServiceDetailsResponse>, response: Response<ServiceDetailsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        val  intent = Intent(this@EditServiceActivity,ServiceDetailsActivity::class.java)
                        intent.putExtra("id",id)
                        startActivity(intent)
                        finish()



                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun AddProduct(path:String){
        showProgressDialog(getString(R.string.please_wait))

        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)

        Client.getClient()?.create(Service::class.java)?.editServiceImage(lang.appLanguage,"Bearer"+user.userData.token,id,filePart!!)?.enqueue(object :Callback<ServiceDetailsResponse>{
            override fun onFailure(call: Call<ServiceDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ServiceDetailsResponse>, response: Response<ServiceDetailsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        val  intent = Intent(this@EditServiceActivity,ServiceDetailsActivity::class.java)
                        intent.putExtra("id",id)
                        startActivity(intent)
                        finish()



                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun AddProduct(times:String?,dis:String?,amo:String?,discount:String){
        if (price.text.toString()=="0"){
            price.error = getString(R.string.Please_enter_the_service_price)
        }else {
            showProgressDialog(getString(R.string.please_wait))

            Client.getClient()?.create(Service::class.java)?.EditServices(
                lang.appLanguage,
                "Bearer" + user.userData.token,
                listModel.id!!,
                id,
                name_ar.text.toString(),
                name_en.text.toString(),
                description_ar.text.toString(),
                description_en.text.toString(),
                price.text.toString(),
                dis,
                times,
                fre_reservation,
                discount,
                amo,
                refundable
            )?.enqueue(object : Callback<ServiceDetailsResponse> {
                override fun onFailure(call: Call<ServiceDetailsResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                }

                override fun onResponse(
                    call: Call<ServiceDetailsResponse>,
                    response: Response<ServiceDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {


                            val intent =
                                Intent(this@EditServiceActivity, ServiceDetailsActivity::class.java)
                            intent.putExtra("id", id)
                            startActivity(intent)
                            finish()


                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }
                }

            })
        }
    }
}