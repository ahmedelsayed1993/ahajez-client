package com.aait.ahjezproviderapp.UI.Activities.Main

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.*
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.AppInfo.SubSectionActivity
import com.aait.ahjezproviderapp.UI.Controllers.ImageAdapter
import com.aait.ahjezproviderapp.UI.Controllers.ImagesAdapter
import com.aait.ahjezproviderapp.UI.Controllers.ListAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.aait.ahjezproviderapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class EditProductActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_edit_product
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var subcategory: TextView
    lateinit var subcategories: RecyclerView
    lateinit var subsection: TextView
    lateinit var subsections: RecyclerView
    lateinit var add_new: TextView
    lateinit var name_ar: EditText
    lateinit var name_en: EditText
    lateinit var description_ar: EditText
    lateinit var description_en: EditText
    lateinit var price: EditText
    lateinit var discount: EditText
    lateinit var quantity: EditText
    lateinit var image: TextView
    lateinit var photo: ImageView
    lateinit var photos: RecyclerView
    lateinit var add: Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var listAdapter: ListAdapter
    lateinit var listAdapter1: ListAdapter
    var subs = ArrayList<ListModel>()
    var sections = ArrayList<ListModel>()
    lateinit var listModel: ListModel
    lateinit var listModel1: ListModel
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var imagesAdapter: ImageAdapter
    var selected = 0
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
            .setRequestCode(200)                                                 //Request code for activity results
            .setCount(10)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    var paths = ArrayList<String>()
    var id = 0
    var images = ArrayList<ImagesModel>()
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        subsection = findViewById(R.id.subsection)
        subsections = findViewById(R.id.subsections)
        subcategory = findViewById(R.id.subcategory)
        subcategories = findViewById(R.id.subcategories)
        add_new = findViewById(R.id.add_new)
        name_ar = findViewById(R.id.name_ar)
        name_en = findViewById(R.id.name_en)
        description_ar = findViewById(R.id.description_ar)
        description_en = findViewById(R.id.description_en)
        price = findViewById(R.id.price)
        discount = findViewById(R.id.discount)
        quantity = findViewById(R.id.quantity)
        image = findViewById(R.id.image)
        photo = findViewById(R.id.photo)
        photos = findViewById(R.id.photos)
        add = findViewById(R.id.add)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.edit_product)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        add_new.setOnClickListener { startActivity(Intent(this,SubSectionActivity::class.java))
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        linearLayoutManager2 = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        listAdapter = ListAdapter(mContext,subs,R.layout.recycle_list)
        listAdapter1 = ListAdapter(mContext,sections,R.layout.recycle_list)
        imagesAdapter = ImageAdapter(mContext,images,R.layout.recycler_image)
        listAdapter.setOnItemClickListener(this)
        listAdapter1.setOnItemClickListener(this)
        imagesAdapter.setOnItemClickListener(this)
        subcategories.layoutManager = linearLayoutManager
        subsections.layoutManager = linearLayoutManager1
        photos.layoutManager = linearLayoutManager2
        subcategories.adapter = listAdapter
        subsections.adapter = listAdapter1
        photos.adapter = imagesAdapter
        subcategory.setOnClickListener {
            selected = 0
            getCategories()
        }
        subsection.setOnClickListener {
            selected = 1
            if (subcategory.text.toString().equals("")){
                subcategory.error = getString(R.string.Please_enter_the_main_section)
            }else{
                getHome()
            }
        }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        photo.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }

        add.setOnClickListener {
            if (CommonUtil.checkTextError(subcategory,getString(R.string.Please_enter_the_main_section))||
                    CommonUtil.checkTextError(subsection,getString(R.string.Please_select_subsection))||
                    CommonUtil.checkEditError(name_ar,getString(R.string.Please_enter_the_product_name_in_Arabic))||
                    CommonUtil.checkEditError(description_ar,getString(R.string.Please_enter_the_description_in_Arabic))||
                    CommonUtil.checkEditError(price,getString(R.string.Please_enter_the_product_price))||
                    CommonUtil.checkEditError(quantity,getString(R.string.Please_enter_the_available_quantity))){
                return@setOnClickListener
            }else{

                if (discount.text.trim().toString().equals("")) {
                    AddProducts("0")
                }else{
                    AddProducts(discount.text.toString())
                }

            }
        }
        getProduct()

    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.SubSections(lang.appLanguage,"Bearer"+user.userData.token,listModel.id!!)?.enqueue(object :
                Callback<ListResponse> {
            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                hideProgressDialog()
                subsections.visibility = View.VISIBLE
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))

//                            initSliderAds(response.body()?.slider!!)
                            listAdapter1.updateAll(response.body()!!.data!!)

//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    fun getCategories(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderSubCats(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object : Callback<ListResponse> {
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                subcategories.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            if (selected == 0){
                subcategories.visibility = View.GONE
                listModel = subs.get(position)
                subcategory.text = listModel.name
            }else{
                subsections.visibility = View.GONE
                listModel1 = sections.get(position)
                subsection.text = listModel1.name
            }
        }else if (view.id == R.id.delete){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DeleteImage(lang.appLanguage,"Bearer"+user.userData.token,images.get(position).id!!,"product")
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    getProduct()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
//            paths.remove(paths.get(position))
           // imagesAdapter.updateAll(paths)
        }

    }

    fun getProduct(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProduct(lang.appLanguage,"Bearer"+user.userData.token,id)
                ?.enqueue(object : Callback<ProductDetaildsResponse>{
                    override fun onFailure(call: Call<ProductDetaildsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()

                    }

                    override fun onResponse(call: Call<ProductDetaildsResponse>, response: Response<ProductDetaildsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){


                                image.text = getString(R.string.attach_image)
                                name_ar.setText(response.body()?.data?.name_ar)
                                name_en.setText(response.body()?.data?.name_en)
                                description_ar.setText(response.body()?.data?.desc_ar)
                                description_en.setText(response.body()?.data?.desc_en)
                                listModel = ListModel(response.body()?.data?.subcategory_id,response.body()?.data?.subcategory)
                                subcategory.text = response.body()?.data?.subcategory
                                listModel1 = ListModel(response.body()?.data?.subsection_id,response.body()?.data?.subsection)
                                subsection.text = response.body()?.data?.subsection
                                price.setText(response.body()?.data?.price)
                                discount.setText(response.body()?.data?.discount)
                                quantity.setText(response.body()?.data?.quantity)
                                imagesAdapter.updateAll(response.body()?.data?.images!!)



                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]
//                for (i in 0..returnValue!!.size-1){
//                    paths.add(returnValue!![i])
//                }
//                if(paths.size==1){
//
//                    image.visibility = View.GONE
//                }else{
//
//                    image.visibility = View.VISIBLE
//                   // imagesAdapter.updateAll(paths)
//                }
                Log.e("pathsss", Gson().toJson(returnValue))



                if (ImageBasePath != null) {
                    AddProduct(ImageBasePath!!)
                    image.text = getString(R.string.attach_image)
                }
            }
        }else if (requestCode == 200){
            if (resultCode == 0) {

            } else {
                returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                // ImageBasePath = returnValue!![0]
                for (i in 0..returnValue1!!.size-1){
                    paths.add(returnValue1!![i])
                }
                if(paths.size!=0){

                    AddProduct(paths)
                    //imagesAdapter.updateAll(paths)
                }
                Log.e("pathsss", Gson().toJson(returnValue1))



//                if (ImageBasePath != null) {
//                    // upLoad(ImageBasePath!!)
//                    image.text = getString(R.string.attach_image)
//                }
            }
        }
    }
    fun AddProduct(imges:ArrayList<String>){
        showProgressDialog(getString(R.string.please_wait))
        var imgs = ArrayList<MultipartBody.Part>()

        for (i in 0..imges.size-1){
            var filePart: MultipartBody.Part? = null
            val ImageFile = File(imges.get(i))
            val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
            imgs.add(filePart)
        }
        Client.getClient()?.create(Service::class.java)?.editProductImages(lang.appLanguage,"Bearer"+user.userData.token,id,imgs)?.enqueue(object :Callback<ProductDetaildsResponse>{
            override fun onFailure(call: Call<ProductDetaildsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ProductDetaildsResponse>, response: Response<ProductDetaildsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        getProduct()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun AddProduct(path:String){
        showProgressDialog(getString(R.string.please_wait))

        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)

        Client.getClient()?.create(Service::class.java)?.editProductImage(lang.appLanguage,"Bearer"+user.userData.token,id,filePart!!)?.enqueue(object :Callback<ProductDetaildsResponse>{
            override fun onFailure(call: Call<ProductDetaildsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ProductDetaildsResponse>, response: Response<ProductDetaildsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        getProduct()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun AddProducts( dis:String){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.EditProduct(lang.appLanguage,"Bearer"+user.userData.token,id,
                listModel.id!!,listModel1.id!!,name_ar.text.toString(),name_en.text.toString(),description_ar.text.toString(),description_en.text.toString()
                ,price.text.toString(),dis,quantity.text.toString())?.enqueue(object :Callback<ProductDetaildsResponse>{
            override fun onFailure(call: Call<ProductDetaildsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ProductDetaildsResponse>, response: Response<ProductDetaildsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                                val  intent = Intent(this@EditProductActivity,ProductDetailsActivity::class.java)
                                intent.putExtra("id",response.body()?.data?.id)
                                startActivity(intent)
                                finish()


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}