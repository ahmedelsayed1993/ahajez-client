package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.ahjezproviderapp.R

import com.aait.ahjezproviderapp.UI.Fragments.HidenProductsFragment
import com.aait.ahjezproviderapp.UI.Fragments.HidenServicesFragment

class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            HidenProductsFragment()
        } else {
            HidenServicesFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.products)
        } else {
            context.getString(R.string.services)
        }
    }
}
