package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class NotificationResponse:BaseResponse(),Serializable {
    var data:ArrayList<NotificationModel>?=null
}