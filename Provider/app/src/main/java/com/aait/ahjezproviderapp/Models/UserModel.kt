package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class UserModel:Serializable {
    var token:String?=null
    var name:String?=null
    var phone:String?=null
    var email:String?=null
    var code:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var avatar:String?=null
    var date:String?=null
    var driver:Int?=null
    var user_type:String?=null
    var active:Int?=null
    var confirm:Int?=null
    var work_times:Int?=null
    var number:Int?=null
    var name_ar:String?=null
    var name_en:String?=null
    var desc_ar:String?=null
    var desc_en:String?=null
    var region:String?=null
    var region_id:Int?=null
    var city:String?=null
    var city_id:Int?=null
    var mac_address_id:String?=null
    var neighborhood:String?=null
    var neighborhood_id:Int?=null
    var street:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var bank_name:String?=null
    var account_name:String?=null
    var iban_number:String?=null



}