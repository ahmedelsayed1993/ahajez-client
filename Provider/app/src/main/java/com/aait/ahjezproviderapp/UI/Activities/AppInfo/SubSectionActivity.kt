package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.Models.ListResponse
import com.aait.ahjezproviderapp.Models.TermsResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.UI.Controllers.ListAdapter
import com.aait.ahjezproviderapp.UI.Controllers.SubSectionAdapter
import com.aait.ahjezproviderapp.UI.Controllers.SubsAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubSectionActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_subsection
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var notificationModels = java.util.ArrayList<ListModel>()
    internal lateinit var subSectionAdapter: SubSectionAdapter
    lateinit var add:Button
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var listAdapter: ListAdapter
    var subCategories = ArrayList<ListModel>()
    lateinit var listModel: ListModel
    lateinit var subs:RecyclerView
    lateinit var subsection:TextView
    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        notification = findViewById(R.id.notification)
        title.text = getString(R.string.notification)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.Subsections_for_services)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        add = findViewById(R.id.add)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        subSectionAdapter =  SubSectionAdapter(mContext,notificationModels,R.layout.recycler_subsection)
        subSectionAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = subSectionAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()
        add.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_add_subsection)
             subsection = dialog?.findViewById<TextView>(R.id.subsection)
             subs = dialog?.findViewById<RecyclerView>(R.id.subs)
            val name_ar = dialog?.findViewById<EditText>(R.id.name_ar)
            val name_en = dialog?.findViewById<EditText>(R.id.name_en)
            val send = dialog?.findViewById<Button>(R.id.send)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            subs.visibility = View.GONE
            linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
            listAdapter = ListAdapter(mContext,subCategories,R.layout.recycle_list)
            listAdapter.setOnItemClickListener(this)
            subs.layoutManager = linearLayoutManager1
            subs.adapter = listAdapter
            subsection.setOnClickListener {  getCategories()}

            cancel.setOnClickListener { dialog?.dismiss() }
            send.setOnClickListener {
                if (CommonUtil.checkTextError(subsection,getString(R.string.Please_select_subsection))||
                        CommonUtil.checkEditError(name_ar,getString(R.string.Please_enter_the_name_of_the_department_in_Arabic))||
                        CommonUtil.checkEditError(name_en,getString(R.string.Please_enter_the_name_of_the_department_in_English))){
                    return@setOnClickListener
                }else{
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.AddSubSection(lang.appLanguage,"Bearer"+user.userData.token
                    ,listModel.id!!,name_ar.text.toString(),name_en.text.toString())?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            dialog?.dismiss()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            dialog?.dismiss()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    getHome()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })

                }
            }
            dialog?.show()

        }


    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.SubSections(lang.appLanguage,"Bearer"+user.userData.token,null)?.enqueue(object :
                Callback<ListResponse> {
            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                subSectionAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }
    fun getCategories(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderSubCats(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                subs.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name)
        {
            subs.visibility = View.GONE
            listModel = subCategories.get(position)
            subsection.text = listModel.name
        }else if (view.id == R.id.delete){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DeleteSubSection(lang.appLanguage,"Bearer"+user.userData
                    .token,notificationModels.get(position).id!!)?.enqueue(object :Callback<TermsResponse>{
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

                override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                           // CommonUtil.makeToast(mContext,response.body()?.data!!)
                            getHome()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
        }
        
    }
}