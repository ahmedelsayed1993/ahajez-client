package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class ProductDetailsModel:Serializable {
    var image:String?=null
    var images:ArrayList<ImagesModel>?=null
    var id:Int?=null
    var subcategory_id:Int?=null
    var subcategory:String?=null
    var subsection_id:Int?=null
    var subsection:String?=null
    var name:String?=null
    var name_ar:String?=null
    var name_en:String?=null
    var price:String?=null
    var discount:String?=null
    var views:Int?=null
    var purchase:Int?=null
    var desc:String?=null
    var desc_ar:String?=null
    var desc_en:String?=null
    var quantity:String?=null
    var hidden:Int?=null
}