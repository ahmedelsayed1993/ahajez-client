package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Fragments.HidenProductsFragment
import com.aait.ahjezproviderapp.UI.Fragments.HidenServicesFragment
import com.aait.ahjezproviderapp.UI.Fragments.OrderFragment
import com.aait.ahjezproviderapp.UI.Fragments.ReservationFragment

class OrderTapAdapter (
        private val context: Context,
        fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            OrderFragment()
        } else {
            ReservationFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.Requests_for_products)
        } else {
            context.getString(R.string.Reservations_requests)
        }
    }
}
