package com.aait.ahjezproviderapp.UI.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatDelegate
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Auth.WorkTimesActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.MainActivity

class SplashActivity  : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash

    var isSplashFinishid = false
    override fun initializeComponents() {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (user.loginStatus!!){

                    val intent = Intent(this, MainActivity::class.java)

                    startActivity(intent)
                    this@SplashActivity.finish()
//                        var intent = Intent(this@SplashActivity, PreActivity::class.java)
//                        startActivity(intent)
//                        finish()

                }else {
                    var intent = Intent(this@SplashActivity, PreActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }, 2100)
        }, 1800)
    }


}
