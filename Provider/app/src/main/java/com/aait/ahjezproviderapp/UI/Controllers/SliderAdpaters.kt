package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.ScaleGestureDetector
import android.view.View
import com.aait.ahjezproviderapp.Models.ImagesModel
import com.aait.ahjezproviderapp.R

import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import com.github.islamkhsh.CardSliderAdapter



class SliderAdpaters (context: Context, list : ArrayList<ImagesModel>) : CardSliderAdapter<ImagesModel>(list) {


    var list = list
    var context=context

    lateinit var image: PhotoView
    override fun bindView(position: Int, itemContentView: View, item: ImagesModel?) {
        image = itemContentView.findViewById(R.id.image)
        item!!.image?.let { Log.e("image", it) }
        Glide.with(context).load(item?.image).into(image)
//        itemContentView.setOnClickListener {
//            val intent  = Intent(context, ImageActivity::class.java)
//            intent.putExtra("link",list)
//            context.startActivity(intent)
//        }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.images }

}