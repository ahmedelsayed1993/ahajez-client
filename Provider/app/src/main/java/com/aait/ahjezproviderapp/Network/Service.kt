package com.aait.ahjezproviderapp.Network

import com.aait.ahjezproviderapp.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Service {

    @FormUrlEncoded
    @POST("check-code")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Field("code") code:String,
                  @Header("lang") lang: String): Call<UserResponse>

    @POST("resend-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String): Call<UserResponse>
    @POST("send-code-email")
    fun ResendEmail(@Header("Authorization") Authorization:String,
                    @Header("lang") lang: String): Call<UserResponse>
    @FormUrlEncoded
    @POST("sign-in")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Field("mac_address_id") mac_address_id:String,
              @Header("lang") lang: String): Call<UserResponse>

    @POST("forget-password")
    fun ForGot(@Query("phone") phone:String,
               @Query("lang") lang:String): Call<UserResponse>

    @POST("update-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Query("password") password:String,
                @Query("code") code:String,
                @Query("lang") lang: String): Call<UserResponse>
    @FormUrlEncoded
    @POST("reset-password")
    fun resetPassword(@Header("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Field("current_password") current_password:String,
                      @Field("password") password:String): Call<BaseResponse>

    @POST("providers-types")
    fun ProviderTypes(@Header("lang") lang:String) :Call<ListResponse>

    @POST("categories")
    fun Categories(@Header("lang") lang:String):Call<ListResponse>

    @POST("subcategories")
    fun SubCategories(@Header("lang") lang:String,
                      @Query("category_id") category_id:Int):Call<ListResponse>
    @POST("regions")
    fun Regions(@Header("lang") lang:String):Call<ListResponse>

    @POST("cities")
    fun Cities(@Header("lang") lang:String,
               @Query("region_id") region_id:Int ):Call<ListResponse>
    @POST("neighborhoods")
    fun Neighborhoods(@Header("lang") lang:String,
    @Query("city_id") city_id:Int):Call<ListResponse>

    @POST("terms")
    fun Terms(@Header("lang") lang: String):Call<TermsResponse>
    @Multipart
    @POST("sign-up-provider")
    fun Sign_up(@Header("lang") lang: String,
    @Query("type_id") type_id:Int,
    @Query("legal_name") legal_name:String,
    @Query("commercial") commercial:String,
    @Query("num_branches") num_branches:String?,
    @Query("delivery_service") delivery_service:String,
    @Part avatar:MultipartBody.Part,
    @Query("name_ar") name_ar:String,
    @Query("name_en") name_en:String,
    @Query("desc_ar") desc_ar:String,
    @Query("desc_en") desc_en:String,
    @Query("category_id") category_id:Int,
    @Query("subcategories") subcategories:String,
    @Query("region_id") region_id:Int,
    @Query("city_id") city_id:Int,
    @Query("neighborhood_id") neighborhood_id:Int?,
    @Query("street") street:String?,
    @Query("lat") lat:String,
    @Query("lng") lng:String,
    @Query("address") address:String,
    @Query("bank_name") bank_name:String,
    @Query("account_name") account_name:String,
    @Query("iban_number") iban_number:String,
    @Query("phone") phone:String,
    @Query("email") email:String?,
    @Query("password") password:String,
    @Query("vision") vision:String?,
    @Query("device_id") device_id:String,
    @Query("device_type") device_type:String):Call<UserResponse>

    @POST("sign-up-provider")
    fun Sign_upwithout(@Header("lang") lang: String,
                @Query("type_id") type_id:Int,
                @Query("legal_name") legal_name:String,
                @Query("commercial") commercial:String,
                @Query("num_branches") num_branches:String?,
                @Query("delivery_service") delivery_service:String,
                @Query("name_ar") name_ar:String,
                @Query("name_en") name_en:String,
                @Query("desc_ar") desc_ar:String,
                @Query("desc_en") desc_en:String,
                @Query("category_id") category_id:Int,
                @Query("subcategories") subcategories:String,
                @Query("region_id") region_id:Int,
                @Query("city_id") city_id:Int,
                @Query("neighborhood_id") neighborhood_id:Int?,
                @Query("street") street:String?,
                @Query("lat") lat:String,
                @Query("lng") lng:String,
                @Query("address") address:String,
                @Query("bank_name") bank_name:String,
                @Query("account_name") account_name:String,
                @Query("iban_number") iban_number:String,
                @Query("phone") phone:String,
                @Query("email") email:String?,
                @Query("password") password:String,
                @Query("vision") vision:String?,
                @Query("device_id") device_id:String,
                @Query("device_type") device_type:String):Call<UserResponse>
    @FormUrlEncoded
    @POST("edit-profile-provider")
    fun getProfile(@Header("Authorization") Authorization:String,
                   @Header("lang") lang:String,
                   @Field("phone") phone: String?,
                   @Field("email") email: String?,
                   @Query("name_ar") name_ar:String?,
                   @Query("name_en") name_en:String?,
                   @Query("desc_ar") desc_ar:String?,
                   @Query("desc_en") desc_en:String?,
                   @Query("region_id") region_id:Int?,
                   @Query("city_id") city_id:Int?,
                   @Query("neighborhood_id") neighborhood_id:Int?,
                   @Query("street") street:String?,
                   @Query("lat") lat:String?,
                   @Query("lng") lng:String?,
                   @Query("address") address:String?,
                   @Query("bank_name") bank_name:String?,
                   @Query("account_name") account_name:String?,
                   @Query("iban_number") iban_number:String?,
                   @Query("mac_address_id") mac_address_id:String):Call<UserResponse>
    @Multipart
    @POST("edit-profile-provider")
    fun AddImage(@Header("Authorization") Authorization:String,
                 @Header("lang") lang:String,
                 @Part avatar: MultipartBody.Part,
                 @Query("mac_address_id") mac_address_id:String):Call<UserResponse>

    @POST("work-times")
    fun AddTimes(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String,
                 @Query("time_from") time_from:String?,
                 @Query("time_to") time_to:String?,
                 @Query("days") days:String?,
                 @Query("extra_from") extra_from:String?,
                 @Query("extra_to") extra_to:String?):Call<WorkTimesResponse>

    @POST("notifications")
    fun Notification(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String):Call<NotificationResponse>
    @POST("delete-notification")
    fun delete(
            @Header("Authorization") Authorization:String,
            @Header("lang") lang:String,
            @Query("notification_id") notification_id:Int):Call<TermsResponse>
    @POST("about")
    fun About(@Header("lang") lang: String):Call<TermsResponse>

    @POST("provider-delivery-service")
    fun Delivery( @Header("Authorization") Authorization:String,
                  @Header("lang") lang:String,
                  @Query("delivery_price") delivery_price:String?,
                  @Query("maximum_distance") maximum_distance:String?,
                  @Query("different_distance") different_distance:String?,
                  @Query("check_different_distance") check_different_distance:String?,
                  @Query("delivery_service") delivery_service:String?):Call<DeliveryResponse>

    @POST("portfolio")
    fun Balance(@Header("Authorization") Authorization:String,
                @Header("lang") lang: String):Call<BalanceResponse>
    @POST("bank-accounts")
    fun Banks(@Header("lang") lang: String,
              @Header("Authorization") Authorization:String):Call<BanksResponse>
    @Multipart
    @POST("payment-bank-transfer")
    fun Transfer(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String,
                 @Query("account_name") account_name:String,
                 @Query("bank_name") bank_name:String,
                 @Query("amount") amount:String,
                 @Part image:MultipartBody.Part):Call<TermsResponse>

    @POST("balance-recovery")
    fun Recover(@Header("lang") lang: String,
                @Header("Authorization") Authorization:String,
                @Query("message") message:String,
                @Query("bank_name") bank_name: String,
                @Query("account_name") account_name: String,
                @Query("account_number") account_number:String,
                @Query("iban_number") iban:String,
                @Query("amount") amount:String):Call<TermsResponse>
    @POST("log-out")
    fun logOut(@Header("Authorization") Authorization:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("mac_address_id") mac_address_id:String,
               @Header("lang") lang:String):Call<TermsResponse>

    @FormUrlEncoded
    @POST("contact-us")
    fun Contact(@Header("lang") lang:String,
                @Field("name") name:String,
                @Field("phone") phone:String,
                @Field("message") message:String):Call<TermsResponse>


    @POST("subsections")
    fun SubSections(@Header("lang") lang: String,
                    @Header("Authorization") Authorization:String,
                    @Query("subcategory_id") subcategory_id:Int?):Call<ListResponse>
    
    @POST("providers-subcategories")
    fun ProviderSubCats(@Header("lang") lang: String,
                        @Header("Authorization") Authorization:String):Call<ListResponse>
    @POST("add-subsection")
    fun AddSubSection(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String,
                      @Query("subcategory_id") subcategory_id:Int,
                      @Query("name_ar") name_ar:String,
                      @Query("name_en") name_en: String):Call<TermsResponse>

    @POST("delete-subsection")
    fun DeleteSubSection(@Header("lang") lang: String,
                         @Header("Authorization") Authorization:String,
                         @Query("subsection_id") subsection_id:Int):Call<TermsResponse>

    @Multipart
    @POST("add-product")
    fun AddProduct(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("subcategory_id") subcategory_id:Int,
                   @Query("subsection_id") subsection_id:Int,
                   @Query("name_ar") name_ar:String,
                   @Query("name_en") name_en:String,
                   @Query("desc_ar") desc_ar:String,
                   @Query("desc_en") desc_en:String,
                   @Query("price") price:String,
                   @Query("discount") discount:String,
                   @Query("quantity") quantity:String,
                   @Part image:MultipartBody.Part,
                   @Part images:ArrayList<MultipartBody.Part>):Call<ProductRespopnse>

    @Multipart
    @POST("add-product")
    fun AddProducts(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("subcategory_id") subcategory_id:Int,
                   @Query("subsection_id") subsection_id:Int,
                   @Query("name_ar") name_ar:String,
                   @Query("name_en") name_en:String,
                   @Query("desc_ar") desc_ar:String,
                   @Query("desc_en") desc_en:String,
                   @Query("price") price:String,
                   @Query("discount") discount:String,
                   @Query("quantity") quantity:String,
                   @Part image:MultipartBody.Part):Call<ProductRespopnse>
    @POST("edit-product")
    fun EditProduct(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                    @Query("product_id") product_id: Int,
                   @Query("subcategory_id") subcategory_id:Int,
                   @Query("subsection_id") subsection_id:Int,
                   @Query("name_ar") name_ar:String,
                   @Query("name_en") name_en:String,
                   @Query("desc_ar") desc_ar:String,
                   @Query("desc_en") desc_en:String,
                   @Query("price") price:String,
                   @Query("discount") discount:String,
                   @Query("quantity") quantity:String):Call<ProductDetaildsResponse>

    @Multipart
    @POST("edit-product")
    fun editProductImages(@Header("lang") lang: String,
                         @Header("Authorization") Authorization:String,
                         @Query("product_id") product_id: Int,
                         @Part images:ArrayList<MultipartBody.Part>):Call<ProductDetaildsResponse>
    @Multipart
    @POST("edit-product")
    fun editProductImage(@Header("lang") lang: String,
                         @Header("Authorization") Authorization:String,
                         @Query("product_id") product_id: Int,
                          @Part image:MultipartBody.Part):Call<ProductDetaildsResponse>

    @POST("main-products")
    fun getProducts(@Header("lang") lang: String,
                    @Header("Authorization") Authorization:String,
                    @Query("subcategory_id") subcategory_id:Int?,
                    @Query("subsection_id") subsection_id:Int?):Call<MainProductsResponse>
    @POST("main-services")
    fun getServices(@Header("lang") lang: String,
                    @Header("Authorization") Authorization:String,
                    @Query("subcategory_id") subcategory_id:Int?):Call<MainProductsResponse>
    @POST("hidden-products-services")
    fun getHidden(@Header("lang") lang: String,
                  @Header("Authorization") Authorization:String,
                  @Query("type") type: String):Call<HiddenResponse>
    @POST("details-product")
    fun getProduct(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("product_id") product_id:Int):Call<ProductDetaildsResponse>

    @POST("hidden-product")
    fun Hide(@Header("lang") lang: String,
             @Header("Authorization") Authorization:String,
             @Query("type_id") type_id:Int,
             @Query("hidden") hidden:Int,
             @Query("type") type:String):Call<TermsResponse>
    @POST("delete-product")
    fun Delete(@Header("lang") lang: String,
             @Header("Authorization") Authorization:String,
             @Query("product_id") product_id:Int,
             @Query("type") type:String):Call<TermsResponse>

    @Multipart
    @POST("add-service")
    fun AddService(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("subcategory_id") subcategory_id:Int,
                   @Query("name_ar") name_ar:String,
                   @Query("name_en") name_en:String,
                    @Query("desc_ar") desc_ar:String,
                   @Query("desc_en") desc_en:String,
                   @Query("price") price:String,
                   @Query("discount") discount:String?,
                   @Query("times") times:String,
                   @Query("free_reservation") free_reservation:Int,
                   @Query("total_discount") total_discount:String,
                   @Query("part_amount") part_amount:String?,
                   @Query("refundable") refundable:Int,
                   @Part image:MultipartBody.Part):Call<ProductRespopnse>

    @Multipart
    @POST("add-service")
    fun AddServices(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("subcategory_id") subcategory_id:Int,
                   @Query("name_ar") name_ar:String,
                   @Query("name_en") name_en:String,
                   @Query("desc_ar") desc_ar:String,
                   @Query("desc_en") desc_en:String,
                   @Query("price") price:String,
                   @Query("discount") discount:String?,
                   @Query("times") times:String,
                   @Query("free_reservation") free_reservation:Int,
                   @Query("total_discount") total_discount:String,
                   @Query("part_amount") part_amount:String?,
                   @Query("refundable") refundable:Int,
                   @Part image:MultipartBody.Part,
                    @Part images:ArrayList<MultipartBody.Part>):Call<ProductRespopnse>
    @POST("edit-service")
    fun EditServices(@Header("lang") lang: String,
                    @Header("Authorization") Authorization:String,
                    @Query("subcategory_id") subcategory_id:Int,
                     @Query("service_id") service_id: Int,
                    @Query("name_ar") name_ar:String,
                    @Query("name_en") name_en:String,
                    @Query("desc_ar") desc_ar:String,
                    @Query("desc_en") desc_en:String,
                    @Query("price") price:String,
                    @Query("discount") discount:String?,
                    @Query("times") times:String?,
                    @Query("free_reservation") free_reservation:Int,
                    @Query("total_discount") total_discount:String?,
                    @Query("part_amount") part_amount:String?,
                    @Query("refundable") refundable:Int):Call<ServiceDetailsResponse>
    
    @POST("details-service")
    fun getService(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("service_id") service_id:Int):Call<ServiceDetailsResponse>
    @Multipart
    @POST("edit-service")
    fun editServiceImages(@Header("lang") lang: String,
                          @Header("Authorization") Authorization:String,
                          @Query("service_id") service_id: Int,
                          @Part images:ArrayList<MultipartBody.Part>):Call<ServiceDetailsResponse>
    @Multipart
    @POST("edit-service")
    fun editServiceImage(@Header("lang") lang: String,
                         @Header("Authorization") Authorization:String,
                         @Query("service_id") service_id: Int,
                         @Part image:MultipartBody.Part):Call<ServiceDetailsResponse>

    @POST("delete-service-time")
    fun DeleteTime(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("time_id") time_id:Int):Call<TermsResponse>

    @POST("edit-delete-image")
    fun DeleteImage(@Header("lang") lang: String,
                    @Header("Authorization") Authorization:String,
                    @Query("image_id") image_id:Int,
                    @Query("type") type:String):Call<TermsResponse>

    @POST("provider-setting")
    fun Setting(@Header("lang") lang: String,
                @Header("Authorization") Authorization:String,
                @Query("closed") closed:Int?,
                @Query("maintenance") maintenance:Int?):Call<SettingResponse>

    @POST("check-subcategories")
    fun SubCategories(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String,
                      @Query("subcategories") subcategories:String?):Call<SubCategoryResponse>

    @POST("packages")
    fun Packages(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String):Call<PackagesResponse>

    @POST("advertising-packages")
    fun Advertises(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String):Call<PackagesResponse>

    @POST("payment-advertising")
    fun Pay(@Header("lang") lang: String,
            @Header("Authorization") Authorization:String,
            @Query("package_id") package_id:Int):Call<PayModel>

    @POST("payment-package")
    fun PackagePay(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("package_id") package_id:Int):Call<TermsResponse>

    @Multipart
    @POST("add-advertising")
    fun AddImage(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String,
                 @Query("advertising_id") advertising_id:Int,
                 @Query("subcategory_id") subcategory_id:Int,
                 @Part image:MultipartBody.Part):Call<TermsResponse>

    @POST("send-message")
    fun Send(@Header("lang") lang: String,
             @Header("Authorization") Authorization:String,
             @Query("receiver_id") receiver_id:Int,
             @Query("conversation_id") conversation_id:Int,
             @Query("type") type:String,
             @Query("message") message:String):Call<SendChatResponse>

    @Multipart
    @POST("send-message")
    fun SendImage(@Header("lang") lang: String,
                  @Header("Authorization") Authorization:String,
                  @Query("receiver_id") receiver_id:Int,
                  @Query("conversation_id") conversation_id:Int,
                  @Query("type") type:String,
                  @Part message: MultipartBody.Part):Call<SendChatResponse>

    @POST("conversation")
    fun Conversation(@Query("lang") lang: String,
                     @Query("conversation_id") conversation_id:Int,
                     @Header("Authorization") Authorization:String,
                     @Query("page") page:Int,
                     @Query("app_type") app_type:String):Call<ChatResponse>
    @FormUrlEncoded
    @POST("conversation-id")
    fun Conversation(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String,
                     @Field("receiver_id") receiver_id:Int):Call<ConversationIdResponse>
    @POST("my-conversations")
    fun Conversations(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String):Call<ChatsResponse>

    @POST("orders")
    fun Orders( @Header("Authorization") Authorization:String,
                @Header("lang") lang: String,
               @Query("type") type: String,
               @Query("status") status:String):Call<ReservationResponse>

    @POST("details-order")
    fun  ReservationDetails(@Header("Authorization") Authorization:String,
                            @Header("lang") lang:String,
                            @Query("type") type:String,
                            @Query("order_id") order_id:Int,
                            @Query("action") status:String?,
                            @Query("cancel_id") cancel_id:Int?):Call<ReservationDetailsResponse>
    @POST("details-order")
    fun  OrderDetails(@Header("Authorization") Authorization:String,
                            @Header("lang") lang:String,
                            @Query("type") type:String,
                            @Query("order_id") order_id:Int,
                            @Query("action") status:String?,
                            @Query("cancel_id") cancel_id:Int?):Call<OrderDetailsResponse>
    @POST("add-rate")
    fun AddRate(@Header("Authorization") Authorization:String,
                @Header("lang") lang:String,
                @Query("order_id") order_id:Int,
                @Query("type") type: String,
                @Query("rate") rate: Int,
                @Query("comment") comment:String?,
                @Query("order_type") order_type:String):Call<TermsResponse>
    @POST("cancellations")
    fun Cancellation(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String):Call<CancelResponse>
    @POST("user-comments")
    fun Comments(@Header("Authorization") Authorization:String,@Header("lang") lang:String,
                 @Query("order_id") order_id: Int,
                @Query("type") type:String):Call<CommentsResponse>
    
}