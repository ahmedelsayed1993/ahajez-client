package com.aait.ahjezproviderapp.UI.Activities.Main

import android.content.Intent
import android.os.Handler
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient




import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.PayModel
import com.aait.ahjezproviderapp.Models.TermsResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.AppInfo.ImageAdvertiseActivity
import com.aait.ahjezproviderapp.UI.Activities.Auth.DoneActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PayOnlineActivity : ParentActivity() {

    internal var web: WebView? = null
    internal var id: Int = 0
    internal var user_id = 0

    internal lateinit var pay: String

    protected override val layoutResource: Int
        get() = R.layout.activity_online



    protected override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        user_id = intent.getIntExtra("user",0)
        pay = getIntent().getStringExtra("type")!!
        web = findViewById(R.id.web)
        web!!.settings.javaScriptEnabled = true
        Log.e(
            "link",
            "https://aihjazwanjuz.aait-sa.com/online-payment?user=" + user_id+"&packege_id="+id+"&methods="+pay
        )
        web!!.loadUrl("https://aihjazwanjuz.aait-sa.com/online-payment?user=" + user_id+"&package_id="+id+"&methods="+pay)
        Log.e("url", web!!.url!!)
        web!!.webViewClient = MyWebVew()

        if (web!!.url!!.contains("https://aihjazwanjuz.aait-sa.com/payment-success")) {
            Pay()

        }else if (web!!.url!!.contains("https://aihjazwanjuz.aait-sa.com/payment-fail")){

            onBackPressed()
            finish()
           // CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
        }
    }

    protected fun hideInputType(): Boolean {
        return false
    }

    inner class MyWebVew : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: String): Boolean {
            Log.e("link",request)
            if (request.contains("https://aihjazwanjuz.aait-sa.com/payment-fail")) {
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        finish()
                      //  CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            } else if (request.contains("https://aihjazwanjuz.aait-sa.com/payment-success")) {


                Pay()

            }else{
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        finish()
                        //CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            }
            return true
        }
    }

    fun Pay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Pay(lang.appLanguage,"Bearer"+user.userData.token,id)?.enqueue(object :
            Callback<PayModel> {
            override fun onFailure(call: Call<PayModel>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<PayModel>, response: Response<PayModel>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        val intent = Intent(this@PayOnlineActivity, ImageAdvertiseActivity::class.java)
                        intent.putExtra("id",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else
                    {
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}
