package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.SubCategoryModel
import com.aait.ahjezproviderapp.Models.SubCategoryResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.UI.Controllers.CategoriesAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubCategoriesActivity :ParentActivity(),OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_subcategory

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var subCategories:RecyclerView
    lateinit var save:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var categoriesAdapter: CategoriesAdapter
    var categories = ArrayList<SubCategoryModel>()
    var cats = ArrayList<SubCategoryModel>()
    var cat = ArrayList<Int>()
    override fun initializeComponents() {
        cat.clear()
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        subCategories = findViewById(R.id.subcategories)
        save = findViewById(R.id.save)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Services_provided)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        categoriesAdapter = CategoriesAdapter(mContext,categories,R.layout.recycler_subcategories)
        categoriesAdapter.setOnItemClickListener(this)
        subCategories.layoutManager = linearLayoutManager
        subCategories.adapter = categoriesAdapter
        getData()
        save.setOnClickListener {
            cat.clear()
            for (i in 0..categoriesAdapter.data.size-1){
                if (categoriesAdapter.data.get(i).checked==1){
                    cat.add(categoriesAdapter.data.get(i).id!!)
                }else{

                }
            }
            Log.e("ids",cat.joinToString(separator = ",",postfix = "",prefix = ""))
            if (cat.isEmpty()){
                CommonUtil.makeToast(mContext,getString(R.string.Determine_the_type_of_service_provided))
            }else{
                getData(cat.joinToString(separator = ",",postfix = "",prefix = ""))
            }

        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCategories(lang.appLanguage,"Bearer"+user.userData.token,null)
                ?.enqueue(object : Callback<SubCategoryResponse> {
                    override fun onFailure(call: Call<SubCategoryResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<SubCategoryResponse>, response: Response<SubCategoryResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                cats = response.body()?.data!!
                                categoriesAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun getData(cat:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCategories(lang.appLanguage,"Bearer"+user.userData.token,cat)
                ?.enqueue(object : Callback<SubCategoryResponse> {
                    override fun onFailure(call: Call<SubCategoryResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<SubCategoryResponse>, response: Response<SubCategoryResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                onBackPressed()
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.cat){
            if (categories.get(position).checked==1){
                categories.get(position).checked = 0
                categoriesAdapter.notifyDataSetChanged()
            }else{
                categories.get(position).checked = 1
                categoriesAdapter.notifyDataSetChanged()
            }
        }
    }
}