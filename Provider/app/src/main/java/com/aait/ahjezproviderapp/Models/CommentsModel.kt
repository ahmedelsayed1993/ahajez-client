package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class CommentsModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var rate:String?=null
    var username:String?=null
    var comment:String?=null
    var created:String?=null
}