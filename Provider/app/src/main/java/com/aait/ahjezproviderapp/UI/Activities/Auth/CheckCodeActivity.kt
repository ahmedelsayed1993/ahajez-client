package com.aait.ahjezproviderapp.UI.Activities.Auth

import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.UserModel
import com.aait.ahjezproviderapp.R

class CheckCodeActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_check_code
    lateinit var confirm: Button
    lateinit var email:TextView
    lateinit var userModel: UserModel
    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        confirm = findViewById(R.id.confirm)
        email = findViewById(R.id.email)
        email.text = userModel.email
        confirm.setOnClickListener { onBackPressed()
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}