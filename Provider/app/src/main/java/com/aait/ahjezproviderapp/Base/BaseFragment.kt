package com.aait.ahjezproviderapp.Base

import android.app.Application
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

//import com.aait.coffee.Cart.AddCartViewModel


import com.aait.ahjezproviderapp.Utils.DialogUtil


import com.aait.ahjezproviderapp.Pereferences.LanguagePrefManager
import com.aait.ahjezproviderapp.Pereferences.SharedPrefManager
import com.aait.ahjezproviderapp.Pereferences.ThemePrefManager


/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */
abstract class BaseFragment : Fragment() {

   lateinit var user: SharedPrefManager

    lateinit var lang: LanguagePrefManager

    protected var mContext: Context? = null

    var mSavedInstanceState: Bundle? = null
    lateinit var theme : ThemePrefManager

    private var mProgressDialog: ProgressDialog? = null


    protected abstract val layoutResource: Int



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(layoutResource, container, false)
        mContext = activity


        user = SharedPrefManager(mContext!!)
        lang = LanguagePrefManager(mContext!!)
        theme = ThemePrefManager(mContext!!)
        this.mSavedInstanceState = savedInstanceState


        initializeComponents(view)
        return view
    }

    protected abstract fun initializeComponents(view: View)

    /**
     * it the current activity is a recycle
     */

    protected fun showProgressDialog(message: String) {
        mProgressDialog =  DialogUtil.showProgressDialog(mContext!!, message, false)
    }

    protected fun hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog!!.dismiss()
        }
    }


}
