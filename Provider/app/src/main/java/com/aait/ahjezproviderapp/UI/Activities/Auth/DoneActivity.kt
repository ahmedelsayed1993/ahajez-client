package com.aait.ahjezproviderapp.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R

class DoneActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_done
lateinit var confirm:Button
    override fun initializeComponents() {
        confirm = findViewById(R.id.confirm)
        confirm.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java))
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,LoginActivity::class.java))
        finish()
    }
}