package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class DeliveryModel:Serializable {
    var delivery_price:String?=null
    var maximum_distance:String?=null
    var different_distance:String?=null
    var check_different_distance:String?=null
    var delivery_service:Int?=null
}