package com.aait.ahjezproviderapp.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.BaseResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_change_password
      lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var password:EditText
    lateinit var view: ImageView
    lateinit var new_pass:EditText
    lateinit var view1:ImageView
    lateinit var confirm_new:EditText
    lateinit var view2:ImageView
    lateinit var save:Button
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        new_pass = findViewById(R.id.new_pass)
        view1 = findViewById(R.id.view1)
        confirm_new = findViewById(R.id.confirm_new)
        view2 = findViewById(R.id.view2)
        save = findViewById(R.id.save)
        back.setOnClickListener { onBackPressed()
        finish()}
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        title.text = getString(R.string.change_password)
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        view1.setOnClickListener { if (new_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            new_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            new_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        view2.setOnClickListener { if (confirm_new?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            confirm_new?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_new?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }

        save.setOnClickListener { if ( CommonUtil.checkEditError(password,getString(R.string.enter_old_password))||
            CommonUtil.checkLength(password,getString(R.string.password_length),6)||
            CommonUtil.checkEditError(new_pass,getString(R.string.enter_new_pass))||
                CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
            CommonUtil.checkEditError(confirm_new,getString(R.string.enter_confirm_new)) ) {
            return@setOnClickListener
        }else{
            if (!new_pass.text.toString().equals(confirm_new.text.toString())){
                confirm_new.error = getString(R.string.password_not_match)
            }else{
                NewPassword()
            }
        }
        }

    }
    fun NewPassword(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.resetPassword(lang.appLanguage,"Bearer "+user.userData.token,password.text.toString(),new_pass.text.toString())?.enqueue(
            object : Callback<BaseResponse> {
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            onBackPressed()
                            finish()

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)

                        }
                    }
                }
            }
        )

    }
}