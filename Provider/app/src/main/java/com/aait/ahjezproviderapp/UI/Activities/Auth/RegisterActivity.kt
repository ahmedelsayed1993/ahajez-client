package com.aait.ahjezproviderapp.UI.Activities.Auth

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.Models.ListResponse
import com.aait.ahjezproviderapp.Models.UserResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.AppInfo.TermsActivity
import com.aait.ahjezproviderapp.UI.Activities.LocationActivity
import com.aait.ahjezproviderapp.UI.Controllers.ListAdapter
import com.aait.ahjezproviderapp.UI.Controllers.SubsAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.aait.ahjezproviderapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging

import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class RegisterActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_register

    lateinit var commercial_lay:LinearLayout
    lateinit var commercial_text:TextView
    lateinit var commercial_image:ImageView
    lateinit var lay1:LinearLayout
    lateinit var provider_type:TextView
    lateinit var provider_types:RecyclerView
    lateinit var legal_name:EditText
    lateinit var commercial_number:EditText
    lateinit var commercial:TextView
    lateinit var maurof:TextView
    lateinit var branch_number:EditText
    lateinit var delivery:TextView
    lateinit var information_lay:LinearLayout
    lateinit var information_text:TextView
    lateinit var information_image:ImageView
    lateinit var lay2:LinearLayout
    lateinit var image:CircleImageView
    lateinit var name_ar:EditText
    lateinit var name_en:EditText
    lateinit var description_ar:EditText
    lateinit var description_en:EditText
    lateinit var category:TextView
    lateinit var cats:RecyclerView
    lateinit var type:TextView
    lateinit var place_lay:LinearLayout
    lateinit var place_text:TextView
    lateinit var place_image:ImageView
    lateinit var lay3:LinearLayout
    lateinit var region:TextView
    lateinit var city:TextView
    lateinit var neighbor:TextView
    lateinit var street:EditText
    lateinit var location:TextView
    lateinit var bank_lay:LinearLayout
    lateinit var bank_text:TextView
    lateinit var bank_image:ImageView
    lateinit var lay4:LinearLayout
    lateinit var bank_name:EditText
    lateinit var benefit_name:EditText
    lateinit var iban:EditText
    lateinit var contact_lay:LinearLayout
    lateinit var contact_text:TextView
    lateinit var contact_image:ImageView
    lateinit var lay5:LinearLayout
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var confirm_password:EditText
    lateinit var view1:ImageView
    lateinit var answer:EditText
    lateinit var check:CheckBox
    lateinit var terms:TextView
    lateinit var register:Button
    lateinit var login:LinearLayout
    lateinit var regions:RecyclerView
    lateinit var cities:RecyclerView
    lateinit var neighbors:RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var listAdapter:ListAdapter
    var  types = ArrayList<ListModel>()
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var listAdapter1:ListAdapter
    var  Categories = ArrayList<ListModel>()
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var listAdapter2:ListAdapter
    var  Regions = ArrayList<ListModel>()
    lateinit var linearLayoutManager3: LinearLayoutManager
    lateinit var listAdapter3:ListAdapter
    var  Cities = ArrayList<ListModel>()
    lateinit var linearLayoutManager4: LinearLayoutManager
    lateinit var listAdapter4:ListAdapter
    var  Neighboors = ArrayList<ListModel>()
    lateinit var linearLayoutManager5: LinearLayoutManager
    lateinit var subsAdapter: SubsAdapter
    var  Subs = ArrayList<ListModel>()
    lateinit var delivery_lay:LinearLayout
    lateinit var yes:TextView
    lateinit var no:TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected = 0
    lateinit var list1:ListModel
    lateinit var list2:ListModel
    lateinit var list3:ListModel
    lateinit var list4:ListModel
    lateinit var list5:ListModel
    //lateinit var list6:ListModel
    var neg:Int?=null
    var ids = ArrayList<Int>()
    var tes = ArrayList<String>()
    var id = ""
    var lat = ""
    var lng = ""
    var result = ""
    var deliver = ""
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    var deviceID = ""
    override fun initializeComponents() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })
        commercial_lay = findViewById(R.id.commercial_lay)
        commercial_text = findViewById(R.id.commercial_text)
        commercial_image = findViewById(R.id.commercial_image)
        lay1 = findViewById(R.id.lay1)
        provider_type = findViewById(R.id.provider_type)
        provider_types = findViewById(R.id.provider_types)
        legal_name = findViewById(R.id.legal_name)
        commercial_number = findViewById(R.id.commercial_number)
        commercial = findViewById(R.id.commercial)
        maurof = findViewById(R.id.maurof)
        branch_number = findViewById(R.id.branch_number)
        delivery = findViewById(R.id.delivery)
        delivery_lay = findViewById(R.id.delivery_lay)
        yes = findViewById(R.id.yes)
        no = findViewById(R.id.no)
        information_lay = findViewById(R.id.information_lay)
        information_text = findViewById(R.id.information_text)
        information_image = findViewById(R.id.information_image)
        lay2 = findViewById(R.id.lay2)
        image = findViewById(R.id.image)
        name_ar = findViewById(R.id.name_ar)
        name_en = findViewById(R.id.name_en)
        description_ar = findViewById(R.id.description_ar)
        description_en = findViewById(R.id.description_en)
        category = findViewById(R.id.category)
        cats = findViewById(R.id.cats)
        type = findViewById(R.id.type)
        place_lay = findViewById(R.id.place_lay)
        place_text = findViewById(R.id.place_text)
        place_image = findViewById(R.id.place_image)
        lay3 = findViewById(R.id.lay3)
        region = findViewById(R.id.region)
        city = findViewById(R.id.city)
        neighbor = findViewById(R.id.neighbor)
        street = findViewById(R.id.street)
        location = findViewById(R.id.location)
        bank_lay = findViewById(R.id.bank_lay)
        bank_text = findViewById(R.id.bank_text)
        bank_image = findViewById(R.id.bank_image)
        lay4 = findViewById(R.id.lay4)
        bank_name = findViewById(R.id.bank_name)
        benefit_name = findViewById(R.id.benefit_name)
        iban = findViewById(R.id.iban)
        contact_lay = findViewById(R.id.contact_lay)
        contact_text = findViewById(R.id.contact_text)
        contact_image = findViewById(R.id.contact_image)
        lay5 = findViewById(R.id.lay5)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        confirm_password = findViewById(R.id.confirm_password)
        view1 = findViewById(R.id.view1)
        answer = findViewById(R.id.answer)
        check = findViewById(R.id.check)
        terms = findViewById(R.id.terms)
        register = findViewById(R.id.register)
        login = findViewById(R.id.login)
        regions = findViewById(R.id.regions)
        cities = findViewById(R.id.cities)
        neighbors = findViewById(R.id.neighbors)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = ListAdapter(mContext,types,R.layout.recycle_list)
        listAdapter.setOnItemClickListener(this)
        provider_types.layoutManager = linearLayoutManager
        provider_types.adapter = listAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter1 = ListAdapter(mContext,Categories,R.layout.recycle_list)
        listAdapter1.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager1
        cats.adapter = listAdapter1
        linearLayoutManager2 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter2 = ListAdapter(mContext,Regions,R.layout.recycle_list)
        listAdapter2.setOnItemClickListener(this)
        regions.layoutManager = linearLayoutManager2
        regions.adapter = listAdapter2
        linearLayoutManager3 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter3 = ListAdapter(mContext,Cities,R.layout.recycle_list)
        listAdapter3.setOnItemClickListener(this)
        cities.layoutManager = linearLayoutManager3
        cities.adapter = listAdapter3
        linearLayoutManager4 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter4 = ListAdapter(mContext,Neighboors,R.layout.recycle_list)
        listAdapter4.setOnItemClickListener(this)
        neighbors.layoutManager = linearLayoutManager4
        neighbors.adapter = listAdapter4
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java))
        finish()}
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        delivery.setOnClickListener {
            if (delivery_lay.visibility==View.GONE){
                delivery_lay.visibility = View.VISIBLE
            }else{
                delivery_lay.visibility = View.GONE
            }
        }
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        view1.setOnClickListener {
            if (confirm_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            confirm_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        yes.setOnClickListener {
            delivery.text = getString(R.string.yes)
            delivery_lay.visibility = View.GONE
            deliver = "yes"
        }
        no.setOnClickListener {
            delivery.text = getString(R.string.no)
            delivery_lay.visibility = View.GONE
            deliver = "no"
        }
        commercial_lay.setOnClickListener {
            if (lay1.visibility == View.GONE){
               commercial_lay.background = mContext.resources.getDrawable(R.color.colorGreen)
                commercial_image.setImageResource(R.mipmap.top)
                commercial_text.textColor = mContext.resources.getColor(R.color.colorWhite)
                information_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                information_image.setImageResource(R.mipmap.arrowdown)
                information_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                place_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                place_image.setImageResource(R.mipmap.arrowdown)
                place_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                bank_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                bank_image.setImageResource(R.mipmap.arrowdown)
                bank_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                contact_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                contact_image.setImageResource(R.mipmap.arrowdown)
                contact_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay1.visibility = View.VISIBLE
                lay2.visibility = View.GONE
                lay3.visibility = View.GONE
                lay4.visibility = View.GONE
                lay5.visibility = View.GONE
            }else{
                commercial_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                commercial_image.setImageResource(R.mipmap.arrowdown)
                commercial_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay1.visibility = View.GONE
            }
        }
        information_lay.setOnClickListener {
            if (lay2.visibility == View.GONE){
                information_lay.background = mContext.resources.getDrawable(R.color.colorGreen)
                information_image.setImageResource(R.mipmap.top)
                information_text.textColor = mContext.resources.getColor(R.color.colorWhite)
                commercial_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                commercial_image.setImageResource(R.mipmap.arrowdown)
                commercial_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                place_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                place_image.setImageResource(R.mipmap.arrowdown)
                place_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                bank_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                bank_image.setImageResource(R.mipmap.arrowdown)
                bank_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                contact_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                contact_image.setImageResource(R.mipmap.arrowdown)
                contact_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay1.visibility = View.GONE
                lay2.visibility = View.VISIBLE
                lay3.visibility = View.GONE
                lay4.visibility = View.GONE
                lay5.visibility = View.GONE
            }else{
                information_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                information_image.setImageResource(R.mipmap.arrowdown)
                information_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay2.visibility = View.GONE
            }
        }
        place_lay.setOnClickListener {
            if (lay3.visibility == View.GONE){
                place_lay.background = mContext.resources.getDrawable(R.color.colorGreen)
                place_image.setImageResource(R.mipmap.top)
                place_text.textColor = mContext.resources.getColor(R.color.colorWhite)
                information_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                information_image.setImageResource(R.mipmap.arrowdown)
                information_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                commercial_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                commercial_image.setImageResource(R.mipmap.arrowdown)
                commercial_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                bank_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                bank_image.setImageResource(R.mipmap.arrowdown)
                bank_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                contact_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                contact_image.setImageResource(R.mipmap.arrowdown)
                contact_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay1.visibility = View.GONE
                lay2.visibility = View.GONE
                lay3.visibility = View.VISIBLE
                lay4.visibility = View.GONE
                lay5.visibility = View.GONE
            }else{
                place_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                place_image.setImageResource(R.mipmap.arrowdown)
                place_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay3.visibility = View.GONE
            }
        }
        bank_lay.setOnClickListener {
            if (lay4.visibility == View.GONE){
                bank_lay.background = mContext.resources.getDrawable(R.color.colorGreen)
                bank_image.setImageResource(R.mipmap.top)
                bank_text.textColor = mContext.resources.getColor(R.color.colorWhite)
                information_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                information_image.setImageResource(R.mipmap.arrowdown)
                information_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                place_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                place_image.setImageResource(R.mipmap.arrowdown)
                place_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                commercial_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                commercial_image.setImageResource(R.mipmap.arrowdown)
                commercial_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                contact_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                contact_image.setImageResource(R.mipmap.arrowdown)
                contact_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay1.visibility = View.GONE
                lay2.visibility = View.GONE
                lay3.visibility = View.GONE
                lay4.visibility = View.VISIBLE
                lay5.visibility = View.GONE
            }else{
                bank_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                bank_image.setImageResource(R.mipmap.arrowdown)
                bank_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay4.visibility = View.GONE
            }
        }
        contact_lay.setOnClickListener {
            if (lay5.visibility == View.GONE){
                contact_lay.background = mContext.resources.getDrawable(R.color.colorGreen)
                contact_image.setImageResource(R.mipmap.top)
                contact_text.textColor = mContext.resources.getColor(R.color.colorWhite)
                information_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                information_image.setImageResource(R.mipmap.arrowdown)
                information_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                place_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                place_image.setImageResource(R.mipmap.arrowdown)
                place_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                bank_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                bank_image.setImageResource(R.mipmap.arrowdown)
                bank_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                commercial_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                commercial_image.setImageResource(R.mipmap.arrowdown)
                commercial_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay1.visibility = View.GONE
                lay2.visibility = View.GONE
                lay3.visibility = View.GONE
                lay4.visibility = View.GONE
                lay5.visibility = View.VISIBLE
            }else{
                contact_lay.background = mContext.resources.getDrawable(R.drawable.shape_gray)
                contact_image.setImageResource(R.mipmap.arrowdown)
                contact_text.textColor = mContext.resources.getColor(R.color.colorPrimaryDark)
                lay5.visibility = View.GONE
            }
        }
        maurof.setOnClickListener { startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://maroof.sa"))) }
        commercial.setOnClickListener { startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://ecr.mci.gov.sa/Land?returnUrl=%2F"))) }
        provider_type.setOnClickListener {
            selected = 0
            getProviders()
        }
        location.setOnClickListener { startActivityForResult(Intent(this, LocationActivity::class.java),1)  }
        category.setOnClickListener {
            selected = 1
            getCategories()
        }
        type.setOnClickListener {
            if (category.text.toString().equals("")){
                category.error = getString(R.string.Please_enter_the_service_category_provided)
            }else{
                val dialog = Dialog(mContext)
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                dialog ?.setCancelable(true)
                dialog ?.setContentView(R.layout.dialog_subs)
                val subs = dialog?.findViewById<RecyclerView>(R.id.subs)
                val confirm = dialog?.findViewById<Button>(R.id.confirm)
                linearLayoutManager5 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
                subsAdapter = SubsAdapter(mContext,Subs,R.layout.recycle_subs)
                subsAdapter.setOnItemClickListener(this)
                subs.layoutManager = linearLayoutManager5
                subs.adapter = subsAdapter
                getSubs()
                confirm.setOnClickListener {
                    if (ids.isEmpty()){
                        CommonUtil.makeToast(mContext,getString(R.string.Please_enter_the_type_of_service_provided))
                    }else{
                        dialog?.dismiss()
                    }
                }

                dialog?.show()
            }
        }
        region.setOnClickListener {
            selected = 2
            getRegions()
        }
        city.setOnClickListener {
            if (region.text.toString().equals("")){
                region.error = getString(R.string.Please_select_a_region)
            }else {
                selected = 3
                getCities()
            }
        }
        neighbor.setOnClickListener {
             if (city.text.toString().equals("")){
                city.error = getString(R.string.Please_select_a_city)
            }else {
                 selected = 4
                 getNeighboors()
             }
        }
        register.setOnClickListener {
            commercial_lay.visibility = View.VISIBLE
            information_lay.visibility = View.VISIBLE
            place_lay.visibility = View.VISIBLE
            bank_lay.visibility = View.VISIBLE
            contact_lay.visibility = View.VISIBLE
            if (CommonUtil.checkTextError(provider_type,getString(R.string.Please_enter_the_type_of_service_provider))||
                    CommonUtil.checkEditError(legal_name,getString(R.string.Please_enter_a_legal_name))||
                    CommonUtil.checkEditError(commercial_number,getString(R.string.Please_enter_the_commercial_registry_number_known_certificate))||
                    CommonUtil.checkLength(commercial_number,getString(R.string.commercial),5)||
                    CommonUtil.checkTextError(delivery,getString(R.string.Please_enter_an_answer))||
                    CommonUtil.checkEditError(name_ar,getString(R.string.Please_enter_the_trade_name_in_Arabic))||
                    CommonUtil.checkEditError(description_ar,getString(R.string.Please_enter_the_description_in_Arabic))||
                    CommonUtil.checkTextError(category,getString(R.string.Please_enter_the_service_category_provided))||
                    CommonUtil.checkTextError(type,getString(R.string.Please_enter_the_type_of_service_provided))||
                    CommonUtil.checkTextError(region,getString(R.string.Please_select_a_region))||
                    CommonUtil.checkTextError(city,getString(R.string.Please_select_a_city))||
                    CommonUtil.checkTextError(location,getString(R.string.Please_select_a_location_on_the_map))||
                    CommonUtil.checkEditError(bank_name,getString(R.string.Please_enter_the_name_of_the_bank))||
                    CommonUtil.checkEditError(benefit_name,getString(R.string.Please_enter_the_name_of_the_beneficiary))||
                    CommonUtil.checkEditError(iban,getString(R.string.Please_enter_the_IBAN_number))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(password,getString(R.string.enter_password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_password,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                if (!email.text.toString().equals("")){
                    if (!CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
                        return@setOnClickListener
                    }else{
                        if (!password.text.toString().equals(confirm_password.text.toString())) {
                            CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                        }else{
                            if (!check.isChecked){
                                CommonUtil.makeToast(mContext,getString(R.string.agree_terms))
                            }else{
                                if (ImageBasePath!=null){
                                    Sign(email.text.toString(),ImageBasePath!!)
                                }else{
                                    Sign(email.text.toString())
                                }


                            }

                        }

                    }
                }else{
                    if (!password.text.toString().equals(confirm_password.text.toString())) {
                        CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                    }else{
                        if (!check.isChecked){
                            CommonUtil.makeToast(mContext,getString(R.string.agree_terms))
                        }else{
                            if (ImageBasePath!=null){
                                Sign(null,ImageBasePath!!)
                            }else{
                                Sign(null)
                            }


                        }

                    }
                }
            }
        }

    }

    fun getProviders(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProviderTypes(lang.appLanguage)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                provider_types.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getCategories(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Categories(lang.appLanguage)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                cats.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter1.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getRegions(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Regions(lang.appLanguage)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                regions.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter2.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cities(lang.appLanguage,list3.id!!)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                cities.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter3.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getSubs(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCategories(lang.appLanguage,list2.id!!)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()

                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        subsAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getNeighboors(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Neighborhoods(lang.appLanguage,list4.id!!)?.enqueue(object : Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                neighbors.visibility = View.VISIBLE
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data!!.isEmpty()){
                            CommonUtil.makeToast(mContext,getString(R.string.no_neighborhood))
                        }else {
                            listAdapter4.updateAll(response.body()?.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name) {
            if (selected == 0) {
                provider_types.visibility = View.GONE
                list1 = types.get(position)
                provider_type.text = list1.name
            } else if (selected == 1) {
                cats.visibility = View.GONE
                list2 = Categories.get(position)
                category.text = list2.name
            } else if (selected == 2) {
                regions.visibility = View.GONE
                list3 = Regions.get(position)
                region.text = list3.name
            } else if (selected == 3) {
                cities.visibility = View.GONE
                list4 = Cities.get(position)
                city.text = list4.name
            } else if (selected == 4) {
                neighbors.visibility = View.GONE
                list5 = Neighboors.get(position)
                neighbor.text = list5.name
                neg = list5.id
            }
        }else if (view.id == R.id.text){
            subsAdapter.notifyDataSetChanged()
            if (Subs.get(position).checked!!){
                ids.add(Subs.get(position).id!!)
                tes.add(Subs.get(position).name!!)
            }else{
                ids.remove(Subs.get(position).id!!)
                tes.remove(Subs.get(position).name!!)
            }

//            if (ids.isEmpty()){
//                ids.add(Subs.get(position).id!!)
//                tes.add(Subs.get(position).name!!)
//            }else{
//                    for (i in 0..ids.size - 1) {
//                        if (ids.size==1){
//                            if (ids.get(0)==Subs.get(position).id){
//                                ids.remove(Subs.get(position).id!!)
//                                tes.remove(Subs.get(position).name!!)
//                            }else{
//                                ids.add(Subs.get(position).id!!)
//                                tes.add(Subs.get(position).name!!)
//                            }
//                        }
//                       else {
//                            if (tes.get(i).equals(Subs.get(position).name!!)) {
//                                ids.remove(Subs.get(position).id!!)
//                                tes.remove(Subs.get(position).name!!)
//                            } else {
//                                ids.add(Subs.get(position).id!!)
//                                tes.add(Subs.get(position).name!!)
//                            }
//                        }
//                    }
//
//            }
            Log.e("ids",Gson().toJson(ids))
            Log.e("subs",Gson().toJson(tes))
            type.text = tes.joinToString(prefix = "",postfix = "",separator = ",")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                location.text = ""
            }
        }else{
            if (requestCode == 100) {
                if (resultCode == 0) {

                } else {
                    returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                    ImageBasePath = returnValue!![0]

                    Glide.with(mContext).load(ImageBasePath).into(image)

                    if (ImageBasePath != null) {
                        //upLoad(ImageBasePath!!)
                        //ID.text = getString(R.string.Image_attached)
                    }
                }
            }
        }
    }

    fun Sign(email:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Sign_upwithout(lang.appLanguage,list1.id!!,legal_name.text.toString()
        ,commercial_number.text.toString(),branch_number.text.toString(),deliver,name_ar.text.toString(),name_en.text.toString()
        ,description_ar.text.toString(),description_en.text.toString(),list2.id!!,ids.joinToString(prefix = "",postfix = "",separator = ",")
        ,list3.id!!,list4.id!!,neg,street.text.toString(),lat,lng,result,bank_name.text.toString(),benefit_name.text.toString()
        ,iban.text.toString(),phone.text.toString(),email,password.text.toString(),answer.text.toString(),deviceID,"android")
                ?.enqueue(object :Callback<UserResponse>{
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if(response.body()?.value.equals("1")){
                                val intent = Intent(this@RegisterActivity,ActivationCodeActivity::class.java)
                                intent.putExtra("user",response.body()?.data)
                                startActivity(intent)
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    fun Sign(email:String?,path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.Sign_up(lang.appLanguage,list1.id!!,legal_name.text.toString()
                ,commercial_number.text.toString(),branch_number.text.toString(),deliver,filePart,name_ar.text.toString(),name_en.text.toString()
                ,description_ar.text.toString(),description_en.text.toString(),list2.id!!,ids.joinToString(prefix = "",postfix = "",separator = ",")
                ,list3.id!!,list4.id!!,neg,street.text.toString(),lat,lng,result,bank_name.text.toString(),benefit_name.text.toString()
                ,iban.text.toString(),phone.text.toString(),email,password.text.toString(),answer.text.toString(),deviceID,"android")
                ?.enqueue(object :Callback<UserResponse>{
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if(response.body()?.value.equals("1")){
                                val intent = Intent(this@RegisterActivity,ActivationCodeActivity::class.java)
                                intent.putExtra("user",response.body()?.data)
                                startActivity(intent)
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}