package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class TimesModel:Serializable {
    var time_from:String?=null
    var time_to:String?=null
    var id:Int?=null

    constructor(time_from: String?, time_to: String?) {
        this.time_from = time_from
        this.time_to = time_to
    }
}