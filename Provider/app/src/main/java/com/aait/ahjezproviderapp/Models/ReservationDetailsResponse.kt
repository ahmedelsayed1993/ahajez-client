package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class ReservationDetailsResponse:BaseResponse(),Serializable {
    var data:ReservationDetailsModel?=null
}