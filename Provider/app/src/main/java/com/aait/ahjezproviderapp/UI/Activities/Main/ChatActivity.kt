package com.aait.ahjezproviderapp.UI.Activities.Main

import android.content.*
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.*
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.ChatModel
import com.aait.ahjezproviderapp.Models.ChatResponse
import com.aait.ahjezproviderapp.Models.SendChatResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Controllers.ChatAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.aait.ahjezproviderapp.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.ArrayList

class ChatActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_chat
    var id = 0
    var receiver = 0
    lateinit var message: EditText
    lateinit var send: LinearLayout
    lateinit var rv_recycle: RecyclerView
    var mActive = false
    internal var sharedPreferences: SharedPreferences? = null
    internal lateinit var MsgReciever: BroadcastReceiver

    internal lateinit var adapter: RecyclerView.Adapter<*>
    internal var messageModels: MutableList<ChatModel> = ArrayList<ChatModel>()


    private var isLoading = false

    // If current page is the last page (Pagination will stop after this page load)
    private var isLastPage = false

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private val TOTAL_PAGES: Int = 0

    // indicates the current page which Pagination is fetching.
    private var currentPage: Int = 0

    var lastpage: Int = 0

    internal lateinit var linearLayout: LinearLayoutManager


    //boolean isLoading, isLastPage;
    internal var PAGE = 0
    internal var PAGE_NUM = 0
    lateinit var notification:ImageView
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var send_image:ImageView

    var m = ""
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        receiver = intent.getIntExtra("receiver",0)
        lastpage = intent.getIntExtra("lastpage",0)
        message = findViewById(R.id.message)
        send = findViewById(R.id.send)
        rv_recycle = findViewById(R.id.chats)
        send_image = findViewById(R.id.send_image)
        notification = findViewById(R.id.notification)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
            finish()}
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        title.text = getString(R.string.conversation_details)
        PAGE = lastpage
        PAGE_NUM = lastpage
        linearLayout = LinearLayoutManager(this)
        linearLayout.stackFromEnd = true
        // linearLayout.setReverseLayout(true)
        rv_recycle.setLayoutManager(linearLayout)
        messageModels = ArrayList()
        adapter = ChatAdapter(mContext, messageModels, receiver!!)
        rv_recycle.setAdapter(adapter)
        loadMoreItems()
        currentPage = lastpage
        rv_recycle.addOnScrollListener(recyclerViewOnScrollListener)
        MsgReciever = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                //  Toast.makeText(ChatActivity.this, "broad", Toast.LENGTH_SHORT).show();
                val chatModel1 = ChatModel()
                chatModel1.avatar = ""
                chatModel1.conversation_id = 0
                chatModel1.created = ""
                chatModel1.message = ""
                chatModel1.messageType = ""
                chatModel1.receiver_id = 0
                //chatModel1.user_id = 0
                chatModel1.id = 0

                if (intent.action!!.equals("new_message", ignoreCase = true)) {
                    //  Toast.makeText(context, "broad2", Toast.LENGTH_SHORT).show();


                    chatModel1.message=(intent.getStringExtra("msg"))
                    chatModel1.id=(Integer.parseInt(intent.getStringExtra("id")!!))
                    chatModel1.receiver_id = (Integer.parseInt(intent.getStringExtra("receiver_id")!!))
                    chatModel1.conversation_id = (Integer.parseInt(intent.getStringExtra("conversation_id")!!))
                    chatModel1.avatar = (intent.getStringExtra("avatar"))
                    chatModel1.created = (intent.getStringExtra("created"))
                    chatModel1.messageType = intent.getStringExtra("messageType")
                   // chatModel1.user_id = (Integer.parseInt(intent.getStringExtra("user_id")!!))
                    messageModels.add(chatModel1)
                    adapter = ChatAdapter(
                        mContext,
                        messageModels,
                        receiver
                    )
                    rv_recycle.setAdapter(adapter)
                    // friend_id = intent.getIntExtra("user",0);
                }
            }
        }

        send.setOnClickListener {


            SendMessage()
        }

        send_image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }


    }
    override fun onResume() {
        super.onResume()
        val editor = getSharedPreferences("home", MODE_PRIVATE).edit()

        editor.putString("sender_id", id.toString())

        editor.apply()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(MsgReciever, IntentFilter("new_message"))
    }


    override fun onStop() {
        super.onStop()
        val editor = getSharedPreferences("home", MODE_PRIVATE).edit()
        editor.putString("sender_id", "0")
        editor.apply()
    }


    private fun loadMoreItems() {
        isLoading = true
        Client.getClient()?.create(Service::class.java)?.Conversation(
            lang.appLanguage,id,
           "Bearer"+ user.userData.token!!,
            PAGE_NUM,
            "android"
        )?.enqueue(object :
            Callback<ChatResponse> {
            override fun onResponse(call: Call<ChatResponse>, response: Response<ChatResponse>) {
                isLoading = false
                if (response.isSuccessful()) {
                   // layProgress!!.setVisibility(View.GONE)
                    try {
                        if (response.body()!!.key.equals("1")) {
                           // Glide.with(mContext).asBitmap().load(response.body()?.user_data?.avatar).into(image)
                           // title.text = response.body()?.user_data?.username
                            messageModels.addAll(0, response.body()?.data!!)
                            adapter.notifyDataSetChanged()
                            if (PAGE_NUM == PAGE) {
                                rv_recycle.scrollToPosition(linearLayout.itemCount - 1)
                            } else {
                                rv_recycle.scrollToPosition(response.body()?.data?.size!! - 1 + linearLayout.childCount)
                            }
                            PAGE_NUM--
                            if (PAGE_NUM == 0) {
                                isLastPage = true
                            }

                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    CommonUtil.makeToast(mContext, getString(R.string.connection_error))
                }
            }

            override fun onFailure(call: Call<ChatResponse>, t: Throwable) {
                isLoading = false
               // layProgress!!.setVisibility(View.GONE)
                if (PAGE_NUM == PAGE) {
                   // layNoInternet!!.setVisibility(View.VISIBLE)
                } else {
                    CommonUtil.makeToast(mContext, getString(R.string.connection_error))
                }
            }
        })


    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val firstVisibleItemPosition = linearLayout.findFirstVisibleItemPosition()

            if (!isLoading && !isLastPage) {
                if (firstVisibleItemPosition < 2) {//&& totalItemCount >= 20
                   // layProgress!!.setVisibility(View.VISIBLE)
                    loadMoreItems()
                }
            }
        }
    }

    fun SendMessage(){
        if (!message.text.toString().equals("")){
            send.isEnabled = false
            Client.getClient()?.create(Service::class.java)?.Send(lang.appLanguage,"Bearer"+user.userData.token!!,receiver,id,"text",message.text.toString())?.enqueue(
                object : Callback<SendChatResponse> {
                    override fun onFailure(call: Call<SendChatResponse>, t: Throwable) {
                        Log.i("exception", "exception")
                        send.isEnabled = true
                        Log.e("errpr", t.toString())
                        CommonUtil.handleException(this@ChatActivity, t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<SendChatResponse>,
                        response: Response<SendChatResponse>
                    ) {

                        send.isEnabled = true
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){

                                val model = ChatModel()
                                model.message = response.body()?.data?.message
                                model.created =(response.body()?.data?.created)
                                model.conversation_id = response.body()?.data?.conversation_id
                                model.avatar = response.body()?.data?.avatar
                                model.id = response.body()?.data?.id
                                model.receiver_id = response.body()?.data?.receiver_id
                               // model.user_id = response.body()?.data?.user_id


                                messageModels.add(response.body()?.data!!)
                                adapter = ChatAdapter(
                                    this@ChatActivity,
                                    messageModels,
                                    receiver
                                )
                                rv_recycle.setAdapter(adapter)
                                adapter.notifyDataSetChanged()

                                message.setText("")
                            }
                        }
                    }
                }
            )
        }
    }
    fun SendMessage(path:String){
        if (ImageBasePath!=null){
            send_image.isEnabled = false
            var filePart: MultipartBody.Part? = null
            val ImageFile = File(path)
            val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
            filePart = MultipartBody.Part.createFormData("message", ImageFile.name, fileBody)
            Client.getClient()?.create(Service::class.java)?.SendImage(lang.appLanguage,"Bearer"+user.userData.token!!,receiver,id,"file",filePart)?.enqueue(
                object : Callback<SendChatResponse> {
                    override fun onFailure(call: Call<SendChatResponse>, t: Throwable) {
                        Log.i("exception", "exception")
                        send.isEnabled = true
                        Log.e("errpr", t.toString())
                        CommonUtil.handleException(this@ChatActivity, t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<SendChatResponse>,
                        response: Response<SendChatResponse>
                    ) {

                        send_image.isEnabled = true
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){

                                val model = ChatModel()
                                model.message = response.body()?.data?.message
                                model.created =(response.body()?.data?.created)
                                model.conversation_id = response.body()?.data?.conversation_id
                                model.avatar = response.body()?.data?.avatar
                                model.id = response.body()?.data?.id
                                model.receiver_id = response.body()?.data?.receiver_id
                                // model.user_id = response.body()?.data?.user_id


                                messageModels.add(response.body()?.data!!)
                                adapter = ChatAdapter(
                                    this@ChatActivity,
                                    messageModels,
                                    receiver
                                )
                                rv_recycle.setAdapter(adapter)
                                adapter.notifyDataSetChanged()

                                message.setText("")
                            }
                        }
                    }
                }
            )
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

               // Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    SendMessage(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }
}