package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class PackagesModel:Serializable {
    var id:Int?=null
    var title:String?=null
    var description:String?=null
    var price:String?=null
}