package com.aait.ahjezproviderapp.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatDelegate
import com.aait.ahjezproviderapp.Base.BaseFragment
import com.aait.ahjezproviderapp.Models.TermsResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.AppInfo.*
import com.aait.ahjezproviderapp.UI.Activities.Auth.LoginActivity
import com.aait.ahjezproviderapp.UI.Activities.SplashActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import io.fotoapparat.selector.firstAvailable
import io.fotoapparat.selector.infinity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoreFragment :BaseFragment(){
    override val layoutResource: Int
        get() = R.layout.fragment_more
    companion object {
        fun newInstance(): MoreFragment {
            val args = Bundle()
            val fragment = MoreFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var profile: LinearLayout
    lateinit var wallet: LinearLayout
    lateinit var settings: LinearLayout
    lateinit var call_us: LinearLayout
    lateinit var about: LinearLayout
    lateinit var terms: LinearLayout
    lateinit var pakages:LinearLayout
    lateinit var app_lang: LinearLayout
    lateinit var night: Switch
    lateinit var logout: LinearLayout
    lateinit var text: TextView
    lateinit var chats: LinearLayout
    lateinit var announce:LinearLayout
    override fun initializeComponents(view: View) {
        profile = view.findViewById(R.id.profile)
        chats = view.findViewById(R.id.chats)
        wallet = view.findViewById(R.id.wallet)
        settings = view.findViewById(R.id.settings)
        pakages = view.findViewById(R.id.pakages)
        announce = view.findViewById(R.id.announce)
        call_us = view.findViewById(R.id.call_us)
        about = view.findViewById(R.id.about)
        terms = view.findViewById(R.id.terms)
        app_lang = view.findViewById(R.id.app_lang)
        night = view.findViewById(R.id.night)
        logout = view.findViewById(R.id.logout)
        text = view.findViewById(R.id.text)
        if (user.loginStatus!!){
            text.text = getString(R.string.logout)
        }else{
            text.text = getString(R.string.login)
        }

        profile.setOnClickListener {
            if (user.loginStatus!!) {
              //  startActivity(Intent(activity, ProfileActivity::class.java))
            }
        }
        chats.setOnClickListener {
            if (user.loginStatus!!){
                startActivity(Intent(activity,ChatsActivity::class.java))
            }
        }
        profile.setOnClickListener { startActivity(Intent(activity,ProfileActivity::class.java)) }
        pakages.setOnClickListener { startActivity(Intent(activity,PackagesActivity::class.java)) }
        announce.setOnClickListener { startActivity(Intent(activity,AdvertisesActivity::class.java)) }
        about.setOnClickListener { startActivity(Intent(activity, AboutActivity::class.java)) }
        terms.setOnClickListener { startActivity(Intent(activity, TermsActivity::class.java)) }
        call_us.setOnClickListener { startActivity(Intent(activity, CallUsActivity::class.java))  }
        wallet.setOnClickListener {
            if (user.loginStatus!!) {
                startActivity(Intent(activity, WalletActivity::class.java))
            }
        }
        settings.setOnClickListener { startActivity(Intent(activity,SettingActivity::class.java)) }
        app_lang.setOnClickListener { startActivity(Intent(activity, AppLangActivity::class.java))  }

        logout.setOnClickListener { if (user.loginStatus!!){
            logout()}else{
            startActivity(Intent(activity, LoginActivity::class.java))
            activity?.finish()
        }
        }

        if (AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_NO){
            night.isChecked = false
        }else{
            night.isChecked = true
        }


        night.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (isChecked) {
                // theme.appTheme =
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }else{
                //  theme.appTheme =
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }
        })

//        night.setOnClickListener {
////            if (theme.appTheme = AppCompatDelegate.MODE_NIGHT_NO){
////
////                }
////                finishAffinity()
////                startActivity(Intent(this, SplashActivity::class.java)))
//
//            if (night.isChecked){
//
//                theme.appTheme = AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
//               activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
//
//            }else{
//                theme.appTheme = AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
//            }
//        }


    }

    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut("Bearer"+user.userData.token,user.userData.device_id!!,"android",user.userData.mac_address_id!!,lang.appLanguage)?.enqueue(object :
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()
                        CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity?.finish()
                    }else{
                        user.loginStatus=false
                        user.Logout()
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity?.finish()

                    }
                }
            }
        })
    }
}