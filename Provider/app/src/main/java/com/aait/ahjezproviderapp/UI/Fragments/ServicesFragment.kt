package com.aait.ahjezproviderapp.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezproviderapp.Base.BaseFragment
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.Models.MainProductsResponse
import com.aait.ahjezproviderapp.Models.ProductModel
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.AddProductActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.AddServiceActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.ProductDetailsActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.ServiceDetailsActivity
import com.aait.ahjezproviderapp.UI.Controllers.ProductAdapter
import com.aait.ahjezproviderapp.UI.Controllers.SectionsAdapter
import com.aait.ahjezproviderapp.UI.Controllers.SubCategoryAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServicesFragment:BaseFragment() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.fragment_services
    companion object {
        fun newInstance(): ServicesFragment {
            val args = Bundle()
            val fragment = ServicesFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    var expire = 0
    var msg = ""
    lateinit var add:ImageView
    lateinit var productAdapter: ProductAdapter
    lateinit var subCategoryAdapter: SubCategoryAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var cats: RecyclerView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    var productModels = ArrayList<ProductModel>()
    var subCategories = ArrayList<ListModel>()
    override fun initializeComponents(view: View) {
        add = view.findViewById(R.id.add)
        add.setOnClickListener {
            if (expire==0) {
                startActivity(Intent(activity, AddServiceActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext!!,msg)
            }
        }
        cats = view.findViewById(R.id.cats)

        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)

        linearLayoutManager = LinearLayoutManager(mContext!!,LinearLayoutManager.HORIZONTAL,false)

        linearLayoutManager2 = LinearLayoutManager(mContext!!,LinearLayoutManager.VERTICAL,false)
        productAdapter = ProductAdapter(mContext!!,productModels,R.layout.recycler_products)
        subCategoryAdapter = SubCategoryAdapter(mContext!!,subCategories,R.layout.recycle_cats)

        productAdapter.setOnItemClickListener(this)
        subCategoryAdapter.setOnItemClickListener(this)

        rv_recycle.layoutManager = linearLayoutManager2
        cats.layoutManager = linearLayoutManager

        rv_recycle.adapter = productAdapter
        cats.adapter = subCategoryAdapter

        swipeRefresh!!.setColorSchemeResources(
                R.color.colorGreen,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome(null) }
        getHome(null)
    }
    fun getHome(cat:Int?){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getServices(lang.appLanguage,"Bearer"+user.userData.token,cat)?.enqueue(object :
                Callback<MainProductsResponse> {
            override fun onResponse(call: Call<MainProductsResponse>, response: Response<MainProductsResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            expire = response.body()?.expired!!
                            msg = response.body()?.msg_expired!!
                            // Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.products?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                subCategories = response.body()?.subcategories!!

                                subCategories.add(0, ListModel(0,getString(R.string.All)))

                                productAdapter.updateAll(response.body()!!.products!!)
                                subCategoryAdapter.updateAll(subCategories)

                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<MainProductsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            Log.e("size",subCategories.size.toString())
            Log.e("pos",position.toString())
            subCategoryAdapter.selected = position

            subCategories.get(position).checked = true
            subCategoryAdapter.notifyDataSetChanged()

            if(subCategories.get(position).id==0){
                getHome(null)
            }else{
                getHome(subCategories.get(position).id!!)
            }


        }else{
            if (expire==0) {
                val intent = Intent(activity, ServiceDetailsActivity::class.java)
                intent.putExtra("id", productModels.get(position).id)
                startActivity(intent)
            }else{
                CommonUtil.makeToast(mContext!!,msg)
            }
        }
    }
}