package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class ReservationModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var created:String?=null
    var type:String?=null
}