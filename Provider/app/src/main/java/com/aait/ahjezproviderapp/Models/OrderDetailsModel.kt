package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var id:Int?=null
    var provider:Int?=null
    var status:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var phone:String?=null
    var notes:String?=null
    var preferences:String?=null
    var products:ArrayList<ProdModel>?=null
    var payment:String?=null
    var created:String?=null
    var delivery_price:String?=null
    var total:String?=null
    var total_tax:String?=null
    var tax:String?=null
    var final_total:String?=null
    var avatar:String?=null
    var cancellation:String?=null
    var name:String?=null
    var rate:Float?=null
    var chat_provider:Int?=null
    var provider_address:String?=null
    var user_rates_count:Int?=null
    var order_rate:Int?=null
    var order_comment:String?=null
}