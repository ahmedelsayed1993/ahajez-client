package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.BalanceResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WalletActivity :ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_wallet

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var balance:TextView
    lateinit var recharge:Button
    lateinit var recover:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        balance = findViewById(R.id.balance)
        recharge = findViewById(R.id.recharge)
        recover = findViewById(R.id.recover)
        back.setOnClickListener { onBackPressed()
        finish()}
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this, NotificationActivity::class.java))
            finish()}
        title.text = getString(R.string.wallet)
        getData()
        recharge.setOnClickListener { startActivity(Intent(this,BanksActivity::class.java)) }
        recover.setOnClickListener { startActivity(Intent(this,RecoverActivity::class.java)) }
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Balance("Bearer "+user.userData.token!!,lang.appLanguage)?.enqueue(object:
            Callback<BalanceResponse> {
            override fun onFailure(call: Call<BalanceResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<BalanceResponse>,
                response: Response<BalanceResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        balance.text = response.body()?.data!!.toString()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}