package com.aait.ahjezproviderapp.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.ahjezproviderapp.Base.BaseFragment
import com.aait.ahjezproviderapp.Listeners.OnItemClickListener
import com.aait.ahjezproviderapp.Models.ListModel
import com.aait.ahjezproviderapp.Models.ListResponse
import com.aait.ahjezproviderapp.Models.MainProductsResponse
import com.aait.ahjezproviderapp.Models.ProductModel
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.AddProductActivity
import com.aait.ahjezproviderapp.UI.Activities.Main.ProductDetailsActivity
import com.aait.ahjezproviderapp.UI.Controllers.ProductAdapter
import com.aait.ahjezproviderapp.UI.Controllers.SectionsAdapter
import com.aait.ahjezproviderapp.UI.Controllers.SubCategoryAdapter
import com.aait.ahjezproviderapp.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductFragments:BaseFragment(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.fragment_products
    companion object {
        fun newInstance(): ProductFragments {
            val args = Bundle()
            val fragment = ProductFragments()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var add:ImageView
    lateinit var productAdapter: ProductAdapter
    lateinit var sectionsAdapter: SectionsAdapter
    lateinit var subCategoryAdapter: SubCategoryAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var cats:RecyclerView
    lateinit var subsection:RecyclerView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    var productModels = ArrayList<ProductModel>()
    var subCategories = ArrayList<ListModel>()
    var sections = ArrayList<ListModel>()
     var listModel: ListModel?=null
    var expire = 0
    var msg = ""
    override fun initializeComponents(view: View) {
        add = view.findViewById(R.id.add)
        cats = view.findViewById(R.id.cats)
        subsection = view.findViewById(R.id.subsection)
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        add.setOnClickListener { if (expire==0) {
            startActivity(Intent(activity, AddProductActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext!!,msg)
        }
        }
        linearLayoutManager = LinearLayoutManager(mContext!!,LinearLayoutManager.HORIZONTAL,false)
        linearLayoutManager1 = LinearLayoutManager(mContext!!,LinearLayoutManager.HORIZONTAL,false)
        linearLayoutManager2 = LinearLayoutManager(mContext!!,LinearLayoutManager.VERTICAL,false)
        productAdapter = ProductAdapter(mContext!!,productModels,R.layout.recycler_products)
        subCategoryAdapter = SubCategoryAdapter(mContext!!,subCategories,R.layout.recycle_cats)
        sectionsAdapter = SectionsAdapter(mContext!!,sections,R.layout.recycle_subcategory)
        productAdapter.setOnItemClickListener(this)
        subCategoryAdapter.setOnItemClickListener(this)
        sectionsAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager2
        cats.layoutManager = linearLayoutManager
        subsection.layoutManager = linearLayoutManager1
        rv_recycle.adapter = productAdapter
        cats.adapter = subCategoryAdapter
        subsection.adapter = sectionsAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorGreen,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome(null,null) }
        getHome(null,null)
    }
    fun getHome(cat:Int?,section:Int?){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getProducts(lang.appLanguage,"Bearer"+user.userData.token,cat,section)?.enqueue(object :
                Callback<MainProductsResponse> {
            override fun onResponse(call: Call<MainProductsResponse>, response: Response<MainProductsResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            expire = response.body()?.expired!!
                            msg = response.body()?.msg_expired!!
                           // Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.products?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                subCategories = response.body()?.subcategories!!
                                sections = response.body()?.subsections!!
                                subCategories.add(0, ListModel(0,getString(R.string.All)))
                                sections.add(0, ListModel(0,getString(R.string.All)))
                                listModel = subCategories.get(0)
                                productAdapter.updateAll(response.body()!!.products!!)
                                subCategoryAdapter.updateAll(subCategories)
                                sectionsAdapter.updateAll(sections)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<MainProductsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            Log.e("size",subCategories.size.toString())
            Log.e("pos",position.toString())
            subCategoryAdapter.selected = position
            listModel = subCategories.get(position)
            subCategories.get(position).checked = true
            subCategoryAdapter.notifyDataSetChanged()
            sectionsAdapter.selected = 0
            // listModel = subCategories.get(position)
            sections.get(0).checked = true
            sectionsAdapter.notifyDataSetChanged()
            if(subCategories.get(position).id==0){
                getHome(null,null)
            }else{
                getHome(listModel!!.id!!,null)
            }

        }else if (view.id == R.id.sub){
            Log.e("size",sections.size.toString())
            Log.e("pos",position.toString())
            sectionsAdapter.selected = position
           // listModel = subCategories.get(position)
            sections.get(position).checked = true
            sectionsAdapter.notifyDataSetChanged()
            if(sections.get(position).id==0){
                if (listModel!!.id==0){
                    getHome(null,null)
                }else {
                    getHome(listModel!!.id, null)
                }
            }else{
                if (listModel!!.id==0){
                    getHome(null,sections.get(position).id!!)
                }else {
                    getHome(listModel!!.id!!, sections.get(position).id!!)
                }
            }
        }else{
            if (expire==0) {
                val intent = Intent(activity, ProductDetailsActivity::class.java)
                intent.putExtra("id", productModels.get(position).id)
                startActivity(intent)
            }else{
                CommonUtil.makeToast(mContext!!,msg)

            }
        }
    }
}