package com.aait.ahjezproviderapp.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.R

class IntroOneActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_intro_one
    lateinit var image:ImageView
    lateinit var title:TextView
    lateinit var content:TextView
    lateinit var next:Button
    lateinit var skip:TextView

    override fun initializeComponents() {
        image = findViewById(R.id.image)
        title =findViewById(R.id.title)
        content = findViewById(R.id.content)
        next = findViewById(R.id.next)
        skip = findViewById(R.id.skip)
        image.setImageResource(R.mipmap.ten)
        title.text = getString(R.string.Ease_of_use_as_if_you_are_preparing)
        next.setOnClickListener { startActivity(Intent(this,IntroTwoActivity::class.java)) }
        skip.setOnClickListener { val intent = Intent(this, PreLoginActivity::class.java)

            startActivity(intent)
            finish() }

    }
}