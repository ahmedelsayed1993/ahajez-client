package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class ServiceDetailsModel:Serializable {
    var image:String?=null
    var images:ArrayList<ImagesModel>?=null
    var times:ArrayList<TimesModel>?=null
    var subcategory_id:Int?=null
    var subcategory:String?=null
    var name:String?=null
    var name_ar:String?=null
    var name_en:String?=null
    var price:String?=null
    var views:Int?=null
    var purchase:Int?=null
    var desc:String?=null
    var desc_ar:String?=null
    var desc_en:String?=null
    var hidden:Int?=null
    var refundable:Int?=null
    var free_reservation:String?=null
    var discount:String?=null
    var aftar_discount:String?=null
    var total_discount:String?=null
    var part_amount:String?=null
}