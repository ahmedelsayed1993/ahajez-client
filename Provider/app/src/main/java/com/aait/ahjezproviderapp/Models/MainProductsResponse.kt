package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class MainProductsResponse:BaseResponse(),Serializable {
    var subcategories:ArrayList<ListModel>?=null
    var subsections:ArrayList<ListModel>?=null
    var products:ArrayList<ProductModel>?=null
    var expired:Int?=null
    var msg_expired:String?=null
}