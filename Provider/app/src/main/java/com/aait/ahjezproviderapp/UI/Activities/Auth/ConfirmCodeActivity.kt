package com.aait.ahjezproviderapp.UI.Activities.Auth

import android.content.Intent
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.UserModel
import com.aait.ahjezproviderapp.Models.UserResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.IntroOneActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ConfirmCodeActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_confirm_code
    lateinit var confirm: Button
    lateinit var one:EditText
    lateinit var two:EditText
    lateinit var three:EditText
    lateinit var four:EditText
    lateinit var resend: TextView
    lateinit var text:TextView
    lateinit var userModel: UserModel
    var code = ""
    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("data") as UserModel
        confirm = findViewById(R.id.confirm)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        resend = findViewById(R.id.resend)
        text = findViewById(R.id.text)
        resend.isEnabled = false
        val timer = object: CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                text.text = millisUntilFinished.div(1000) .toString()
            }

            override fun onFinish() {
                resend.isEnabled = true
            }
        }
        timer.start()
        code = one.text.toString()+two.text.toString()+three.text.toString()+four.text.toString()
        Log.e("code",code)
        one.setSelection(0)
        two.setSelection(0)
        three.setSelection(0)
        four.setSelection(0)
        one.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                two.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        two.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                three.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        three.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                four.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(one,getString(R.string.activation_code))||
                CommonUtil.checkEditError(two,getString(R.string.activation_code))||
                CommonUtil.checkEditError(three,getString(R.string.activation_code))||
                CommonUtil.checkEditError(four,getString(R.string.activation_code))){
                return@setOnClickListener
            }else{
                var code = one.text.toString()+two.text.toString()+three.text.toString()+four.text.toString()
                check(code)

            } }
        resend.setOnClickListener { Resend() }


    }
    fun check(code:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode("Bearer "+userModel.token!!,code,lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent =
                            Intent(this@ConfirmCodeActivity, NewPassActivity::class.java)
                        intent.putExtra("data", response.body()?.data)
                        startActivity(intent)
                        this@ConfirmCodeActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Resend("Bearer "+userModel.token!!,lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<UserResponse>,
                response: Response<UserResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        resend.isEnabled = false
                        val timer = object: CountDownTimer(60000, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                                text.text = millisUntilFinished.div(1000) .toString()
                            }

                            override fun onFinish() {
                                resend.isEnabled = true
                            }
                        }
                        timer.start()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}