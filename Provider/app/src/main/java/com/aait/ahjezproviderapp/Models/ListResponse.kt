package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class ListResponse:BaseResponse(),Serializable {
    var data:ArrayList<ListModel>?=null
}