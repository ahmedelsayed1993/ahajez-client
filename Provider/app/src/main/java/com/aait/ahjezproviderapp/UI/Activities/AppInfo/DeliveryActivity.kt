package com.aait.ahjezproviderapp.UI.Activities.AppInfo

import android.content.Intent
import android.widget.*
import com.aait.ahjezproviderapp.Base.ParentActivity
import com.aait.ahjezproviderapp.Models.DeliveryResponse
import com.aait.ahjezproviderapp.Network.Client
import com.aait.ahjezproviderapp.Network.Service
import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.NotificationActivity
import com.aait.ahjezproviderapp.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.properties.Delegates

class DeliveryActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_delivery
     lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var fixed:RadioButton
    lateinit var different:RadioButton
    lateinit var no_delivery:RadioButton
    lateinit var fixed_price:EditText
    lateinit var fixed_distance:EditText
    lateinit var different_price:TextView
    lateinit var different_distance:EditText
    lateinit var confirm:Button
     var deliver = "no"
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        fixed = findViewById(R.id.fixed)
        different = findViewById(R.id.different)
        no_delivery = findViewById(R.id.no_delivery)
        fixed_price = findViewById(R.id.fixed_price)
        fixed_distance = findViewById(R.id.fixed_distance)
        different_price = findViewById(R.id.different_price)
        different_distance = findViewById(R.id.different_distance)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Delivery_Service)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        different_price.text = "10 "+getString(R.string.Rial)+"+ 1 "+getString(R.string.Rial)+"/"+getString(R.string.Km)
        fixed.setOnClickListener { deliver = "yes" }
        different.setOnClickListener { deliver = "yes" }
        no_delivery.setOnClickListener { deliver = "no" }
        Delivery()
        confirm.setOnClickListener {
            if (fixed.isChecked){
                if (CommonUtil.checkEditError(fixed_price,getString(R.string.Fixed_price))||
                        CommonUtil.checkEditError(fixed_distance,getString(R.string.Maximum_distance))){
                    return@setOnClickListener
                }else{
                    Delivery(fixed_price.text.toString(),fixed_distance.text.toString(),null,"0")
                }
            }else if (different.isChecked){
                if (CommonUtil.checkEditError(different_distance,getString(R.string.Maximum_distance))){
                    return@setOnClickListener
                }else{
                    Delivery(null,null,different_distance.text.toString(),"1")
                }
            }else{
                Delivery(null,null,null,"0")
            }
        }


    }

    fun Delivery(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Delivery("Bearer"+user.userData.token,lang.appLanguage,null,null,null,null,null)
                ?.enqueue(object : Callback<DeliveryResponse> {
                    override fun onFailure(call: Call<DeliveryResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<DeliveryResponse>, response: Response<DeliveryResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                fixed_price.setText(response.body()?.data?.delivery_price)
                                fixed_distance.setText(response.body()?.data?.maximum_distance)
                                different_distance.setText(response.body()?.data?.different_distance)
                                if (response.body()?.data?.delivery_service==0){
                                    no_delivery.isChecked = true
                                    deliver = "no"
                                }else{
                                    if (response.body()?.data?.check_different_distance.equals("0")){
                                        fixed.isChecked = true
                                        deliver = "yes"
                                    }else{
                                        different.isChecked = true
                                        deliver = "yes"
                                    }

                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun Delivery(fixed_price:String?,fixed_distance:String?,distance:String?,check:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Delivery("Bearer"+user.userData.token,lang.appLanguage,fixed_price,fixed_distance,distance,check,deliver)
                ?.enqueue(object : Callback<DeliveryResponse> {
                    override fun onFailure(call: Call<DeliveryResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<DeliveryResponse>, response: Response<DeliveryResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                               onBackPressed()
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}