package com.aait.ahjezproviderapp.Models

import java.io.Serializable

class WorkTimesModel:Serializable {
    var days:ArrayList<DaysModel>?=null
    var time_from:String?=null
    var time_to:String?=null
    var extra_from:String?=null
    var extra_to:String?=null
}