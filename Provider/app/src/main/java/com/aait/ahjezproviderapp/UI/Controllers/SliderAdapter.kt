package com.aait.ahjezproviderapp.UI.Controllers

import android.content.Context
import android.content.Intent
import android.view.MotionEvent
import android.view.ScaleGestureDetector

import android.view.View
import android.widget.ImageView

import com.aait.ahjezproviderapp.R
import com.aait.ahjezproviderapp.UI.Activities.Main.ImagesActivity
import com.aait.ahjezproviderapp.Models.ImagesModel


import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderAdapter





class SliderAdapter (context:Context,list : ArrayList<ImagesModel>) : CardSliderAdapter<ImagesModel>(list) {


    var list = list
    var context=context

    lateinit var image:ImageView
    override fun bindView(position: Int, itemContentView: View, item: ImagesModel?) {
            image = itemContentView.findViewById(R.id.image)
            Glide.with(context).load(item!!.image).into(image)
            itemContentView.setOnClickListener {
                val intent  = Intent(context, ImagesActivity::class.java)
                intent.putExtra("link",list)
                context.startActivity(intent)
            }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }

}